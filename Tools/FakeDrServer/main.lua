function fatal(...)
  print("FATAL:", ...)
  os.exit(-1)
end
oldPrint = print
print = function(...) oldPrint('Main',...) end

require 'threadUtils'
SDL = require 'SDL'

local udpThread, err = SDL.createThread("thUdp", 'udpThread.lua')
if not udpThread then error(err) fatal("Unable to create udp thread") end
local tcpThread, err = SDL.createThread("thTcp", 'tcpThread.lua')
if not tcpThread then error(err) fatal("Unable to create tcp thread") end

local udpChannel = SDL.getChannel ("chKernelToUdp")
local tcpChannel = SDL.getChannel ("chKernelToTcp")
for argumentIndex=1,#arg do
  local UpdateSourceFile = arg[argumentIndex]
  print("Update source file", UpdateSourceFile)
  tcpChannel:push("SET_UPDATE_FILE_PATH,"..UpdateSourceFile)
  udpChannel:push("BROADCAST_UPDATE")
  SDL.delay(1000)
end
tcpChannel:push("SET_UPDATE_FILE_PATH") -- Clear update file content

print("----------------END OF PROGRAM ----------------")
terminateThread("Udp")
waitForThreadDeath(udpThread); udpThread=nil
terminateThread("Tcp")
waitForThreadDeath(tcpThread); tcpThread=nil
print("Exiting main")
