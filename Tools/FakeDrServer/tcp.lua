require 'utils'

local SDL
local SDLNet
local tcpPort
local ip
local tcpSocket
local set
local SetSize = 8
local clients
local updateFileName
local updateFileDir
local updateFileContentPath

function tcp_init(sdlInstance, sdlNetInstance, port)
  -- copy params
  SDL = sdlInstance
  SDLNet = sdlNetInstance
  tcpPort = port

  -- check init
  if SDL == nil or SDLNet == nil
  then
    FATAL("Invalid SDL instance passed")
  end

  SDLNet.init()
  set = SDLNet.set(SetSize)
  clients = {}
end

function tcp_startServer(fileName, fileDir)
  if not SDL or not SDLNet then FATAL("tcp not initialized") end
  if tcpServer ~= nil then FATAL("TCP server already started") end
  if type(fileName) ~= 'string' or type(fileDir) ~= 'string' then FATAL("Invalid update file details") end

  --Start network
  local addr = SDLNet.resolveHost(nil, tcpPort)
  tcpSocket = SDLNet.openTcp(addr)
  set:add(tcpSocket)
  clients = {}

  --Save settings
  updateFileName = fileName
  updateFileDir = fileDir

  print("TCP server started")
end

function tcp_stopServer()
  --Clear the arrays
  for i,client in ipairs(clients) do
    set:del(client)
  end
  set:del(tcpSocket)
  set = nil

  --Close the socket
  tcpSocket:close()
  tcpSocket = nil

  -- Clean settings
  updateFileContentPath = nil
  updateFileName = nil
  updateFileDir = nil
end

local function serializeLength(len, byteCount)
  local lengthBytes = {}
  for i=1,byteCount do
    lengthBytes[i] = len % 256
    len = (len - lengthBytes[i]) / 256
  end
  if len ~= 0 then
    print("Warning, unable to completely serialize file length over " .. (byteCount or 'nil') .. " bytes")
  end

  local serializedString = ''
  for i=byteCount,1,-1 do
    serializedString = serializedString .. string.char(lengthBytes[i])
  end

  return serializedString
end

local function tcp_processIncomingBytes(client, bytes)
  -- TODO check params

  -- TODO Check if update file matches

  -- Load update file content
  local updateFileContent = files_load(updateFileContentPath)
  if updateFileContent == nil then
      FATAL("Unable to load TCP content file", filePath)
  end

  -- Build and send update file content
  local updateFileContentLength = string.len(updateFileContent)
  local message = serializeLength(updateFileContentLength,4) .. updateFileContent
  client:send(message)
  print("TCP socket sent ".. (string.len(message) or 'nil') .." bytes")

end

function tcp_serverPoll(pollingTimeout)
  --print("TCP poll (timeout: "..(pollingTimeout or 'nil')..")")

  local socketReadyCount = set:checkSockets(pollingTimeout)
  if socketReadyCount > 0 then
    --Check if it is the socket
    if tcpSocket:ready() then
      local newClient = tcpSocket:accept()
      if newClient then
        print("New client on TCP socket")
        -- Add client to arrays
        table.insert(clients, newClient)
        set:add(newClient)
      end

    --Or if it a client
    else
      -- Loop over clients to check for ready ones
      for clientIndex, client in ipairs(clients) do
        if client:ready() then
          local byteReceived, byteReceivedCount = client:recv(4096)
          if not byteReceived then
            print("TCP client disconnected")
            -- Remove client from arrays
            set:del(client)
            local _,i = table_find(clients, client)
            table.remove(clients, i)
          else
            -- Process received bytes
            print("TCP socket received ".. (byteReceivedCount or 'nil') .." bytes")
            tcp_processIncomingBytes(client, byteReceived)
          end -- check if bytes received
        end
      end -- client loop
    end -- socket or client ready if
  end -- socketReadCount > 0

end

-- nil means no file
function tcp_setFilePath(filePath)
  updateFileContentPath = filePath

  if updateFileContentPath then
    if files_exists(updateFileContentPath) ~= true then
      FATAL("TCP content file doesn't exist", filePath)
    end
  end
end
