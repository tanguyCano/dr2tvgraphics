local SDL
local SDLNet
local udpPort
local destIp = '127.0.0.1'
local ourIp = '127.0.0.1'

local address
local socket

function udp_init(sdlInstance, sdlNetInstance, port)
  -- copy params
  SDL = sdlInstance
  SDLNet = sdlNetInstance
  udpPort = port

  -- check init
  if sdlInstance == nil or sdlNetInstance == nil
  then
    FATAL("Invalid SDL instance passed")
  end
end

function udp_openSocket()
  print("Opening UDP socket", udpPort)

  address = SDLNet.resolveHost(destIp, udpPort)
  if not address then FATAL("Unable to resolve host address") end
  socket,err = SDLNet.openUdp(0)
  if not socket then FATAL("Unable to open udp socket") end

end

function udp_closeSocket()
  print("Closing UDP socket")
  socket:close()
  socket = nil
end

function udp_broadcastUpdate(allScoreBoard, updateFileName)
  if not SDL or not SDLNet then FATAL("udp not initialized") end
  if not socket then FATAL("udp socket not opened") end

  -- Build update udp message

  local eventAorB = 'a'
  local computerName = 'FakeDrServer'
  local eventMode = 1; if allScoreBoard == true then eventMode = 0 end
  if type(updateFileName) ~= 'string' then updateFileName = '' end
  local udpMsg = "UPDATE|" .. eventAorB .. '|' .. computerName .. '|' ..
    eventMode .. '|' .. ourIp .. '|' .. updateFileName .. '|' .. '^'

  print("UDP: send message", udpMsg)
  -- Send multiple update messages (simulate loss of some broadcast messages)
  for i=1,math.random(1,8) do
    socket:send(udpMsg, address)
  end

  --Workaround for the client to realise future update message won't be duplicates
  socket:send('NOISE', address)

end
