require 'utils'
require 'tcp'

SDL = require 'SDL'
Net = require 'SDL.net'
config = require 'config'

oldPrint = print
print = function(...) oldPrint('thTcp',...) end

-- Thread globals
local incomingChannel

function initThread()
  --Open main to tcp channel
  incomingChannel = SDL.getChannel "chKernelToTcp"
  if not incomingChannel then FATAL("Unable to open incoming channel") end

  --Initialize network
  tcp_init(SDL, Net, config.Dr.TcpPort)
  tcp_startServer(config.Dr.UpdateFileName, config.Dr.UpdateParentFolder)
end

function deinitThread()
  tcp_stopServer()
  Net.quit()
end

function tcpThreadLoop()
  while true do
    --Check for pending message from main
    if incomingChannel:first() ~= nil then
      local channelMsg = incomingChannel:wait()
      incomingChannel:pop()

      local messageFields = split(channelMsg, ',')
      local messageType = messageFields[1]
      local messageData = messageFields[2]
      print("Incoming message (type,data)", messageType, messageData)

      if messageType == "TERMINATE" then
        return -- Leave the loop
      elseif messageType == "SET_UPDATE_FILE_PATH" then
        tcp_setFilePath(messageData)
      end
    end

    --Run the socket server polling loop
    tcp_serverPoll(config.TcpPollingTimeoutMs)

    --Yield
    SDL.delay(config.TcpPollingIntervalMs)
  end
end

initThread()
tcpThreadLoop()
deinitThread()

print("Exiting thread")
return 0
