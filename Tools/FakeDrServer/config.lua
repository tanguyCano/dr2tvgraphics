local config = {}

config.Dr = {}
config.Dr.UdpPort = 58091
config.Dr.TcpPort = 58291
config.Dr.UpdateFileName = 'Update.txt'
config.Dr.UpdateParentFolder = 'Xfer'
config.Dr.UpdateFilePath = 'C:\\fake\\'..config.Dr.UpdateParentFolder..'\\'..config.Dr.UpdateFileName
config.UdpPollingIntervalMs = 10
config.TcpPollingIntervalMs = 10
config.TcpPollingTimeoutMs = 1

function FATAL(...)
  print("FATAL:", ...)
  os.exit(-1)
end

return config