function FATAL(...)
  print("FATAL:", ...)
  os.exit(-1)
end

function files_exists(filePath)
    if filePath == nil or type(filePath) ~= 'string' or filePath == '' then
        return false
    end

    -- attempt to open the file (will fail if file doesn't exist)
    local wasFileFound = false
    local fileHandler = io.open(filePath, "r")

    if fileHandler then
        wasFileFound = true
        fileHandler:close()
    end

    return wasFileFound
end

function files_load(filePath)
  local fileContent = nil
  if filePath == nil or type(filePath) ~= 'string' or filePath == '' then
    return false
  end

  local fileHandler = io.open(filePath, "r")
  if fileHandler then
    fileContent = fileHandler:read("*all")
    fileHandler:close()
  end

  return fileContent
end

---@function [parent=#utils] table_find
--@param #type search_token The value to look for in the associative array
--@param #table t The table to search through
--@return #bool whether or not at least one entry did match the search_token
function table_find(search_token, t)
  if type(t) ~= 'table' then return false end

  -- TODO handle research_token being a function
  for i,v in pairs(t) do
    if v == search_token then
      return true, i
    end
  end
  return false
end

-- NOT MY CODE

function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end
