function terminateThread(threadName)
  -- Get thread channel
  -- TODO, have a lookup table with thread channels
  local ch = SDL.getChannel ("chKernelTo"..threadName)
  if not ch then fatal("Unable to obtain channel for thread" .. threadName) end
  ch:push("TERMINATE")
end

function waitForThreadDeath(threadObject)
  local threadName = threadObject:getName()
  print("Waiting for end of thread " .. threadName)
  threadObject:wait()
  print(threadName .. " terminated")
end
