require 'utils'
require 'udp'

SDL = require 'SDL'
Net = require 'SDL.net'
config = require 'config'

oldPrint = print
print = function(...) oldPrint('thUdp',...) end

-- Thread globals
local incomingChannel

function initThread()
  --Open main to udp channel
  incomingChannel = SDL.getChannel "chKernelToUdp"
  if not incomingChannel then FATAL("Unable to open incoming channel") end

  --Initialize network
  udp_init(SDL, Net, config.Dr.UdpPort)
  udp_openSocket()

end

function deinitThread()
  udp_closeSocket()
end

function udpThreadLoop()
  -- Channel polling loop
  while true do
    local channelMsg = incomingChannel:wait()
    incomingChannel:pop()
    print("Incoming message", channelMsg)

    if channelMsg == "TERMINATE" then
      return -- Leave the loop
    elseif channelMsg == "BROADCAST_UPDATE" then
      udp_broadcastUpdate(false, config.Dr.UpdateFilePath)
    end
  end
end

initThread()
udpThreadLoop()
deinitThread()

print("Exiting thread")
return 0
