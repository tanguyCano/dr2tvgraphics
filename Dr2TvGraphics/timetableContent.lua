-- Sample timetable

--timetable internal structure
--{
--  meetName (string)
--  meetLocation (string)
--  sessionArray = array of
--    {
--      name (string)
--      startTimestamp (number) (force recomputed by buildSearchArray)
--      eventGroupArray = array of
--        {
--          startTimestamp (number)
--          eventArray = array of
--            {
--              eventAorB (optional string)
--              eventName (string)
--            }
--        }
--    }
--}

local year = 2022
local month = 1
local day1 = 31
local day2 = day1 + 1 -- Note, this handles day/month wraparound (31/01 + 1 = 01/02)
local timetable =
{
  meetName = 'Championnats de France hiver 2022',
  meetLocation = 'Piscine de Schiltigheim, Strasbourg',
  sessionArray =
  {
      {
        name = 'Matin',
        eventGroupArray =
          {
              {
                startTimestamp = os.time{year=year, month=month, day=day1, hour=10, min=0},
                eventArray =
                  {
                    [1] = {eventAorB = 'a', eventName = 'Jeunes - Garçons Cadets A - 1m'},
                    [2] = {eventAorB = 'a', eventName = 'Jeunes - Filles Minimes B - 1m'},
                    [3] = {eventAorB = 'b', eventName = 'Jeunes - Garçons Poussins D - 3m'},
                    [4] = {eventAorB = 'b', eventName = 'Jeunes - Filles Minimes C - 3m'},
                  },
              }, -- End of eventGroup
              {
                startTimestamp = os.time{year=year, month=month, day=day1, hour=11, min=0},
                eventArray =
                  {
                    [1] = {eventAorB = nil, eventName = 'Jeunes - Filles Benjamines C - 3m'},
                    [2] = {eventAorB = nil, eventName = 'Jeunes - Garçons Benjamins C - 3m'},
                  },
              }, -- End of eventGroup
          }, -- end of eventGroupArray
      }, -- end of session
      {
        name = 'Après-midi',
        eventGroupArray =
          {
              {
                startTimestamp = os.time{year=year, month=month, day=day1, hour=14, min=0},
                eventArray =
                  {
                    [1] = {eventAorB = 'a', eventName = 'Jeunes - Filles Cadettes A - 1m'},
                    [2] = {eventAorB = 'a', eventName = 'Jeunes - Garçons Minimes B - 1m'},
                    [3] = {eventAorB = 'b', eventName = 'Jeunes - Filles Poussines D - 3m'},
                    [4] = {eventAorB = 'b', eventName = 'Jeunes - Garçons Minimes C - 3m'},
                  },
              }, -- End of eventGroup
              {
                startTimestamp = os.time{year=year, month=month, day=day1, hour=16, min=0},
                eventArray =
                  {
                    [1] = {eventAorB = nil, eventName = 'Jeunes - Filles Benjamines C - 1m'},
                    [2] = {eventAorB = nil, eventName = 'Jeunes - Garçons Benjamins C - 1m'},
                  },
              }, -- End of eventGroup
          }, -- end of eventGroupArray
      }, -- end of session
      {
        name = 'Matin',
        eventGroupArray =
          {
              {
                startTimestamp = os.time{year=year, month=month, day=day2, hour=9, min=15},
                eventArray =
                  {
                    [1] = {eventAorB = nil, eventName = 'Jeunes - Fille A/B Sync - 3m'},
                    [2] = {eventAorB = nil, eventName = 'Jeunes - Garçons A/B Sync - 3m'},
                  },
              }, -- End of eventGroup
              {
                startTimestamp = os.time{year=year, month=month, day=day2, hour=10, min=30},
                eventArray =
                  {
                    [1] = {eventAorB = nil, eventName = 'Team Event'},
                  },
              }, -- End of eventGroup
          }, -- end of eventGroupArray
      }, -- end of session
  }, -- end of sessionArray
} -- end of timeTable

return timetable
