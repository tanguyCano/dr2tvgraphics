--- File relate functions

--- Requires

--- Constants

--- Private variables

--- Private functions

--- Public functions

---@function files_strToFiles
--@param #string str string to write into file
--@param #string filePath path of the file the string will be written to
--@return #bool whether or not the string was written to he file
function files_strToFile(str, filePath, append)
    local modeStr = 'w'
    if append == true then
      modeStr = 'a'
    end
    local fileHandler = io.open(filePath, modeStr)
    if fileHandler
    then
        fileHandler:write(str)
        fileHandler:close()
        return true
    end
    return false
end

---@function files_fileToStr
--@param #string filePath path of the file the string will be read from
--@return #string the content of the file
function files_fileToStr(filePath)
    local fileContent = ''
    local fileHandler = io.open(filePath, "r")
    if fileHandler
    then
        for line in fileHandler:lines()
        do
            fileContent = fileContent .. line .. '\n'
        end
        fileHandler:close()
    end
    return fileContent
end

---@function files_utf16ToUtf8
--@param #string fileContent_utf16 the utf16 string to convert to utf8
--@return #string the converted utf8 string
function files_utf16ToUtf8(fileContent_utf16)
  if type(fileContent_utf16) ~= 'string' then
    return nil
  end

  -- TODO, this is assuming you are on unix and have iconv installed
  local tmpTextFile = '/tmp/Dr2TvGraphics.update.txt'
  local ret = files_strToFile(fileContent_utf16,tmpTextFile) -- Save utf-16 content
  if ret ~= true then
      log(2, "File saving failed")
      return nil
  end
  ret = os.execute('iconv -f UTF-16LE -t UTF-8 '..tmpTextFile..' -o '..tmpTextFile)-- Convert to utf-8
  if ret ~= true then
    log(2, "iconv failed")
    return nil
  end
  local fileContent_utf8 = files_fileToStr(tmpTextFile)-- Read utf-8 content

  return fileContent_utf8
end
