--- Helper for loading and querying the timetable

--timetable internal structure
--{
--  meetName (string)
--  meetLocation (string)
--  sessionArray = array of
--    {
--      name (string)
--      startTimestamp (number) (force recomputed by buildSearchArray)
--      eventGroupArray = array of
--        {
--          startTimestamp (number)
--          eventArray = array of
--            {
--              eventAorB (optional string)
--              eventName (string)
--            }
--        }
--    }
--}

--- Requires

require 'files'
local config = require 'config'

--- Constants

local timetableContentFileName = 'timetableContent.lua'

--- Private variables

local timetable = nil

--- Private functions

local function sortTimetableArrays()
  -- Check we've been initialised
  if type(timetable) ~= 'table' then
    return
  end

  -- Clear search arrays
  sessionStartTimeSortedArray = {}

  -- Force sorting of all eventGroupArray, deduce session startTimestamp
  for sessionId,session in pairs(timetable.sessionArray or {}) do
    if type(session.eventGroupArray) == 'table' then
      table.sort(session.eventGroupArray, table_sorterBuilder_numberField('startTimestamp', true))
      session.startTimestamp = session.eventGroupArray[1].startTimestamp
    else
      session.startTimestamp = nil
    end
  end
  -- Force sorting of sessionArray
  table.sort(timetable.sessionArray, table_sorterBuilder_numberField('startTimestamp', true))
end

-- Returns the index (possibly #array+1) of the first array entry for which the
-- startTimestamp field is higher than the parameter
local function findNextStartTimestampArrayIdInArray(array, searchedTimestamp)
  -- Check param
  if type(array) ~= 'table' or type(searchedTimestamp) ~= 'number' then
    return nil
  end

  -- Handle obvious case
  if #array == 0 then
    log(2, 'findNextStartTimestampArrayIdInArray: empty main array')
    return nil
  end


  -- Build a search array with the timestamp of the main array
  local searchArray = {} -- array of {startTimestamp, mainArrayId}
  for i,v in ipairs(array) do
    table.insert(searchArray, {startTimestamp = v.startTimestamp, mainArrayId=i})
  end
  -- Add the searched timestamp to the search array (with an invalid id)
  table.insert(searchArray, {startTimestamp = searchedTimestamp, mainArrayId=-1})

  -- Sort the search array with ascending startTimestamp
  table.sort(searchArray, table_sorterBuilder_numberField('startTimestamp',true))

  -- Look for the searched timestamp in the now sorted array
  local searchedTimestampSearchArrayId = nil
  for searchArrayId, searchArrayEntry in ipairs(searchArray) do
    if type(searchArrayEntry) == 'table' and searchArrayEntry.mainArrayId == -1 then
      searchedTimestampSearchArrayId = searchArrayId
      break
    end
  end

  -- Return the main array id of the entry after the searched timestamp (handle corner cases)
  if searchedTimestampSearchArrayId == nil then
    -- We definitely should have found this
    log(2, 'findNextStartTimestampArrayIdInArray: internal error (could not find the searched timestamp)')
    return nil
  elseif searchedTimestampSearchArrayId == #searchArray then
    -- There's no next, return #array+1 to indicate so
    return #array + 1
  else
    -- Return the id of the entry after the searched timestamp in the search array
    if type(searchArray[searchedTimestampSearchArrayId+1]) ~= 'table' then
      -- Again, we just added these, we really shouldn't be in this case
      log(2, 'findNextStartTimestampArrayIdInArray: internal error (next entry in search table is not a table')
      return nil
    else
      return searchArray[searchedTimestampSearchArrayId+1].mainArrayId
    end
  end
end

local function getSession(sessionStr)
  if sessionStr ~= 'current' and sessionStr ~= 'next' then
    return nil
  end
  -- Check we've been initialised
  if type(timetable) ~= 'table' then
    return nil
  end

  local currentTime = os.time()
  log(4, 'Looking for '..sessionStr..' session, current time is ', os.date('%c', currentTime))

  local minimumStartTimestamp = currentTime + (config.cmd.timetableSessionCurrentMarginSeconds or 0)
  log(4, 'Looking for '..sessionStr..' session (configMarginSeconds, minimumStartTimestamp)', config.cmd.timetableSessionCurrentMarginSeconds, os.date('%c', minimumStartTimestamp))

  -- The session array is sorted, return the first that start after the minimumStartTimestamp (the next)
  local nextSessionArrayId = findNextStartTimestampArrayIdInArray(timetable.sessionArray, minimumStartTimestamp)
  -- Handle outcomes of the search function
  if type(nextSessionArrayId) ~= 'number' then
    -- error in the search, can't return anything
    log(2, 'Looking for '..sessionStr..' session, findNext returned nil (sessionArray type, sessionArray size)', type(timetable.sessionArray), #(timetable.sessionArray or {}))
    return nil
  else
    -- An array index was found (possibly out of bound of the array) for the next session
    log(4, 'Looking for '..sessionStr..' session, findNext returned (returned, sessionArray size)', nextSessionArrayId, #(timetable.sessionArray or {}))
    local sessionArrayId = nextSessionArrayId
    if sessionStr == 'current' then
      sessionArrayId = sessionArrayId - 1 -- Move from the next to the current
    end

    -- Return the session at this index (possibly nil if index is 0 (no current), or #sessionArray+1 (no next))
    local foundSession = timetable.sessionArray[sessionArrayId]
    if foundSession == nil then
      log(3, 'No '..sessionStr..' session found (minimumStartTimestamp)', os.date('%c', minimumStartTimestamp))
    else
      log(4, sessionStr..' session found (startTime, name)', os.date('%c', foundSession.startTimestamp), foundSession.name)
    end
    return foundSession
  end
end

--- Public functions

function command_timetable_loadFromFileSystem()
  timetable = nil -- clear previous content

  -- Lua and execute from file in a sandbox
  log(3, "Load timetable content from file", timetableContentFileName)
  local timetableContentStr = files_fileToStr(timetableContentFileName)
  log(5, "Timetable content is", timetableContentStr)

  local returnFunction, errorStr = load(timetableContentStr)
  if returnFunction == nil then
    log(2, "Unable to execute timetableContent, error", errorStr)
    log(3, "input serializedTableStr was", timetableContentStr)
  elseif type(returnFunction) == 'function' then
    timetable = returnFunction()
    if type(timetable) ~= 'table' then
      log(2, "TimetableContent didn't return a table (type)", type(timetable))
      timetable = nil
    else
      sortTimetableArrays()
    end
    --TODO, extra checks (looks for sessionGroup, ...)
  end

  return timetable ~= nil
end

function command_timetable_getNextEventGroup()
  -- Check we've been initialised
  if type(timetable) ~= 'table' then
    return nil
  end

  local currentTime = os.time()
  log(4, 'Looking for next eventGroup, current time is ', os.date('%c', currentTime))

  -- Concatenate all the sessions eventGroupArrays, sort them, and search for the next eventGroup in it
  local allEventGroupArrays = {}
  for _,session in ipairs(timetable.sessionArray or {}) do
    allEventGroupArrays = table_arrayConcatenate(allEventGroupArrays,session.eventGroupArray)
  end
  table.sort(allEventGroupArrays, table_sorterBuilder_numberField('startTimestamp',true))
  local nextEventGroupArrayId = findNextStartTimestampArrayIdInArray(allEventGroupArrays, currentTime)

  -- Handle outcomes of the search function
  if type(nextEventGroupArrayId) ~= 'number' then
    -- error in the search, can't return anything
    log(2, 'Looking for next eventGroup, findNext returned nil (sessionArray type, sessionArray size)', type(nextEventGroupArrayId), #(nextEventGroupArrayId or {}))
    return nil
  else
    -- An array index was found (possibly out of bound of the array) for the next session
    log(4, 'Looking for next eventGroup, findNext returned (returned, sessionArray size)', nextEventGroupArrayId, #(allEventGroupArrays or {}))

    -- Return the eventGroup at this index (possibly nil if index is #allEventGroupArrays+1 (no next))
    local foundEventGroup = allEventGroupArrays[nextEventGroupArrayId]
    if foundEventGroup == nil then
      log(3, 'No next eventGroup found (currentTime)', os.date('%c', currentTime))
    else
      log(4, 'eventGroup found (startTime)', os.date('%c', foundEventGroup.startTimestamp))
    end
    return foundEventGroup
  end
end

function command_timetable_getMeetName()
  if type(timetable) == 'table' and type(timetable.meetName) == 'string' then
    return timetable.meetName
  else
    return nil
  end
end

function command_timetable_getMeetLocation()
  if type(timetable) == 'table' and type(timetable.meetLocation) == 'string' then
    return timetable.meetLocation
  else
    return nil
  end
end

function command_timetable_getCurrentSession()
  return getSession('current')
end
function command_timetable_getNextSession()
  return getSession('next')
end
