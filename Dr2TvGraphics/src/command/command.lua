--- Helper for parsing socket (tcp) commands

--- Requires

require 'command.timetable'
local config = require 'config'

--- Constants

commandType = {}
commandType.CLEAR                     = 'clear'
commandType.SET_MEDAL_RANKING         = 'setMedalRanking' -- " true/false"
commandType.SHOW_NEXT_EVENT           = 'showNextEvent'
commandType.SHOW_SESSION_TIMETABLE    = 'showSessionTimetable' -- " current/next"
commandType.SHOW_TIME                 = 'showTime' -- " true/false"
commandType.RELOAD_TIMETABLE          = 'reloadTimetable'
commandType.PING                      = 'ping'
commandType.PRESENT                   = 'present'

commandOutcome = {}
commandOutcome.SUCCESS                = 'Success'
commandOutcome.FAIL                   = 'Fail'
commandOutcome.UNKNOWN_COMMAND        = 'Unknown command'
commandOutcome.INTERNAL_ERROR         = 'Internal error'
commandOutcome.PARAMETER_ERROR        = 'Parameter error'

local commandHandler = {} -- table of function(commandObject, cmdArgs) returning success, commandOutcome, errorDetails
-- Will be filled at the end of the private function, because the symbol don't exist yet

--- Private variables

--- Private functions

local function commandHandler_oneBooleanArg(commandObject, cmdArgs)
  -- Check param
  if type(commandObject) ~= 'table' then
    return false, commandOutcome.INTERNAL_ERROR
  end

  -- Get wanted session and store it in the command object
  local newValueStr = cmdArgs[2]:lower()
  if newValueStr == 'true' then
    commandObject.newValue = true
  elseif newValueStr == 'false' then
    commandObject.newValue = false
  else
    local errorDetails = "Unknown first argument '" .. (newValueStr  or 'nil') .. "'"
    return false, commandOutcome.PARAMETER_ERROR, errorDetails
  end

  return true, commandOutcome.SUCCESS
end

local function commandHandler_showNextEvent(commandObject, cmdArgs)
  -- Check param
  if type(commandObject) ~= 'table' then
    return false, commandOutcome.INTERNAL_ERROR
  end

  -- Get the next event group and store it in the command object
  commandObject.nextEventGroup = command_timetable_getNextEventGroup()

  if commandObject.nextEventGroup == nil then
    local errorDetails = 'Unable to retrieve next event group'
    return false, commandOutcome.FAIL, errorDetails
  else
    return true, commandOutcome.SUCCESS
  end
end

local function commandHandler_showSessionTimetable(commandObject, cmdArgs)
  -- Check param
  if type(commandObject) ~= 'table' then
    return false, commandOutcome.INTERNAL_ERROR
  end

  if type(cmdArgs[2]) ~= 'string' then
    local errorDetails = "Missing first argument (current/next)"
    return false, commandOutcome.PARAMETER_ERROR, errorDetails
  end

  -- Get wanted session and store it in the command object
  commandObject.sessionToShow = cmdArgs[2]:lower()
  if commandObject.sessionToShow == 'current' then
    commandObject.session = command_timetable_getCurrentSession()
  elseif commandObject.sessionToShow == 'next' then
    commandObject.session = command_timetable_getNextSession()
  else
    local errorDetails = "Unknown first argument '" .. (commandObject.sessionToShow  or 'nil') .. "'"
    return false, commandOutcome.PARAMETER_ERROR, errorDetails
  end

  if commandObject.session == nil then
    local errorDetails = 'Unable to retrieve ' .. (commandObject.sessionToShow or 'nil') .. ' session'
    return false, commandOutcome.FAIL, errorDetails
  end

  -- Populate other timetable fields
  commandObject.meetName = command_timetable_getMeetName()
  commandObject.meetLocation = command_timetable_getMeetLocation()

  return true, commandOutcome.SUCCESS
end

local function commandHandler_reloadTimetable(commandObject, cmdArgs)
  -- Check param
  if type(commandObject) ~= 'table' then
    return false, commandOutcome.INTERNAL_ERROR
  end

  -- This is not really parsing, just reloading the timetable
  local reloadSuccess = command_timetable_loadFromFileSystem()
  if reloadSuccess == true then
    return true, commandOutcome.SUCCESS
  else
    return true, commandOutcome.FAIL, 'Failed to load timetable from filesystem'
  end
end

-- Now that the symbol exist, fill the handler table
commandHandler[commandType.SET_MEDAL_RANKING]         = commandHandler_oneBooleanArg
commandHandler[commandType.SHOW_NEXT_EVENT]           = commandHandler_showNextEvent
commandHandler[commandType.SHOW_SESSION_TIMETABLE]    = commandHandler_showSessionTimetable
commandHandler[commandType.SHOW_TIME]                 = commandHandler_oneBooleanArg
commandHandler[commandType.RELOAD_TIMETABLE]          = commandHandler_reloadTimetable

--- Public functions

-- Returns parseSuccess, commandObject, commandOutcome, errorDetails
function command_socketBytesToObject(cmdBytes)
  -- Check params
  if type(cmdBytes) ~= 'string' then
    log(2, 'command_socketBytesToObject: Invalid cmdBytes type (type)', type(cmdBytes))
    return false
  end

  -- Check this is a line terminated command
  if string.len(cmdBytes) < 1 then
    log(2, 'command_socketBytesToObject: Zero cmdBytes passed')
    return false
  end
  if string.sub(cmdBytes,string.len(cmdBytes)) ~= '\n' then
    log(2, 'command_socketBytesToObject: cmdBytes is not terminated by a line return')
    return false
  end
  cmdBytes = string.sub(cmdBytes, 1, string.len(cmdBytes)-1) -- Trim terminating \n

  -- Clear possible '\r'
  if string.sub(cmdBytes,string.len(cmdBytes)) == '\r' then
    cmdBytes = string.sub(cmdBytes, 1, string.len(cmdBytes)-1) -- Trim terminating \n
  end

  -- TODO, handle multiple command at one
  -- TODO Add better errorDetails from this function

  -- Parse
  local cmdArgs = split(cmdBytes,' ',false)
  if type(cmdArgs) ~= 'table' or #cmdArgs < 1 then
    log(2, 'command_socketBytesToObject: cmdBytes could not be split to arguments')
    return false
  end
  log(4, 'Received cmd (type, arg count)', cmdArgs[1], #cmdArgs)

  -- Check type
  local commandObject = {}
  for _,type in pairs(commandType) do
    if type == cmdArgs[1] then
      commandObject.typeStr = type
    end
  end
  if commandObject.typeStr == nil then
    log(2, 'command_socketBytesToObject: Unknown cmd typeStr ', cmdArgs[1])
    return false, nil, commandOutcome.UNKNOWN_COMMAND
  end

  -- Execute (optional) command parser
  if type(commandHandler[commandObject.typeStr]) == 'function' then
    log(3, 'Executing command parser for type', commandObject.typeStr)
    local success, parserOutcome, errorDetails = commandHandler[commandObject.typeStr](commandObject, cmdArgs)
    if success ~= true then
      log(3, 'Command parser execution failed', parserOutcome, errorDetails)
      return success, commandObject, parserOutcome, errorDetails
    end
  end

  return true, commandObject, commandOutcome.SUCCESS
end
