--- Update file tcp content saving module
-- Allow to save tcp update files for future replay

--- Requires

require 'files'

--- Constants

--- Private variables

local replayActivated = false
local updateReplayDir

--- Private functions

--- Public functions

function save_updateReplay_start()
  -- Set directory based on update dir
  replayActivated = false
  updateReplayDir = os.getenv("DR2TVGRAPHICS_REPLAY_DIR")
  if updateReplayDir == nil then
    log(2, 'Unable to retrieve replay folder from $DR2TVGRAPHICS_REPLAY_DIR, replay won\'t be saved')
    return false
  end

  log(3, 'Using $DR2TVGRAPHICS_REPLAY_DIR', updateReplayDir)
  updateReplayDir = updateReplayDir .. '/'

  -- Try to save the current date in the folder (will log it and tell us if the path is usable)
  local firstFilePath = updateReplayDir .. 'startTime.txt'
  local writeSucceed = files_strToFile((os.date('%c') or 'fail to get date')..'\n', firstFilePath)
  if writeSucceed ~= true then
    log(2, 'Unable to create files in updateReplayDir, replay won`t be saved', firstFilePath)
    return false
  end

  -- Everything looks safe, enable replay saving
  replayActivated = true
  return true
end

function save_updateReplay_save(updateFileContent, updateTick)
  if replayActivated ~= true then
    return -- Replay aren't activated / possible
  end

  if type(updateFileContent) ~= 'string' or type(updateTick) ~= 'number' then
    log(2, 'Invalid params to save_updateReplay_save type(updateFileContent, updateTick)', type(updateFileContent), type(updateTick))
    return false
  end

  -- Create binary update file
  local updateTickStr = string.format("%010d", updateTick)
  if type(updateTickStr) ~= 'string' then
    log(2, 'Unable to build updateTick string (updateTick)', updateTick)
    return false
  end
  local updateFileName = 'updateReplay.'..updateTickStr..'.bin'
  local updateFilePath = updateReplayDir .. '/' .. updateFileName
  local saveSuccess = files_strToFile(updateFileContent, updateFilePath)
  if saveSuccess ~= true then
    log(2, 'Unable to save update replay (path)', updateFilePath)
  end
end
