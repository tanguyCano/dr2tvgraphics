--- Dive timestamp saving module
-- Allow to save dive and timestamp for generating start of dive tables

--- Requires

require 'files'

--- Constants

--- Private variables

local saveActivated = false
local saveDir
local diveTimestampFile

--- Private functions

--- Public functions

function save_diveTimestamp_start()
  saveActivated = false
  -- Create the file
  diveTimestampFile = os.getenv("DR2TVGRAPHICS_DIVESTAMPS_FILE")
  if diveTimestampFile == nil then
    log(2, 'Unable to retrieve replay folder from DR2TVGRAPHICS_DIVESTAMPS_FILE, dive timestamps won\'t be saved')
    return false
  end
  log(3, 'Using DR2TVGRAPHICS_DIVESTAMPS_FILE', diveTimestampFile)

  -- Create the empty file for next write to append
  local writeSucceed = files_strToFile('', diveTimestampFile)
  if writeSucceed ~= true then
    log(2, 'Unable to create file in saveDir, dive timestamps won`t be saved', diveTimestampFile)
    return false
  end

  -- Everything looks safe, enable replay saving
  saveActivated = true
  return true
end

function save_diveTimestamp_save(eventName, startOrder, round, diveCode, timestamp, diverAName, diverBName)
  if saveActivated ~= true then
    return -- saving isn't activated / possible
  end

  local fieldsToSave = {
    [1] = (eventName or 'nil'),
    [2] = (startOrder or 'nil'),
    [3] = (round or 'nil'),
    [4] = (diveCode or 'nil'),
    [5] = (timestamp or 'nil'),
    [6] = (diverAName or 'nil'),
    [7] = (diverBName or 'nil'),
  }

  local newLine = nil
  for _,field in ipairs(fieldsToSave) do
    -- Handle non printable types
    if type(field) ~= 'string' and type(field) ~= 'number' then
      field = 'nil'
    end
    if newLine == nil then
      newLine = '' -- first iteration
    else
      newLine = newLine .. '\t'
    end
    newLine = newLine .. field
  end
  newLine = newLine .. '\n'

  -- Append to info file
  local saveSuccess = files_strToFile(newLine, diveTimestampFile, true)
  if saveSuccess ~= true then
    log(2, 'Unable to append divestamp (path)', diveTimestampFile)
  end
end
