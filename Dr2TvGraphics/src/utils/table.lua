--- Table relate functions

--- Private functions

local function table_serialize_field(tableField, pre)
  -- Does not support cross references, loops nor functions
  local lr = '\n'
  if pre == nil then pre = '' end
  local serializedFieldStr = ''

  if type(tableField) == 'table' then
    serializedFieldStr = serializedFieldStr.. lr -- table will be split over several lines
    serializedFieldStr = serializedFieldStr.. pre .. '{' .. lr -- table opening bracket
    for i,v in pairs(tableField) do
      local nextPre = pre .. '  '
      -- key
      serializedFieldStr = serializedFieldStr.. nextPre .. '[' .. table_serialize_field(i,nextPre)
      -- value
      serializedFieldStr = serializedFieldStr.. '] = ' ..  table_serialize_field(v,nextPre) .. ',' .. lr
    end
    serializedFieldStr = serializedFieldStr.. pre .. '}' -- table closing bracket

  elseif type(tableField)== 'string' then
    serializedFieldStr = serializedFieldStr.. string.format("%q", tableField)

  elseif type(tableField)== 'number' then
    serializedFieldStr = serializedFieldStr.. tableField

  elseif type(tableField)== 'boolean' then
    local tableFieldStr = 'nil'
    if tableField == true then tableFieldStr = 'true' end
    if tableField == false then tableFieldStr = 'false' end
    serializedFieldStr = serializedFieldStr.. tableFieldStr

  elseif type(tableField)== 'nil' then
    serializedFieldStr = serializedFieldStr.. 'nil'

  else
    log(2, "Unhandled type in table serialization", type(tableField))
    return table_serialize_field(nil, pre) -- Neither crash nor serialize the field
  end

  return serializedFieldStr
end

--- Public functions

local table_serialize_magick = '--MAGICK_LUA_SERIALIZED_TABLE_154983\n'
---@function [parent=#table] table_serialize
--@param #table t The table to serialize
--@return #string The serialized table string
function table_serialize(t)
  local serializedTableStr = table_serialize_magick .. 'return '
  -- Serialize the table to lua code (complex types might not all be supported)
  serializedTableStr = serializedTableStr.. table_serialize_field(t)
  return serializedTableStr
end

---@function [parent=#table] table_deserialize
--@param #string serializedTableStr The serialized table string
--@return #table The deserialized table
function table_deserialize(serializedTableStr)
  if type(serializedTableStr) ~= 'string' then
    log(2, 'table_deserialize called with non string argument (type)', type(serializedTableStr))
    return nil
  end

  -- Check some magick header
  if serializedTableStr:sub(1,string.len(table_serialize_magick)) ~= table_serialize_magick then
    log(2, 'table_deserialize called with invalid string arguement (does not start with magick value)')
    return nil
  end

  -- Execute as lua code returning a table
  local returnValue
  local returnFunction, errorStr = load(serializedTableStr)
  if returnFunction == nil then
    log(2, "Unable to deserialize, error", errorStr)
    log(5, "input serializedTableStr was", serializedTableStr)
  elseif type(returnFunction) == 'function' then
    returnValue = returnFunction()
  end

  return returnValue
end

function printTable(t)
  for i,v in pairs(t) do
    print(i,v)
  end
end

function table_arrayToOneCharLookup(array)
  if type(array) ~= 'table' then return nil end

  local lookup = {}
  for i,v in ipairs(array) do
    local oneChar = string.char(string.byte('0') + i)
    lookup[v] = oneChar
  end
  return lookup
end

function table_reverseLookup(lookup)
  if type(lookup) ~= 'table' then return nil end

  local reverseLookup = {}
  for i,v in pairs(lookup) do
    reverseLookup[v] = i
  end
  return reverseLookup
end

function table_copyFields(srcTable, dstTable, fieldList)
  if type(srcTable) ~= 'table' then return end
  if type(dstTable) ~= 'table' then return end
  if type(fieldList) ~= 'table' then return end

  for i,v in pairs(fieldList) do
    dstTable[v] = srcTable[v]
  end
end

---@function [parent=#table] table_find
--@param #type search_token The value to look for in the associative array
--@param #table t The table to search through
--@param #table search_function Optional function to use to compare the search token and the array entries
--@return #bool whether or not at least one entry did match the search_token
function table_find(search_token, t, search_function)
  if type(t) ~= 'table' then return false end
  if type(search_function) ~= 'function' then
    search_function = function(a,b) return a == b end -- default value comparison
  end

  for i,v in pairs(t) do
    if search_function(v,search_token) then
      return true, i
    end
  end
  return false
end

---@function [parent=#table] table_finderBuilder_keyValue
--@param #string key The key in which to look for the search_token value if the object is an associative array
--@return #function a function that can be passed to table_find to look for key with search_token value
function table_finderBuilder_keyValue(key)
  return function(object, search_token)
    if type(object) == 'table' and object[key] == search_token then return true end
    return false
  end
end

---@function [parent=#table] table_arrayEntryKey_to_valueArray
--@param #table a The array in which all entries have a value for key k
--@param #string k The key to the values to make an array of
--@return #bool whether or not to abort if some entries don't have a value for key k
--@return #table an array of values, with possibly some indexes being left nil
function table_arrayEntryKey_to_valueArray(a, k, allowMissing)
  if type(a) ~= 'table' then return nil end
  if type(k) ~= 'string' then return nil end

  local valueArray = {}
  for i=1,#a do
    if a[i][k] == nil then
      if allowMissing ~= true then
        return nil
      end
    else
      valueArray[i] = a[i][k]
    end
  end -- For each entry
  return valueArray
end

---@function [parent=#table] table_sorterBuilder_numberField
--@param #string fieldName The key of the field to interpret as a number to compare the two array entries
--@param #bool sortAscending Whether or not to sort in ascending order (default to false)
--@return #function sortFunction A function which is used to sort between two array entries
function table_sorterBuilder_numberField(fieldName, sortAscending)
  if type(fieldName) == nil then return nil end
  if sortAscending ~= true then sortAscending = false end

  -- Return a closure over that field
  local fieldNameToSortOver = fieldName
  local tableSorter = function(a,b)
    local aFieldValue = nil
    local bFieldValue = nil
    -- Retrieve number the fields, if possible
    if type(a) == 'table' and type(a[fieldNameToSortOver]) == 'number' then
      aFieldValue = a[fieldNameToSortOver]
    end
    if type(b) == 'table' and type(b[fieldNameToSortOver]) == 'number' then
      bFieldValue = b[fieldNameToSortOver]
    end

    -- Note: nil will be considered less than any number
    if aFieldValue == nil then return true end  -- catches both nil or just bFieldValue
    if bFieldValue == nil then return false end -- catches just aFieldValue nil
    if sortAscending == true then
      return aFieldValue < bFieldValue
    else
      return aFieldValue > bFieldValue
    end
  end
  return tableSorter
end

---@function [parent=#table] table_arraySortedCopy
--@param #table a The array to return a sorted copy
--@param #function sortFunction A function which is used to sort between two array entries
--@return #table an new sorted array
function table_arraySortedCopy(a, sortFunction)
  if type(a) ~= 'table' then return nil end
  if type(sortFunction) ~= 'function' then return nil end

  local arraySortedCopy = {}
  for i=1,#a do
    table.insert(arraySortedCopy, a[i])
  end
  table.sort(arraySortedCopy, sortFunction)
  return arraySortedCopy
end

---@function [parent=#table] table_arrayConcat
--@param #table firstArray The array of which each elements should be first
--@param #table secondArray The array of which each elements should be second
--@return #table A new array containing each of the first array elements, then each of the second elements
function table_arrayConcatenate(firstArray, secondArray)
  if type(firstArray) ~= 'table' then firstArray = nil end
  if type(secondArray) ~= 'table' then secondArray = nil end
  if type(firstArray) == nil and secondArray == nil then
    return nil
  end

  local concatenatedArray = {}
  for i=1,#(firstArray or {}) do
    concatenatedArray[#concatenatedArray+1] = firstArray[i]
  end
  for i=1,#(secondArray or {}) do
    concatenatedArray[#concatenatedArray+1] = secondArray[i]
  end

  return concatenatedArray
end
