function FATAL(...)
  print("FATAL:", ...)
  os.exit(-1)
end

local logMaxLevel = 4 -- 0:failure, 1:critical, 2:warning, 3:debug, 4:log, 5:verbose
local threadFilter = nil -- No filtering if nil
function log(level, ...)
  if level <= logMaxLevel then
    if threadFilter == nil or currentThreadId==threadFilter then
      print(level,...)
    end
  end
end

function firstLetterToUpper(str)
  return (str:gsub("^%l", string.upper))
end

function stringbytes_to_int(str, byteOffset, byteCount, MSBFirst)
  -- check params
  if type(str) ~= 'string' then
    log(2, "Invalid 1st param type to stringbytes_to_int", type(str))
    return nil
  end
  --TODO all the other param checks
  -- Normalize optional param to bool
  if MSBFirst ~= true then
    MSBFirst = false
  end

  -- Build number from string bytes
  local n = 0
  local i
  -- TODO handle endianness
  for i=byteOffset,byteOffset+byteCount-1
  do
      n = n*256
      n = n + string.byte(str:sub(i+1,i+1))
  end
  return n
end

function stringbytes_to_hexdump(str)
  local hexdumpString = ''
  local asciiCol = 56

  local hexs = ''
  local asciis = ''
  for i=0,string.len(str)-1 do
    if i > 0 and (i % 16) == 0 then

      hexdumpString = hexdumpString .. hexs .. string.rep(' ' , asciiCol - #hexs) .. asciis .. '\n'
      hexs = ''
      asciis = ''
    end

    local c = str:sub(i+1,i+1)
    local b = string.byte(c)
    local printableC = c if b < string.byte(' ') or b > string.byte('~') then printableC = '.' end

    hexs = hexs .. string.format('%02x ', b)
    if (i % 16) > 0 and (i % 4) == 0 then
      hexs = hexs .. ' '
    end
    asciis = asciis .. printableC
  end
  if hexs ~= '' then
    hexdumpString = hexdumpString .. hexs .. string.rep(' ' , asciiCol - #hexs) .. asciis
  end

  return hexdumpString
end

function color_hexToRgb(hex)
  hex = math.round(hex)
  local r = math.floor(hex / (256 * 256))
  local g = math.floor((hex - (r*256*256)) / 256)
  local b = math.floor((hex - (r*256*256) - g*256))
  return r,g,b
end

function color_rgbToHex(r,g,b)
  return
    math.round(r) * 256 * 256 +
    math.round(g) * 256 +
    math.round(b)
end

function color_applyAlpha(color, bgColor, alphaRatio)
  -- Extract color components
  local cR,cG,cB = color_hexToRgb(color)
  local bR,bG,bB = color_hexToRgb(bgColor)

  return color_rgbToHex(
    math.round(bR + (cR-bR) * alphaRatio),
    math.round(bG + (cG-bG) * alphaRatio),
    math.round(bB + (cB-bB) * alphaRatio)
  )
end

-- NOT MY CODE (http://lua-users.org/wiki/SplitJoin) (improved with 3rd param)
function split(str, pat, keepEmptyEntries)
   if keepEmptyEntries ~= true then keepEmptyEntries = false end
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or (cap ~= "" or (keepEmptyEntries == true)) then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

math.round = function(n)
  if type(n) ~= 'number' then
    return math.floor(n) -- propagate error
  else
    return math.floor(n + 0.5)
  end
end
