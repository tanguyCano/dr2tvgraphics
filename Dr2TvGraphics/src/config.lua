local config = {}

config.general = {}
config.general.autostart = true

config.Dr = {}
config.Dr.UdpPort = 58091
config.Dr.TcpPort = 58291
config.Dr.UdpDuplicateTickDelta = 200
config.Dr.defaultDiverCountry = 'FRA'
config.Dr.defaultJudgeCountry = 'FRA'
config.Dr.flagNonFraNorMonAsGuest = true
config.UdpPollingIntervalMs = 10

config.cmd = {}
config.cmd.tcpPollingTimeoutMs = 1
config.cmd.pollingIntervalMs = 100
config.cmd.tcpPort = 58491
config.cmd.timetableSessionCurrentMarginSeconds = 45*60 -- 45 minutes

config.save = {}
config.save.saveUpdatesForReplay = true
config.save.saveDivesTimestamp = true

config.Ui = {}
config.Ui.useFramerate = false
config.Ui.framerate = 30
config.Ui.useTtl = true
config.Ui.ttlPeriodMs = 1000
config.Ui.ttlDiveInfoMs = 6000
config.Ui.colorKey = 0x00FF00 -- Greenscreen
config.Ui.tableMaxRowCount = 17 -- for table truncating and scrolling
config.Ui.tableScrollRowCount = 8 -- scrollAction PAGE_UP and PAGE_DOWN

config.Ui.ticker = {}
config.Ui.ticker.onlyShowTickerForActiveEvent = false
config.Ui.ticker.showTickerBeforeDive = true
config.Ui.ticker.beforeDiveMinimalTicker = false
config.Ui.ticker.showTickerAfterDive = true
config.Ui.ticker.afterDiveMinimalTicker = false

config.Ui.colorSwatch = {}
config.Ui.colorSwatch.baseFadedTxtAlphaRatio = 2/3
-- FFN color scheme
config.Ui.colorSwatch.baseColorTxt1 = 0xFFFFFF
config.Ui.colorSwatch.baseColorBg1 = 0x231A51
config.Ui.colorSwatch.baseColorBg2 = 0xC49C4E
config.Ui.colorSwatch.baseColorBg3 = 0xC20E1A

return config
