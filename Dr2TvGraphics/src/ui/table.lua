--- UI table composite graphic object
-- Big banner for title1, small banners for title2 and footer, tableRows for entries
-- Lua Class

--- Requires

require 'ui.banner'
require 'ui.screenLayout'
require 'ui.tableRow'

--- Constants

-- Div 10, H:6 wide and centered, V: person is 1 high (above 9th), title is 0.5 high (below 9th)
local layoutDiv = 10
local layoutWRatio = 8
local layoutYDivPos = 1 -- default value, overriden by constructor param
local layoutPersonHBaseRatio = 2/3
local layoutPersonHExtraPersonRatio = 1/3
local layoutPrefixXOffsetRatio = 1
local layoutTextXOffsetRatio = 2
local layoutTitle1HRatio = 1
local layoutTitle2HRatio = 1/2
local layoutFooterHRatio = 1/2

local textColor = 0xFFFFFF
local title1BgColor=0x000000
local title2BgColor=0x000000
local rowBgColor=0x000000
local footerBgColor=0x000000

--- Class constructor

--- @param #table paramTable {rowEntries, title1Text, title2Text, [footerText, yDivCount], title1BgColor, title2BgColor, rowBgColor, footerBgColor, textColor, screenLayout}
function ui_table_create(paramTable)

--- private members

local personsBanner
local titleBanner

--- private methods

local function computeDimensions(self)
  if self.screenLayout == nil then
    log(2,'No screenLayout provided')
    return false
  end

  -- Use layout to compute banners and row position and size based on number of row
  local layoutParamTable = {wDivisor=layoutDiv, hDivisor=layoutDiv}
  self.nextYDivCount = self.yDivCount

  layoutParamTable.xDivCount = 0 -- not relevant, we just use the vertical dimension
  layoutParamTable.yDivCount = self.nextYDivCount
  layoutParamTable.wDivCount = 0 -- not relevant, we just use the vertical dimension
  layoutParamTable.hDivCount = 0 -- not relevant, we just use the y position

  local layoutCenteredXDivPos = (layoutDiv-layoutWRatio)/2
  layoutParamTable.xDivCount = layoutCenteredXDivPos
  layoutParamTable.yDivCount = self.nextYDivCount
  layoutParamTable.wDivCount = layoutWRatio
  if self.title1Text ~= nil then
    layoutParamTable.hDivCount = layoutTitle1HRatio
    self.title1TextGeo = self.screenLayout:getScreenFraction(layoutParamTable)
    self.nextYDivCount = layoutParamTable.yDivCount + layoutParamTable.hDivCount -- Update y position for next element
  end

  if self.title2Text ~= nil then
    layoutParamTable.yDivCount = self.nextYDivCount
    layoutParamTable.hDivCount = layoutTitle2HRatio
    self.title2TextGeo = self.screenLayout:getScreenFraction(layoutParamTable)
    self.nextYDivCount = layoutParamTable.yDivCount + layoutParamTable.hDivCount -- Update y position for next element
  end

  self.entryRowGeoArray = {}
  layoutParamTable.xDivCount = 0 -- TODO, use a layout constant here
  layoutParamTable.yDivCount = 0 -- not relevant, we just use the horizontal dimension
  layoutParamTable.wDivCount = layoutPrefixXOffsetRatio
  layoutParamTable.hDivCount = 0 -- not relevant, we just use the horizontal dimension
  self.entryRowPrefixXOffset = self.screenLayout:getScreenFraction(layoutParamTable).w
  layoutParamTable.wDivCount = layoutTextXOffsetRatio
  self.entryRowTextXOffset = self.screenLayout:getScreenFraction(layoutParamTable).w

  layoutParamTable.xDivCount = layoutCenteredXDivPos
  layoutParamTable.yDivCount = self.nextYDivCount
  layoutParamTable.wDivCount = layoutWRatio
  for i=1,self.rowCount do
    local personCount = #self.rowEntries[i].personArray -- TODO, clean array earlier?
    local rowHRatio = layoutPersonHBaseRatio + (personCount-1) * layoutPersonHExtraPersonRatio
    layoutParamTable.yDivCount = self.nextYDivCount
    layoutParamTable.hDivCount = rowHRatio
    self.entryRowGeoArray[i] = self.screenLayout:getScreenFraction(layoutParamTable)
    self.nextYDivCount = layoutParamTable.yDivCount + layoutParamTable.hDivCount -- Update y position for next element
  end

  if self.footerText ~= nil then
    layoutParamTable.yDivCount = self.nextYDivCount
    layoutParamTable.hDivCount = layoutTitle2HRatio
    self.footerTextGeo = self.screenLayout:getScreenFraction(layoutParamTable)
    self.nextYDivCount = layoutParamTable.yDivCount + layoutParamTable.hDivCount -- Update y position for next element
  end

  return true
end

local function buildUiObjects(self)

  if self.title1Text ~= nil then
    self.title1TextBanner = ui_banner_create({text=self.title1Text, fontName='medium', bgColor=self.title1BgColor, textColor=self.textColor, textAlign='center', x=self.title1TextGeo.x, y=self.title1TextGeo.y, w=self.title1TextGeo.w, h=self.title1TextGeo.h})
    if self.title1TextBanner == nil then
      log(2,'Unable to create title1 banner')
      return false
    end
  end

  if self.title2Text ~= nil then
    self.title2TextBanner = ui_banner_create({text=self.title2Text, fontName='medium', bgColor=self.title2BgColor, textColor=self.textColor, textAlign='center', x=self.title2TextGeo.x, y=self.title2TextGeo.y, w=self.title2TextGeo.w, h=self.title2TextGeo.h})
    if self.title2TextBanner == nil then
      log(2,'Unable to create title2 banner')
      return false
    end
  end

  self.entryRowObjectArray = {}
  for i=1,self.rowCount do
    local tableRowParam = {}

    -- Copy geometry fields to the param table
    for _,fieldName in pairs({'x','y','w','h'}) do
      tableRowParam[fieldName] = self.entryRowGeoArray[i][fieldName]
    end

    -- Copy content fields to the param table
    for _,fieldName in pairs({'personArray','firstColumnText','lastColumnText'}) do
      tableRowParam[fieldName] = self.rowEntries[i][fieldName]
    end

    -- Copy table-wide config fields to the param table
    tableRowParam.prefixXOffset = self.entryRowPrefixXOffset
    tableRowParam.textXOffset = self.entryRowTextXOffset
    tableRowParam.bgColor = self.rowBgColor
    tableRowParam.textColor = self.textColor

    -- Build the row composite object
    self.entryRowObjectArray[i] = ui_tableRow_create(tableRowParam)
  end

  if self.footerText ~= nil then
    self.footerTextBanner = ui_banner_create({text=self.footerText, fontName='light', bgColor=self.footerBgColor, textColor=self.textColor, textAlign='center', x=self.footerTextGeo.x, y=self.footerTextGeo.y, w=self.footerTextGeo.w, h=self.footerTextGeo.h})
    if self.footerTextBanner == nil then
      log(2,'Unable to create footer banner')
      return false
    end
  end

  return true
end

--- class instance

local self = {}

--- public members

self.yDivCount = paramTable.yDivCount or layoutYDivPos
self.textColor = paramTable.textColor or textColor
self.title1BgColor= paramTable.title1BgColor or title1BgColor
self.title2BgColor= paramTable.title2BgColor or title2BgColor
self.rowBgColor = paramTable.rowBgColor or rowBgColor
self.footerBgColor= paramTable.footerBgColor or footerBgColor
self.screenLayout= paramTable.screenLayout
self.title1Text = paramTable.title1Text
self.title2Text = paramTable.title2Text
self.footerText = paramTable.footerText
self.rowEntries = paramTable.rowEntries or {}
self.rowCount = #self.rowEntries -- TODO, check content first

if computeDimensions(self) ~= true then
  log(2,'Unable to instantiate table (Failed to compute dimensions)')
  return nil
end

if buildUiObjects(self) ~= true then
  log(2,'Unable to instantiate table (Failed to build ui objects)')
  return nil
end

--- public methods

function self:getNextYDivCount()
  return self.nextYDivCount or 0
end

function self:draw()

  -- Draw title1, title2 and footer banners
  for _,banner in pairs({self.title1TextBanner, self.title2TextBanner, self.footerTextBanner}) do
    if banner ~= nil then
      banner:draw()
    end
  end

  if type(self.entryRowObjectArray) == 'table' then
    for i=1,self.rowCount do
      if self.entryRowObjectArray[i] ~= nil then
        self.entryRowObjectArray[i]:draw()
      end
    end
  end

end

return self
end
