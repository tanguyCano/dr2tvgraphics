--- UI presenter composite graphic object
-- Big banner for person(s), small banner for title
-- Lua Class

--- Requires

require 'ui.banner'
require 'ui.bannerPlus'
require 'ui.screenLayout'

--- Constants

-- Div 10, H:6 wide and centered, V: person is 1 high (above 9th), title is 0.5 high (below 9th)
local layoutDiv = 10
local layoutWRatio = 6
local layoutYDivPos = 8
local layoutPersonHBaseRatio = 1
local layoutPersonHExtraPersonRatio = 0.5
local layoutTitleHRatio = 0.5

local textColor=0xFFFFFF
local personsBgColor=0x000000
local titleBgColor=0x000000

--- Class constructor

--- @param #table paramTable {personsArray{{name,club,country}}, title, personsBgColor, titleBgColor, textColor, screenLayout}
function ui_presenter_create(paramTable)

--- private members

local personsBanner
local titleBanner

--- private methods

local function computeDimensions(self)
  -- Use layout to compute banner position and size based on number of persons
  local layoutCenteredXDivPos = (layoutDiv-layoutWRatio)/2
  local personBannerHRatio = layoutPersonHBaseRatio + (self.personCount -1) * layoutPersonHExtraPersonRatio
  local personYDivPos = layoutYDivPos - ((self.personCount-1) * layoutPersonHExtraPersonRatio)

  if self.screenLayout == nil then
    log(2,'No screenLayout provided')
    return false
  end

  local layoutParamTable = {wDivisor=layoutDiv, hDivisor=layoutDiv, xDivCount=layoutCenteredXDivPos, wDivCount=layoutWRatio}
  layoutParamTable.yDivCount = personYDivPos
  layoutParamTable.hDivCount = personBannerHRatio
  self.personBannerDim = self.screenLayout:getScreenFraction(layoutParamTable)
  layoutParamTable.yDivCount = personYDivPos + personBannerHRatio
  layoutParamTable.hDivCount = layoutTitleHRatio
  self.titleBannerDim = self.screenLayout:getScreenFraction(layoutParamTable)

  --fix possible gap between the two banners
  self.personBannerDim.y = self.titleBannerDim.y - self.personBannerDim.h

  return true
end

local function buildUiObjects(self)

  local textArray = {}
  local prefixArray = nil
  local flagKeyArray = nil
  for i = 1,self.personCount do
    textArray[i] = self.personArray[i].name

    if type(self.personArray[i].club) == 'string' then
      prefixArray = prefixArray or {} -- Make sure there's a prefixArray
      prefixArray[i] = self.personArray[i].club
    end

    if type(self.personArray[i].country) == 'string' then
      flagKeyArray = flagKeyArray or {} -- Make sure there's a flagKeyArray
      flagKeyArray[i] = self.personArray[i].country
    end
  end

  personsBanner = ui_bannerPlus_create({textArray=textArray, prefixArray = prefixArray, flagKeyArray = flagKeyArray, fontName='medium', bgColor=self.personsBgColor, textColor=self.textColor, textAlign='center', x=self.personBannerDim.x, y=self.personBannerDim.y, w=self.personBannerDim.w, h=self.personBannerDim.h})
  if personsBanner == nil then
    log(2,'Unable to create person bannerPlus')
    return false
  end

  if self.title ~= nil then
    titleBanner = ui_banner_create({text=self.title, fontName='light', bgColor=self.titleBgColor, textColor=self.textColor, textAlign='center', x=self.titleBannerDim.x, y=self.titleBannerDim.y, w=self.titleBannerDim.w, h=self.titleBannerDim.h})
    if titleBanner == nil then
      log(2,'Unable to create title banner')
      return false
    end
  end

  return true
end

--- class instance

local self = {}

--- public members

self.personArray = paramTable.personArray or {}
self.title = paramTable.title
self.personCount = #self.personArray -- TODO, check content first
self.personsBgColor = paramTable.personsBgColor or personsBgColor
self.titleBgColor = paramTable.titleBgColor or titleBgColor
self.textColor = paramTable.textColor or textColor
self.screenLayout = paramTable.screenLayout

if computeDimensions(self) ~= true then
  log(2,'Unable to instantiate presenter (Failed to compute dimensions)')
  return nil
end

if buildUiObjects(self) ~= true then
  log(2,'Unable to instantiate presenter (Failed to build ui objects)')
  return nil
end

--- public methods

function self:draw()
  personsBanner:draw()
  if self.title ~= nil then
    titleBanner:draw()
  end
end

return self
end
