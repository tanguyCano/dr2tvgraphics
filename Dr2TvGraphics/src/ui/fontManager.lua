--- Object in charge of SDL font handling
-- Check font availibiliy, auto-size fonts

--- Requires

require 'utils.misc'
--local config = require 'config'

--- Constants

local fontList =
{
  ['light'] =   {path='resources/GothamLight.ttf'},
  ['medium'] =  {path='resources/GothamMedium.ttf'},
  ['bold'] =    {path='resources/GothamBold.ttf'},
}

--- Private variables

--- Private functions

--- Public functions

function ui_fontManager_checkFontsAvailability()
  local allFontsAreAvailable = true

  log(4, 'Checking font availability')
  if type(fontList) ~= 'table' then
    log(2, 'No font table found')
    return false
  end

  for fontKey,fontProperties in pairs(fontList) do
    local thisFontIsAvailable = false
    log(4, "  - font: '" .. (fontKey or 'nil') .. "'")
    if type(fontProperties) ~= 'table' then
      log(2, "No properties for font '" .. (fontKey or 'nil') .. "'")
    else
      local fontPath = fontProperties.path
      if type(fontPath) ~= 'string' or fontPath == '' then
        log(2, "No path property for font '" .. (fontKey or 'nil') .. "'")
      else
        log(4, "    path is: '" .. (fontProperties.path or 'nil') .. "'")

        -- load font to check if its availability
        local font, err = SDLFont.open(fontProperties.path, 10)
        if err then
          log(2,"Unable to open font '" .. (fontKey or 'nil') .. "'")
        else
          thisFontIsAvailable = true -- All other branches will leave it false
        end -- font could be loaded by SDL
      end -- font path is a non empty string
    end -- font property is a table

    allFontsAreAvailable = allFontsAreAvailable and thisFontIsAvailable
  end -- font loop

  return allFontsAreAvailable
end

function ui_fontManager_getFont(fontName, fontSize)

  -- Check parameters
  if type(fontSize) ~= 'number' or fontSize == 0 then
    log(2,"Invalid font size parameter '" .. (fontSize or 'nil') .. "'")
    return nil
  end

  if type(fontName) ~= 'string' or fontName == '' then
    log(2,"Invalid font name parameter '" .. (fontName or 'nil') .. "'")
    return nil
  end
  if fontList[fontName] == nil then
    log(2,"Unknown font name parameter '" .. (fontName or 'nil') .. "'")
    return nil
  end

  -- Build and return font
  local font, err = SDLFont.open((fontList[fontName].path or ''), fontSize)
  if err then
    log(2,'Unable to open font ('.. (err or 'nil') ..')')
    return nil
  end

  return font
end

-- Return fontSize, firstTextCenterY, interTextYOffset, sidePadding
function ui_fontManager_autoSize(fontName, areaH, textCount)

  -- Check parameters
  if type(fontName) ~= 'string' or fontName == '' then
    log(2,"Invalid font name parameter '" .. (fontName or 'nil') .. "'")
    return nil
  end
  if fontList[fontName] == nil then
    log(2,"Unknown font name parameter '" .. (fontName or 'nil') .. "'")
    return nil
  end

  if type(areaH) ~= 'number' or areaH == 0 then
    log(2,"Invalid areaH parameter '" .. (areaH or 'nil') .. "'")
    return nil
  end

  if type(textCount) == 'nil' then
    textCount = 1 -- Optional parameter
  end
  if type(textCount) ~= 'number' or textCount == 0 or math.floor(textCount) ~= textCount then
    log(2,"Invalid textCount parameter '" .. (textCount or 'nil') .. "'")
    return nil
  end

  -- Do the math (note that we don't really use the fontName in the current implementation ...)
  local fontSizeToFontHeight = 0.90 -- TODO This is empirical data on the current font
  local marginCount = 2 + (textCount-1)
  local textTotalHeightRatio = 0.60
  local textHeightRatio = 0.60 / textCount
  local marginHeightRatio = (1 - textTotalHeightRatio) / marginCount

  local fontSize = math.round(textHeightRatio * areaH / fontSizeToFontHeight)
  local firstTextCenterY = marginHeightRatio * areaH + textHeightRatio * areaH / 2
  local interTextYOffset = marginHeightRatio * areaH + textHeightRatio * areaH
  local sidePadding = math.round(marginHeightRatio * areaH)

  return fontSize, firstTextCenterY, interTextYOffset, sidePadding
end
