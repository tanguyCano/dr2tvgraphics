--- UI banner graphic object
-- Single or multi line text over a solid background rectangle
-- Lua Class

--- Requires

require 'ui.fontManager'

--- Constants

--- Class constructor

--- @param #table paramTable {text or textArray, textColor, bgColor, textAlign, x, y, w, h, fontName}
function ui_banner_create(paramTable)

--- private members

--- private methods

local function createSdlObjects(self)

  -- Use fontManager to compute the ideal font size and vertical position of each of the text
  local firstTextCenterY, interTextYOffset
  self.fontSize, firstTextCenterY, interTextYOffset, self.sidePadding = ui_fontManager_autoSize(self.fontName, self.h, self.textCount)

  if self.fontSize == nil or firstTextCenterY == nil or interTextYOffset == nil then
    log(2,'Unable to autoSize banner (font: '.. (self.fontName or 'nil') ..', h:'.. (self.h or 'nil') ..' )')
    return false
  end

  self.textCenterYOffsetFloat = {}
  for textId=1,self.textCount do
    self.textCenterYOffsetFloat[textId] = firstTextCenterY + interTextYOffset * (textId-1)
  end

  -- Font and background surface
  self.font = ui_fontManager_getFont(self.fontName, self.fontSize)
  if self.font == nil then
    return false
  end
  local err
  self.backgroundSurface, err = SDLImage.load("resources/White.bmp")
  if err then
    log(2,'Unable to open image ('.. (err or 'nil') ..')')
    return false
  end
  self.backgroundSurface:fillRect(nil,self.bgColor)
  self.backgroundTexture = ui_renderer_renderer:createTextureFromSurface(self.backgroundSurface)
  self.backgroundSrcRect = {x=0,y=0,w=10,h=10}

  -- Text textures
  self.textTextureArray = {}
  for textId=1,self.textCount do
    local textSurface = self.font:renderUtf8(self.textArray[textId], "blended", self.textColor)
    self.textTextureArray[textId] = ui_renderer_renderer:createTextureFromSurface(textSurface)
  end
  -- Save regular text height (accentuation messes with text height)
  local referenceTextSurface = self.font:renderUtf8('A', "blended", self.textColor)
  _,_,_,self.textReferenceTextureHeight = ui_renderer_renderer:createTextureFromSurface(referenceTextSurface):query()

  return true
end

local function computeGeometry(self)
  self.backgroundGeo = {}
  for _,fieldName in pairs({'x','y','w','h'}) do  -- Copy object geometry
    self.backgroundGeo[fieldName] = self[fieldName]
  end

  self.textGeo = {}
  for textId=1,self.textCount do
    self.textGeo[textId] = {}

    -- W and H come from text texture size
    _,_,self.textGeo[textId].w,self.textGeo[textId].h = self.textTextureArray[textId]:query()

    -- Y is vertically centered around y + textCenterYOffsetFloat[textId]
    self.textGeo[textId].y = math.round(self.y + self.textCenterYOffsetFloat[textId] - self.textGeo[textId].h/2)

    -- limit text w in case of text overflow
    local maxTextW = self.w - 2*self.sidePadding
    self.textGeo[textId].w = math.min(self.textGeo[textId].w, maxTextW)

    -- X is based on alignment
    if self.textAlign == 'left' then
      self.textGeo[textId].x = self.x + self.sidePadding
    elseif self.textAlign == 'right' then
      self.textGeo[textId].x = self.x + self.w - self.sidePadding - self.textGeo[textId].w
    else -- center
      self.textGeo[textId].x = self.x + math.round((self.w - self.textGeo[textId].w)/2)
    end
  end

  return true
end

--- class instance

local self = {}

--- public members

if type(paramTable.text) == 'string' then
  self.textArray = {[1]=paramTable.text} -- Build a single entry textArray
  -- TODO, warning if param.textArray is also set
else
  self.textArray = paramTable.textArray or {[1]=' '}
  -- TODO protect against invalid table content
end
self.textCount = #self.textArray
self.textColor = paramTable.textColor or 0xFFFFFF
self.bgColor = paramTable.bgColor or 0x000000
self.textAlign = (paramTable.textAlign or 'left'):lower()
self.x = paramTable.x or 0
self.y = paramTable.y or 0
self.w = paramTable.w or 100
self.h = paramTable.h or 25
self.fontName = paramTable.fontName or 'medium'

if createSdlObjects(self) ~= true then
  log(2,'Unable to instanciate banner (Failed to create Sdl objects)')
  return nil
end

if computeGeometry(self) ~= true then
  log(2,'Unable to instanciate banner (Failed to compute geometry)')
  return nil
end

--- public methods

function self:draw()
  ui_renderer_renderer:copy(self.backgroundTexture, self.backgroundSrcRect, self.backgroundGeo)

  for textId=1,self.textCount do
    ui_renderer_renderer:copy(self.textTextureArray[textId], nil, self.textGeo[textId])
  end
end

return self
end
