--- UI dive card graphic object
-- top : Single or multi line text over a solid background rectangle, lead by optional flag and/or prefix, surrounded by position and points
-- botom : Dive details, with or without awards
-- Lua Class

--- Requires

require 'ui.fontManager'
require 'ui.screenLayout'
require 'ui.tableRow'
require 'utils.misc'

--- Constants

-- Div 10, H:6 wide and centered, V: person is 1 high (above 9th), title is 0.5 high (below 9th)
local layoutDiv = 10
local layoutWRatio = 8
local layoutYDivPos = 7
local layoutPersonHBaseRatio = 1
local layoutPersonHExtraPersonRatio = 0.5
local layoutPrefixXOffsetRatio = 1.2 -- TODO rename to rank and point (sorta) ?
local layoutTextXOffsetRatio = 2
local layoutInfoHRatio = 1
local layoutInfoDDXOffsetRatio = 3/4
local layoutInfoDiveNameXOffsetRatio = 3/2
local layoutAwardSoloFirstXOffsetRatio = 3/2
local layoutAwardSynchroFirstXOffsetRatio = 1.8
local layoutAwardSoloNextXOffsetRatio = 1
local layoutAwardSynchroNextXOffsetRatio = 4/5
local layoutPenaltyBannerWRatio = 3
local layoutPenaltyBannerHRatio = 0.5

local fontNameNumber = 'medium'
local fontNameHeight = 'light'
local fontNameDD = 'medium'
local fontNameDiveName = 'light'
local fontNameAwardSynchroLabel = 'light'
local fontNameAward = 'bold'
local fontPenalty = 'light'
-- Ratio to full size text
local fontSizeRatioTotal = 0.85
local fontSizeRatioNumber = 2/3
local fontSizeRatioHeight = 2/3
local fontSizeRatioDD = 2/3
local fontSizeRatioDiveName = 0.5
local fontSizeRatioAwardSynchroLabel = 2/3
local fontSizeRatioAwardSolo = 1
local fontSizeRatioAwardSynchro = 3/4

local textColor=0xFFFFFF
local bgColor=0x000000
local penaltyBgColor=0xFF0000
local fadedTextOpacity = 2/3

--- Class constructor

--- @param #table paramTable {personArray, rank, points, bgColor, penaltyBgColor, textColor, textFadedColor, screenLayout, diveNumber, diveHeight, diveDD, diveName, diveTotal, awards, awardsUsed, synchro, executionAwardCount, penalty}
function ui_diveCard_create(paramTable)

--- private members

local personsBanner

--- private methods

local function computeGeometry(self)
  if self.screenLayout == nil then
    log(2,'No screenLayout provided')
    return false
  end

  -- Use layout to compute banners and row position and size based on number of row
  local layoutParamTable = {wDivisor=layoutDiv, hDivisor=layoutDiv}

  -- person row
  local layoutCenteredXDivPos = (layoutDiv-layoutWRatio)/2
  local personCount = #self.personArray -- TODO, clean array earlier?
  local rowHRatio = layoutPersonHBaseRatio + (personCount-1) * layoutPersonHExtraPersonRatio
  layoutParamTable.xDivCount = layoutCenteredXDivPos
  layoutParamTable.yDivCount = layoutYDivPos
  layoutParamTable.wDivCount = layoutWRatio
  layoutParamTable.hDivCount = rowHRatio
  self.personRowGeo = self.screenLayout:getScreenFraction(layoutParamTable)

  layoutParamTable.wDivCount = layoutPrefixXOffsetRatio -- Only relevant field since we generate width
  self.entryRowPrefixXOffset = self.screenLayout:getScreenFraction(layoutParamTable).w
  layoutParamTable.wDivCount = layoutTextXOffsetRatio -- Only relevant field since we generate width
  self.entryRowTextXOffset = self.screenLayout:getScreenFraction(layoutParamTable).w

  -- Info geometry
  layoutParamTable.xDivCount = layoutCenteredXDivPos
  layoutParamTable.yDivCount = 0 -- not relevant, we reuse previous y + h (TODO, fix this, use division instead)
  layoutParamTable.wDivCount = layoutWRatio
  layoutParamTable.hDivCount = layoutInfoHRatio
  self.infoBgGeo = self.screenLayout:getScreenFraction(layoutParamTable) -- Ignore y
  -- Set y just below the person row
  self.infoBgGeo.y = self.personRowGeo.y + self.personRowGeo.h

  -- Info font sizes
  local fullFontSize
  fullFontSize, _, _, self.infoSidePadding = ui_fontManager_autoSize(fontNameNumber, self.infoBgGeo.h, 1)
  self.infoFontSizeNumber = math.round(fullFontSize * fontSizeRatioNumber)
  self.infoFontSizeHeight = math.round(fullFontSize * fontSizeRatioHeight)
  self.infoFontSizeDD = math.round(fullFontSize * fontSizeRatioDD)
  self.infoFontSizeDiveName = math.round(fullFontSize * fontSizeRatioDiveName)
  self.infoFontSizeAwardSynchroLabel = math.round(fullFontSize * fontSizeRatioAwardSynchroLabel)
  if self.synchro == true then
    self.infoFontSizeAward = math.round(fullFontSize * fontSizeRatioAwardSynchro)
  else
    self.infoFontSizeAward = math.round(fullFontSize * fontSizeRatioAwardSolo)
  end

  -- Info text basic geometry
  _, self.infoFirstTextCenterY, self.infoInterTextYOffset= ui_fontManager_autoSize(fontNameNumber, self.infoBgGeo.h, 2)
  layoutParamTable = {wDivisor=layoutDiv, hDivisor=layoutDiv, xDivCount=0, yDivCount=0, hDivCount=0} -- We will generate several width
  layoutParamTable.wDivCount = layoutInfoDDXOffsetRatio
  self.infoDDXOffset = self.screenLayout:getScreenFraction(layoutParamTable).w
  layoutParamTable.wDivCount = layoutInfoDiveNameXOffsetRatio
  self.infoDiveNameXOffset = self.screenLayout:getScreenFraction(layoutParamTable).w
  if self.synchro == true then
    layoutParamTable.wDivCount = layoutAwardSoloFirstXOffsetRatio
    self.infoAwardSynchroLabelXOffset = self.screenLayout:getScreenFraction(layoutParamTable).w
    layoutParamTable.wDivCount = layoutAwardSynchroFirstXOffsetRatio
    self.infoFirstAwardXOffset = self.screenLayout:getScreenFraction(layoutParamTable).w
  else
    layoutParamTable.wDivCount = layoutAwardSoloFirstXOffsetRatio
    self.infoFirstAwardXOffset = self.screenLayout:getScreenFraction(layoutParamTable).w
  end

  if type(self.penalty) == 'string' then
    -- Below the bgInfo, aligned with the left edge
    layoutParamTable.xDivCount = layoutCenteredXDivPos
    layoutParamTable.yDivCount = 0 -- not relevant, we reuse previous y + h (TODO, fix this, use division instead)
    layoutParamTable.wDivCount = layoutPenaltyBannerWRatio
    layoutParamTable.hDivCount = layoutPenaltyBannerHRatio
    self.penaltyBannerGeo = self.screenLayout:getScreenFraction(layoutParamTable) -- Ignore y
    -- Set y just below the infoBackground rectangle
    self.penaltyBannerGeo.y = self.infoBgGeo.y + self.infoBgGeo.h
  end

  return true
end

local function createSdlObjects(self)
  local tableRowParam = {}
  tableRowParam.bgColor = self.bgColor
  tableRowParam.textColor = self.textColor
  tableRowParam.personArray = self.personArray
  tableRowParam.firstColumnText = self.rank
  tableRowParam.lastColumnText = self.points
  if self.displayAwards == true then
    tableRowParam.columnTextColor = self.textColor
  else
    tableRowParam.columnTextColor = self.textFadedColor
  end
  tableRowParam.lastColumnFontSizeRatio = fontSizeRatioTotal

  -- Copy geometry fields to the param table
  for _,fieldName in pairs({'x','y','w','h'}) do
    tableRowParam[fieldName] = self.personRowGeo[fieldName]
  end
  tableRowParam.prefixXOffset = self.entryRowPrefixXOffset
  tableRowParam.textXOffset = self.entryRowTextXOffset


  personsBanner = ui_tableRow_create(tableRowParam)
  if personsBanner == nil then
    log(2,'Unable to create person table Row')
    return false
  end

  -- Info bg
  local err
  self.backgroundSurface, err = SDLImage.load("resources/White.bmp")
  if err then
    log(2,'Unable to open image ('.. (err or 'nil') ..')')
    return false
  end
  self.backgroundSurface:fillRect(nil,self.bgColor)
  self.backgroundTexture = ui_renderer_renderer:createTextureFromSurface(self.backgroundSurface)
  self.backgroundSrcRect = {x=0,y=0,w=10,h=10}

  -- Info text fonts
  self.infoNumberFont = ui_fontManager_getFont(fontNameNumber, self.infoFontSizeNumber)
  self.infoHeightFont = ui_fontManager_getFont(fontNameHeight, self.infoFontSizeHeight)
  self.infoDDFont = ui_fontManager_getFont(fontNameDD, self.infoFontSizeDD)
  self.infoDiveNameFont = ui_fontManager_getFont(fontNameDiveName, self.infoFontSizeDiveName)
  self.infoDiveTotalFont = personsBanner.lastColumnFont -- Clone personsBanner (tableRow) lastColumnFont (total)
  self.infoAwardSynchroLabelFont = ui_fontManager_getFont(fontNameAwardSynchroLabel, self.infoFontSizeAwardSynchroLabel)
  self.infoAwardFont = ui_fontManager_getFont(fontNameAward, self.infoFontSizeAward)
  if self.infoNumberFont == nil or self.infoHeightFont == nil or self.infoDDFont  == nil or self.infoDiveNameFont == nil or self.infoDiveTotalFont == nil or self.infoAwardFont == nil or self.infoAwardSynchroLabelFont == nil then
    log(2, "Unable to instanciate one of the info text fonts")
    return false
  end

  -- Info text textures
  local textSurface
  textSurface, err = self.infoNumberFont:renderUtf8(self.diveNumber, "blended", self.textColor)
  if textSurface == nil then
    log(2, "Error when rendering info dive number '" .. (self.diveNumber or 'nil') .. "' : " .. (err or 'nil'))
    return false
  end
  self.infoNumberTexture = ui_renderer_renderer:createTextureFromSurface(textSurface)

  textSurface, err = self.infoHeightFont:renderUtf8(self.diveHeight, "blended", self.textColor)
  if textSurface == nil then
    log(2, "Error when rendering info dive height '" .. (self.diveHeight or 'nil') .. "' : " .. (err or 'nil'))
    return false
  end
  self.infoHeightTexture = ui_renderer_renderer:createTextureFromSurface(textSurface)

  textSurface, err = self.infoDDFont:renderUtf8(self.diveDD, "blended", self.textColor)
  if textSurface == nil then
    log(2, "Error when rendering info dive DD '" .. (self.diveDD or 'nil') .. "' : " .. (err or 'nil'))
    return false
  end
  self.infoDDTexture = ui_renderer_renderer:createTextureFromSurface(textSurface)

  if self.displayAwards ~= true then
    textSurface, err = self.infoDiveNameFont:renderUtf8(self.diveName, "blended", self.textColor)
    if textSurface == nil then
      log(2, "Error when rendering info dive name '" .. (self.diveName or 'nil') .. "' : " .. (err or 'nil'))
      return false
    end
    self.infoDiveNameTexture = ui_renderer_renderer:createTextureFromSurface(textSurface)
  end

  if self.displayAwards == true then
    textSurface, err = self.infoDiveTotalFont:renderUtf8(self.diveTotal, "blended", self.textColor)
    if textSurface == nil then
      log(2, "Error when rendering info dive total '" .. (self.diveTotal or 'nil') .. "' : " .. (err or 'nil'))
      return false
    end
    self.infoDiveTotalTexture = ui_renderer_renderer:createTextureFromSurface(textSurface)
  end

  -- Info awards text textures
  if self.synchro == true then
    -- TODO, extract language constants ('E' and 'S')
    local execLabel = 'E'
    local synchLabel = 'S'

    textSurface, err = self.infoAwardSynchroLabelFont:renderUtf8(execLabel, "blended", self.textColor)
    if textSurface == nil then
      log(2, "Error when rendering award label '".. (execLabel or 'nil') .."' : " .. (err or 'nil'))
      return false
    end
    self.infoAwardSynchroExecLabelTexture = ui_renderer_renderer:createTextureFromSurface(textSurface)

    textSurface, err = self.infoAwardSynchroLabelFont:renderUtf8(synchLabel, "blended", self.textColor)
    if textSurface == nil then
      log(2, "Error when rendering award label '".. (synchLabel or 'nil') .."' : " .. (err or 'nil'))
      return false
    end
    self.infoAwardSynchroSyncLabelTexture = ui_renderer_renderer:createTextureFromSurface(textSurface)
  end

  if self.displayAwards == true then
    self.infoAwardTextureArray = {}
    for awardId=1,#self.awards do
      --TODO, force award format to "%1.1d"
      local awardColor = self.textColor
      if type(self.awardsUsed) == 'table' and self.awardsUsed[awardId] == false then
        awardColor = self.textFadedColor
      end
      textSurface, err = self.infoAwardFont:renderUtf8(self.awards[awardId], "blended", awardColor)
      if textSurface == nil then
        log(2, "Error when rendering award ".. (awardId or 'nil') .." '" .. (self.awards[awardId] or 'nil') .. "' : " .. (err or 'nil'))
        return false
      end
      self.infoAwardTextureArray[awardId] = ui_renderer_renderer:createTextureFromSurface(textSurface)
    end
  end

  -- Info penalty
  if type(self.penalty) == 'string' then
    -- Create a banner below the infoBg rectangle
    local bannerParam = {}
    bannerParam.text=self.penalty
    bannerParam.fontName=fontPenalty
    bannerParam.bgColor = self.penaltyBgColor
    bannerParam.textAlign='left'
    -- Copy geometry fields to the param table
    for _,fieldName in pairs({'x','y','w','h'}) do
      bannerParam[fieldName] = self.penaltyBannerGeo[fieldName]
    end

    self.infoPenaltyBanner = ui_banner_create(bannerParam)
    if self.infoPenaltyBanner == nil then
      log(2, 'Unable to create penalty banner')
      return false
    end
  end

  return true
end

local function adjustTextGeometry(self)

  self.infoNumberGeo = {}
  -- W and H come from text texture size
  _,_,self.infoNumberGeo.w,self.infoNumberGeo.h = self.infoNumberTexture:query()
  -- X uses side padding
  self.infoNumberGeo.x = self.infoBgGeo.x + self.infoSidePadding
  -- Y is vertically centered around y of proper line
  self.infoNumberGeo.y = math.round(self.infoBgGeo.y + self.infoFirstTextCenterY - self.infoNumberGeo.h/2)

  self.infoHeightGeo = {}
  -- W and H come from text texture size
  _,_,self.infoHeightGeo.w,self.infoHeightGeo.h = self.infoHeightTexture:query()
  -- X uses side padding
  self.infoHeightGeo.x = self.infoBgGeo.x + self.infoSidePadding
  -- Y is vertically centered around y of proper line
  self.infoHeightGeo.y = math.round(self.infoBgGeo.y + self.infoFirstTextCenterY + self.infoInterTextYOffset - self.infoHeightGeo.h/2)

  self.infoDDGeo = {}
  -- W and H come from text texture size
  _,_,self.infoDDGeo.w,self.infoDDGeo.h = self.infoDDTexture:query()
  -- X uses side padding
  self.infoDDGeo.x = self.infoBgGeo.x + self.infoDDXOffset
  -- Y is vertically centered around y of proper line
  self.infoDDGeo.y = math.round(self.infoBgGeo.y + self.infoFirstTextCenterY + self.infoInterTextYOffset - self.infoDDGeo.h/2)


  if self.displayAwards ~= true then
    self.infoDiveNameGeo = {}
    -- W and H come from text texture size
    _,_,self.infoDiveNameGeo.w,self.infoDiveNameGeo.h = self.infoDiveNameTexture:query()
    -- X uses side padding
    self.infoDiveNameGeo.x = self.infoBgGeo.x + self.infoDiveNameXOffset
    -- Y is vertically centered -- TODO handle multiline
    self.infoDiveNameGeo.y = math.round(self.infoBgGeo.y + (self.infoBgGeo.h - self.infoDiveNameGeo.h)/2)
  end

  if self.displayAwards == true then
    self.infoDiveTotalGeo = {}
    -- W and H come from text texture size
    _,_,self.infoDiveTotalGeo.w,self.infoDiveTotalGeo.h = self.infoDiveTotalTexture:query()
    -- X uses personsBanner (tableRow) side padding (doubled) to right align
    self.infoDiveTotalGeo.x = self.infoBgGeo.x + self.infoBgGeo.w - personsBanner.columnTextSidePadding*2 - self.infoDiveTotalGeo.w
    -- Y is vertically centered
    self.infoDiveTotalGeo.y = math.round(self.infoBgGeo.y + (self.infoBgGeo.h - self.infoDiveTotalGeo.h)/2)
  end

  if self.displayAwards == true then

    if self.synchro == true then
      self.infoAwardSynchroExecLabelGeo = {}
      self.infoAwardSynchroSyncLabelGeo = {}
      -- W and H come from text texture size
      _,_,self.infoAwardSynchroExecLabelGeo.w,self.infoAwardSynchroExecLabelGeo.h = self.infoAwardSynchroExecLabelTexture:query()
      _,_,self.infoAwardSynchroSyncLabelGeo.w,self.infoAwardSynchroSyncLabelGeo.h = self.infoAwardSynchroSyncLabelTexture:query()
      -- X uses side padding
      self.infoAwardSynchroExecLabelGeo.x = self.infoBgGeo.x + self.infoAwardSynchroLabelXOffset
      self.infoAwardSynchroSyncLabelGeo.x = self.infoBgGeo.x + self.infoAwardSynchroLabelXOffset
      -- Y is the same as the Number and DD
      self.infoAwardSynchroExecLabelGeo.y = self.infoNumberGeo.y
      self.infoAwardSynchroSyncLabelGeo.y = self.infoHeightGeo.y
    end

    self.infoAwardGeoArray = {}
    local awardCount = #self.awards
    local nextAwardX = self.infoBgGeo.x + self.infoFirstAwardXOffset
    local interAwardsXOffset
    local layoutParamTable = {wDivisor=layoutDiv, hDivisor=layoutDiv, xDivCount=0, yDivCount=0, hDivCount=0} -- We will generate several width
    if self.synchro == true then
      layoutParamTable.wDivCount = layoutAwardSynchroNextXOffsetRatio
      interAwardsXOffset = self.screenLayout:getScreenFraction(layoutParamTable).w
    else
      layoutParamTable.wDivCount = layoutAwardSoloNextXOffsetRatio
      interAwardsXOffset = self.screenLayout:getScreenFraction(layoutParamTable).w
      -- Handle too many awards for the width
      if type(awardCount) == 'number' then
        local equalSpacingInterAwardXoffset = (self.infoDiveTotalGeo.x - nextAwardX) / (awardCount)
        if interAwardsXOffset >  equalSpacingInterAwardXoffset then
          interAwardsXOffset = equalSpacingInterAwardXoffset -- Reduce the spacing between awards
          -- Reduce the left margin (quick and dirty for now, TODO, do proper math on the award block)
          layoutParamTable.wDivCount = layoutAwardSoloFirstXOffsetRatio * 0.9
          self.infoFirstAwardXOffset = self.screenLayout:getScreenFraction(layoutParamTable).w
          nextAwardX = self.infoBgGeo.x + self.infoFirstAwardXOffset
        end
      end
    end

    for awardId=1,awardCount do
      self.infoAwardGeoArray[awardId] = {}
      -- W and H come from text texture size
      _,_,self.infoAwardGeoArray[awardId].w,self.infoAwardGeoArray[awardId].h = self.infoAwardTextureArray[awardId]:query()
      -- X is horizontaly centered and uses loop variables
      self.infoAwardGeoArray[awardId].x = math.round(nextAwardX + (interAwardsXOffset - self.infoAwardGeoArray[awardId].w)/2)
      if self.synchro == true then
        -- Y is vertically centered on Execution or Synchro label y
        if awardId <= self.executionAwardCount then
          self.infoAwardGeoArray[awardId].y = math.round(self.infoAwardSynchroExecLabelGeo.y + (self.infoAwardSynchroExecLabelGeo.h - self.infoAwardGeoArray[awardId].h)/2)
        else
          self.infoAwardGeoArray[awardId].y = math.round(self.infoAwardSynchroSyncLabelGeo.y + (self.infoAwardSynchroSyncLabelGeo.h - self.infoAwardGeoArray[awardId].h)/2)
        end
      else
        -- Y is vertically centered
        self.infoAwardGeoArray[awardId].y = math.round(self.infoBgGeo.y  + (self.infoBgGeo.h - self.infoAwardGeoArray[awardId].h)/2)
      end

      -- Update variables for next look
      nextAwardX = nextAwardX + interAwardsXOffset

      if self.synchro == true and awardId == self.executionAwardCount then
        -- Start next line (synchronisation awards)
        nextAwardX = self.infoBgGeo.x + self.infoFirstAwardXOffset
      end
    end
  end

  -- TODO, limits w and h in case of text overflow
  return true
end

--- class instance

local self = {}

--- public members

self.personArray = paramTable.personArray or {}
self.rank = paramTable.rank
self.points = paramTable.points
self.diveNumber = paramTable.diveNumber
self.diveHeight = paramTable.diveHeight
self.diveDD = paramTable.diveDD
self.diveName = paramTable.diveName
self.bgColor = paramTable.bgColor or bgColor
self.penaltyBgColor = paramTable.penaltyBgColor or penaltyBgColor
self.textColor = paramTable.textColor or textColor
self.textFadedColor = paramTable.textFadedColor or color_applyAlpha(self.textColor, self.bgColor, fadedTextOpacity)
self.screenLayout= paramTable.screenLayout
if #(paramTable.awards or {}) > 0 then
  self.displayAwards = true
  self.awards = paramTable.awards
  self.awardsUsed = paramTable.awardsUsed
  self.synchro = paramTable.synchro
  self.executionAwardCount = paramTable.executionAwardCount or 99 -- Prevent nil comparison
  self.diveTotal = paramTable.diveTotal
end
self.penalty = paramTable.penalty

if computeGeometry(self) ~= true then
  log(2,'Unable to instanciate diveCard (Failed to compute geometry)')
  return nil
end

if createSdlObjects(self) ~= true then
  log(2,'Unable to instanciate diveCard (Failed to create Sdl objects)')
  return nil
end

if adjustTextGeometry(self) ~= true then
  log(2,'Unable to instanciate diveCard (Failed to adjust text geometry)')
  return nil
end

--- public methods

function self:draw()
  personsBanner:draw()

  -- Info background
  ui_renderer_renderer:copy(self.backgroundTexture, self.backgroundSrcRect, self.infoBgGeo)

  -- Info Text
  if self.infoNumberTexture ~= nil then
    ui_renderer_renderer:copy(self.infoNumberTexture, nil, self.infoNumberGeo)
  end
  if self.infoHeightTexture ~= nil then
    ui_renderer_renderer:copy(self.infoHeightTexture, nil, self.infoHeightGeo)
  end
  if self.infoDDTexture ~= nil then
    ui_renderer_renderer:copy(self.infoDDTexture, nil, self.infoDDGeo)
  end
  if self.infoDiveNameTexture ~= nil then
    ui_renderer_renderer:copy(self.infoDiveNameTexture, nil, self.infoDiveNameGeo)
  end

  if self.infoDiveTotalTexture ~= nil then
    ui_renderer_renderer:copy(self.infoDiveTotalTexture, nil, self.infoDiveTotalGeo)
  end

  -- Info awards
  if self.infoAwardSynchroExecLabelTexture ~= nil then
    ui_renderer_renderer:copy(self.infoAwardSynchroExecLabelTexture, nil, self.infoAwardSynchroExecLabelGeo)
  end
  if self.infoAwardSynchroSyncLabelTexture ~= nil then
    ui_renderer_renderer:copy(self.infoAwardSynchroSyncLabelTexture, nil, self.infoAwardSynchroSyncLabelGeo)
  end

  if self.infoAwardTextureArray ~= nil then
    for awardTextureId=1,#self.infoAwardTextureArray do
      if self.infoAwardTextureArray[awardTextureId] ~= nil then
        ui_renderer_renderer:copy(self.infoAwardTextureArray[awardTextureId], nil, self.infoAwardGeoArray[awardTextureId])
      end
    end
  end

  -- Info penalty
  if self.infoPenaltyBanner ~= nil then
    self.infoPenaltyBanner:draw()
  end

end

return self
end
