--- UI ticker row graphic object
-- Single or multi line text over a solid background rectangle with a rank and a point total
-- Lua Class

--- Requires

require 'ui.fontManager'
require 'ui.screenLayout'

--- Constants

-- Layout constants
local textAlign = 'left'
local layoutDiv = 10
local layoutBgExtendToLeftSide = true
local layoutBgTrigramOnlyWDiv = 0
local layoutBgFullWDiv = 3/4
local layoutRankWDiv = 0.3
local sidePadingRatioPoint = 1/2 
local layoutXDivPos = 0
local layoutYDivPos = 0 -- Default value, overriden by paramTable
local layoutPersonHBaseRatio = 1/2
local layoutPersonHExtraPersonRatio = layoutPersonHBaseRatio / 2

local fontRank = 'bold'
local fontTrigram = 'medium'
local fontPoints = 'light'
-- Ratio to full size text
local fontSizeRatioRank = 1
local fontSizeRatioPoints = 3/4

local textColor=0xFFFFFF
local bgColor=0x000000

--- Class constructor

--- @param #table paramTable {rankText, trigramTextArray, pointsText, textColor, bgColor, textAlign, screenLayout, yDivPos, trigramOnly}
function ui_tickerRow_create(paramTable)

--- private members

--- private methods

local function computeGeometry(self)
  if self.screenLayout == nil then
    log(2,'No screenLayout provided')
    return false
  end

  -- Use layout to compute bg geo
  local layoutParamTable = {wDivisor=layoutDiv, hDivisor=layoutDiv}
  layoutParamTable.extendToLeftEdge = layoutBgExtendToLeftSide
  layoutParamTable.xDivCount = layoutXDivPos
  layoutParamTable.yDivCount = self.yDivPos
  if self.trigramOnly == true then
    layoutParamTable.wDivCount = layoutBgTrigramOnlyWDiv
  else
    layoutParamTable.wDivCount = layoutBgFullWDiv
  end
  layoutParamTable.hDivCount = layoutPersonHBaseRatio + (self.trigramCount - 1) * layoutPersonHExtraPersonRatio
  self.nextYDivCount = layoutParamTable.yDivCount + layoutParamTable.hDivCount

  self.backgroundGeo = self.screenLayout:getScreenFraction(layoutParamTable)
  for _,fieldName in pairs({'x','y','w','h'}) do  -- Copy object geometry
    self[fieldName] = self.backgroundGeo[fieldName] 
  end

  -- Use layout to know single trigram bg height (for rank and points fontManager autoSize)
  layoutParamTable.hDivCount = layoutPersonHBaseRatio
  self.singleTrigramBgH = self.screenLayout:getScreenFraction(layoutParamTable).h

  -- Compute area of the background used for the rank
  if self.trigramOnly ~= true then
    layoutParamTable.extendToLeftEdge = false
    layoutParamTable.wDivCount = layoutRankWDiv
    self.rankBgW = self.screenLayout:getScreenFraction(layoutParamTable).w
  end

  -- Text font sizes
  local fullFontSize
  local firstTextCenterY, interTextYOffset
  fullFontSize, firstTextCenterY, interTextYOffset, self.trigramSidePadding = ui_fontManager_autoSize(fontTrigram, self.backgroundGeo.h, self.trigramCount)
  self.trigramFontSizeNumber = math.round(fullFontSize)
  self.trigramCenterYOffsetFloatArray = {}
  for trigramId=1,self.trigramCount do
    self.trigramCenterYOffsetFloatArray[trigramId] = firstTextCenterY + interTextYOffset * (trigramId-1)
  end

  if self.trigramOnly ~= true then
    fullFontSize, _, _, self.rankSidePadding = ui_fontManager_autoSize(fontRank, self.singleTrigramBgH, 1)
    self.rankFontSizeNumber = math.round(fullFontSize * fontSizeRatioRank)
  
    fullFontSize, _, _, self.pointsSidePadding = ui_fontManager_autoSize(fontPoints, self.singleTrigramBgH, 1)
    self.pointsFontSizeNumber = math.round(fullFontSize * fontSizeRatioPoints)
  end

  return true
end

local function createSdlObjects(self)

  -- Background rectangle
  local err
  self.backgroundSurface, err = SDLImage.load("resources/White.bmp")
  if err then
    log(2,'Unable to open image ('.. (err or 'nil') ..')')
    return false
  end
  self.backgroundSurface:fillRect(nil,self.bgColor)
  self.backgroundTexture = ui_renderer_renderer:createTextureFromSurface(self.backgroundSurface)
  self.backgroundSrcRect = {x=0,y=0,w=10,h=10}

  -- Text fonts
  self.trigramFont = ui_fontManager_getFont(fontTrigram, self.trigramFontSizeNumber)
  if self.trigramOnly ~= true then
    self.rankFont = ui_fontManager_getFont(fontRank, self.rankFontSizeNumber)
    self.pointsFont = ui_fontManager_getFont(fontPoints, self.pointsFontSizeNumber)
  end

  -- Text textures
  local textSurface, err

  for trigramId,trigramText in ipairs(self.trigramTextArray) do
    self.trigramTextureArray = self.trigramTextureArray or {}
    textSurface, err = self.trigramFont:renderUtf8(trigramText, "blended", self.textColor)
    if textSurface == nil then
      log(2, "Error when rendering " .. (trigramId or 'nil') .. "-th trigram '" .. (self.trigramText or 'nil') .. "' : " .. (err or 'nil'))
      return false
    end
    self.trigramTextureArray[trigramId] = ui_renderer_renderer:createTextureFromSurface(textSurface)
  end

  if self.trigramOnly ~= true and self.rankText ~= nil then
    textSurface, err = self.rankFont:renderUtf8(self.rankText, "blended", self.textColor)
    if textSurface == nil then
      log(2, "Error when rendering rank '" .. (self.rankText or 'nil') .. "' : " .. (err or 'nil'))
      return false
    end
    self.rankTexture = ui_renderer_renderer:createTextureFromSurface(textSurface)

    textSurface, err = self.pointsFont:renderUtf8(self.pointsText, "blended", self.textColor)
    if textSurface == nil then
      log(2, "Error when rendering points '" .. (self.pointsText or 'nil') .. "' : " .. (err or 'nil'))
      return false
    end
    self.pointsTexture = ui_renderer_renderer:createTextureFromSurface(textSurface)
  end

  return true
end

local function adjustTextGeometry(self)

  -- trigram geo
  for trigramId,trigramTexture in ipairs(self.trigramTextureArray) do
    self.trigramGeoArray = self.trigramGeoArray or {}
    self.trigramGeoArray[trigramId] = {}
    -- W and H come from text texture size
    _,_,self.trigramGeoArray[trigramId].w,self.trigramGeoArray[trigramId].h = trigramTexture:query()
    if self.trigramOnly == true then
      -- X uses side padding from left edge of ticker row
      self.trigramGeoArray[trigramId].x = self.x + self.trigramSidePadding
    else
      -- X start just after the rank bg area (no padding)
      self.trigramGeoArray[trigramId].x = self.x + self.rankBgW
    end
    -- Y is vertically centered around self.y + textCenterYOffsetFloat[textId]
    self.trigramGeoArray[trigramId].y = math.round(self.y + self.trigramCenterYOffsetFloatArray[trigramId] - self.trigramGeoArray[trigramId].h/2)
  end

  -- rank geo
  if self.trigramOnly ~= true and self.rankTexture ~= nil then
    self.rankGeo = {}
    -- W and H come from text texture size
    _,_,self.rankGeo.w,self.rankGeo.h = self.rankTexture:query()
    -- Limit text W in case of text overflow
    self.rankGeo.w = math.min(self.rankGeo.w, self.rankBgW)
    -- Center in rank area
    self.rankGeo.x = math.round(self.x + (self.rankBgW - self.rankGeo.w)/2)
    -- Y is vertically centered over background
    self.rankGeo.y = math.round(self.backgroundGeo.y + (self.backgroundGeo.h - self.rankGeo.h)/2)
  end

  if self.trigramOnly ~= true and self.pointsTexture ~= nil then
    self.pointsGeo = {}
    -- W and H come from text texture size
    _,_,self.pointsGeo.w,self.pointsGeo.h = self.pointsTexture:query()
    -- Right align
    self.pointsGeo.x = math.round(self.x + self.w - self.pointsGeo.w - self.pointsSidePadding * sidePadingRatioPoint)
    -- Y is vertically centered over background
    self.pointsGeo.y = math.round(self.backgroundGeo.y + (self.backgroundGeo.h - self.pointsGeo.h)/2)
  end

  return true
end

--- class instance

local self = {}

--- public members

self.rankText = paramTable.rankText
self.trigramTextArray = paramTable.trigramTextArray
self.pointsText = paramTable.pointsText
self.textColor = paramTable.textColor or textColor
self.bgColor = paramTable.bgColor or bgColor
self.textAlign = paramTable.textAlign or textAlign
self.screenLayout = paramTable.screenLayout
self.yDivPos = paramTable.yDivPos or layoutYDivPos
self.trigramOnly = paramTable.trigramOnly or false

if type(self.trigramTextArray) == 'string' then
  self.trigramTextArray = {self.trigramTextArray} -- Handle single trigram
elseif type(self.trigramTextArray) ~= 'table' then
  self.trigramTextArray = {''} -- Protect against no trigram
end
self.trigramCount = #self.trigramTextArray

if computeGeometry(self) ~= true then
  log(2,'Unable to instanciate tickerRow (Failed to compute geometry)')
  return nil
end

if createSdlObjects(self) ~= true then
  log(2,'Unable to instanciate tickerRow (Failed to create Sdl objects)')
  return nil
end

if adjustTextGeometry(self) ~= true then
  log(2,'Unable to instanciate tickerRow (Failed to adjust text geometry)')
  return nil
end


--- public methods

function self:getNextYDivCount()
  return self.nextYDivCount or 0
end

function self:draw()
  -- Background
  ui_renderer_renderer:copy(self.backgroundTexture, self.backgroundSrcRect, self.backgroundGeo)

  -- Texts
  if self.rankTexture ~= nil then
    ui_renderer_renderer:copy(self.rankTexture, nil, self.rankGeo)
  end

  if self.trigramTextureArray ~= nil then
    for trigramId,trigramTexture in ipairs(self.trigramTextureArray) do
      if self.trigramGeoArray[trigramId] ~= nil then
        ui_renderer_renderer:copy(self.trigramTextureArray[trigramId], nil, self.trigramGeoArray[trigramId])
      end
    end
  end

  if self.pointsTexture ~= nil then
    ui_renderer_renderer:copy(self.pointsTexture, nil, self.pointsGeo)
  end
end

return self
end
