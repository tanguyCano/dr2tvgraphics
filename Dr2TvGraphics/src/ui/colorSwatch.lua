--- Object in charge of color allocation
-- Manage all colors

--- Requires

require 'utils.misc'
local config = require 'config'

--- Constants

local defaultColorTxt = 0xFFFFFF
local defaultColorBg = 0x000000
local defaultTextFadedAlphaRatio = 2/3

-- base color to object properties lookup. [object][property] = {colorKey or colorKey1+colorKey2+fadedRatioKey}
local objectPropertyConfigLookup =
{
  ['eventInfo'] =
  {
    ['bg'] =        {colorKey='bg1'},
    ['txt'] =       {colorKey='txt1'},
  },
  ['eventName'] =
  {
    ['bg'] =        {colorKey='bg2'},
    ['txt'] =       {colorKey='txt1'},
    ['txtFaded'] =  {colorKey1='txt1', colorKey2='bg2', fadedRatioKey='txtAlphaRatio'},
  },
  ['ticker'] =
  {
    ['bg'] =        {colorKey='bg1'},
    ['txt'] =       {colorKey='txt1'},
    ['txtFaded'] =  {colorKey1='txt1', colorKey2='bg1', fadedRatioKey='txtAlphaRatio'},
  },
  ['time'] =
  {
    ['bg'] =        {colorKey='bg1'},
    ['txt'] =       {colorKey='txt1'},
  },
  ['diveCard'] =
  {
    ['bg'] =        {colorKey='bg1'},
    ['bgPenalty'] = {colorKey='bg3'},
    ['txt'] =       {colorKey='txt1'},
    ['txtFaded'] =  {colorKey1='txt1', colorKey2='bg1', fadedRatioKey='txtAlphaRatio'},
  },
  ['table'] =
  {
    ['bgTitle1'] =  {colorKey='bg1'},
    ['bgTitle2'] =  {colorKey='bg3'},
    ['bgRow'] =     {colorKey='bg1'},
    ['bgFooter'] =  {colorKey='bg2'},
    ['txt'] =       {colorKey='txt1'},
  },
  ['medal'] =
  {
    ['bgPerson'] =  {colorKey='bg1'},
    ['bgMedal'] =   {colorKey='bg2'},
    ['txt'] =       {colorKey='txt1'},
  },
}

--- Private variables

local colorMap = nil

--- Private functions

--- Public functions

function ui_colorSwatch_init()
  -- Load config base colors
  local baseColor = {}
  baseColor.txt1 = config.Ui.colorSwatch.baseColorTxt1 or defaultColorTxt
  baseColor.bg1 = config.Ui.colorSwatch.baseColorBg1 or defaultColorBg
  baseColor.bg2 = config.Ui.colorSwatch.baseColorBg2 or defaultColorBg
  baseColor.bg3 = config.Ui.colorSwatch.baseColorBg3 or defaultColorBg
  baseColor.txtAlphaRatio = config.Ui.colorSwatch.baseFadedTxtAlphaRatio or defaultTextFadedAlphaRatio

  -- Build colorMap table
  colorMap = {}
  if type(objectPropertyConfigLookup) ~= 'table' then
    log(2, 'No objectPropertyLookup, unable to build colorSwatch colorMap')
    return
  end
  log(3, 'Building colorSwatch colorMap')
  for object,propertyArray in pairs(objectPropertyConfigLookup) do
    log(4, 'Building colorMap properties for object', object)
    -- Create the object in the color map
    colorMap[object] = colorMap[object] or {}
    for property,colorRuleTable in pairs(propertyArray or {}) do
      log(4, 'Building properties (object, property)', object, property)
      -- Attempt to build the property
      if type(colorRuleTable) == 'table' then

        if type(colorRuleTable.colorKey) == 'string' then
          if baseColor[colorRuleTable.colorKey] == nil then
            log(2, 'Invalid color rule colorKey (object, property, colorKey)', object, property, colorRuleTable.colorKey)
          else
            colorMap[object][property] = baseColor[colorRuleTable.colorKey]
          end
        end -- colorKey rule

        if type(colorRuleTable.colorKey1) == 'string' and type(colorRuleTable.colorKey2) == 'string' and type(colorRuleTable.fadedRatioKey) == 'string' then
          if baseColor[colorRuleTable.colorKey1] == nil or baseColor[colorRuleTable.colorKey2] == nil or baseColor[colorRuleTable.fadedRatioKey] == nil then
            log(2, 'Invalid color rule colorKey (object, property, colorKey1, colorKey2, fadedRatioKey)', object, property, colorRuleTable.colorKey1, colorRuleTable.colorKey2, colorRuleTable.fadedRatioKey)
          else
            colorMap[object][property] = color_applyAlpha(baseColor[colorRuleTable.colorKey1], baseColor[colorRuleTable.colorKey2], baseColor[colorRuleTable.fadedRatioKey])
          end
        end -- fadedRatioKey rule

      end -- colorRuleTable is a table
    end
  end
end

function ui_colorSwatch_get(object, property)
  if type(object) ~= 'string' or type(property) ~= 'string' then
    log(2, 'Invalid color get parameter types (object, property)', type(object), type(property))
    return 0xFFFFFF -- Return a valid color
  end
  if type(colorMap) == 'table' then
    if type(colorMap[object]) == 'table' and type(colorMap[object][property]) == 'number' then
      return colorMap[object][property]
    else
      log(2, 'No color for parameters (object, property)', object, property)
    end
  else
    log(2, 'No color map, has the colorSwatch been initialized?')
  end
  return 0xFFFFFF -- Return a valid color
end
