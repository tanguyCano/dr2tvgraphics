--- UI banner plus graphic object
-- Single or multi line text over a solid background rectangle, lead by optional flag and/or prefix
-- Lua Class, inherit from banner

--- Requires

require 'ui.fontManager'
require 'ui.banner'
require 'ui.flag'

--- Constants

local fontSizeRatio = 2/3 -- Text to prefix font ratio

--- Class constructor

--- @param #table paramTable {prefix or prefixArray, flagKey or flagKeyArray, [+ all banner]}
function ui_bannerPlus_create(paramTable)

--- private members

--- private methods

local function createSdlObjects(self)
  -- Derive font size from parent object one
  if type(self.fontSize) ~= 'number' then
    log(2,'Parent class did not set font size')
    return false
  end

  -- Instantiate prefix font
  self.prefixFontSize = math.round(self.fontSize * fontSizeRatio)
  self.prefixFont = ui_fontManager_getFont(self.fontName, self.prefixFontSize)
  if self.prefixFont == nil then
    return false
  end

  -- Prefix text textures
  self.prefixTextTextureArray = {}
  if self.prefixArray ~= nil then
    for textId=1,self.textCount do
      local textSurface, err = self.prefixFont:renderUtf8(self.prefixArray[textId], "blended", self.textColor)
      if textSurface == nil then
        log(2, "Error when rendering text id " .. (textId or 'nil') .. " '" .. (self.prefixArray[textId] or 'nil') .. "' : " .. (err or 'nil'))
        return false
      end
      self.prefixTextTextureArray[textId] = ui_renderer_renderer:createTextureFromSurface(textSurface)
    end
  end -- Optional prefix texts

  -- Flag composite objects
  if self.flagKeyArray ~= nil then
    self.flagObjectArray = {}
    for flagId=1,self.textCount do
      -- TODO, check number of entries matches between flag and text in parent class
      self.flagObjectArray[flagId] = ui_flag_create({flagKey=self.flagKeyArray[flagId]})
    end
  end

  return true
end

local function computeGeometry(self)
  -- Note: banner createSdlObjects() already computed self.sidePadding and self.textCenterYOffsetFloat[]
  --       banner computeGeometry() already computed self.backgroundGeo, self.textGeo and self.textGeo[]


  -- Compute the position and size of each of the flags and prefixText (if any)
  for textId=1,self.textCount do

    local currentFlagGeo = {w=0} -- ensure currentFlagGeo and currentFlagGeo.w can be safely used in the loop context
    if self.flagKeyArray ~= nil then
      -- TODO, check number of entries matches between flag and text in parent class
      self.flagObject = self.flagObject or {} -- Create array it doesn't exists yet

      -- H comes from reference text height
      currentFlagGeo.h = self.textReferenceTextureHeight or self.textGeo[1].h
      -- For now, apply flag ratio to get W (TODO, let the flag decide of this once ratio are handled natively)
      currentFlagGeo.w = math.round(currentFlagGeo.h * 3/2)

      -- Y is vertically centered around y + textCenterYOffsetFloat[textId]
      currentFlagGeo.y = math.round(self.y + self.textCenterYOffsetFloat[textId] - currentFlagGeo.h/2)

      -- X is based on alignment (see at the bottom of the loop)
    end -- Flag geo

  -- Compute the position and size of each of the prefix text (if any)
    if self.prefixArray ~= nil then
      -- TODO, check number of entries matches between prefix and text in parent class
      self.prefixTextGeo = self.prefixTextGeo or {} -- Create array it doesn't exists yet
      self.prefixTextGeo[textId] = {}

      -- W and H come from text texture size
      _,_,self.prefixTextGeo[textId].w,self.prefixTextGeo[textId].h = self.prefixTextTextureArray[textId]:query()

      -- Y is vertically centered around y + textCenterYOffsetFloat[textId]
      self.prefixTextGeo[textId].y = math.round(self.y + self.textCenterYOffsetFloat[textId] - self.prefixTextGeo[textId].h/2)

      -- X is based on alignment (see at the bottom of the loop)
    end -- prefixText geo

    -- Deal with flag, prefixText and text horizontal alignement (influence each others)
    if self.textAlign == 'left' then
      -- Flag (if any) then prefixText (if any) then text, all left aligned and separated by sidePadding
      if currentFlagGeo.w > 0 then -- If there's a flag
        currentFlagGeo.x = self.textGeo[textId].x -- place flag where the text used to be
        self.textGeo[textId].x = currentFlagGeo.x + currentFlagGeo.w + self.sidePadding -- move text
      end
      if self.prefixTextGeo and self.prefixTextGeo[textId] then -- If there's a prefixText
        self.prefixTextGeo[textId].x = self.textGeo[textId].x -- place prefixText where the text used to be
        self.textGeo[textId].x = self.prefixTextGeo[textId].x + self.prefixTextGeo[textId].w + self.sidePadding -- move text
      end

    elseif self.textAlign == 'right' then
      -- Flag (if any) then prefixText (if any) then text, all right aligned and separated by sidePadding
      local prevElementX = self.textGeo[textId].x
      if self.prefixTextGeo and self.prefixTextGeo[textId] then -- If there's a prefixText
        self.prefixTextGeo[textId].x = prevElementX - self.prefixTextGeo[textId].w - self.sidePadding -- place prefixText
        prevElementX = self.prefixTextGeo[textId].x
      end
      if currentFlagGeo.w > 0 then -- If there's a flag
        currentFlagGeo.x = prevElementX - currentFlagGeo.w - self.sidePadding -- place flag
        prevElementX = currentFlagGeo.x
      end

    else -- center
      -- Flag (if any) then club (if any) left aligned
      local nextElementX = self.x + self.sidePadding
      if currentFlagGeo.w > 0 then -- If there's a flag
        currentFlagGeo.x = nextElementX -- place flag
        nextElementX = nextElementX + currentFlagGeo.w + self.sidePadding
      end
      if self.prefixTextGeo and self.prefixTextGeo[textId] then -- If there's a prefixText
        self.prefixTextGeo[textId].x = nextElementX -- place prefixText
        nextElementX = nextElementX + self.prefixTextGeo[textId].w + self.sidePadding
      end
      if nextElementX > self.textGeo[textId].x then -- If the above left aligned objects overlap with the center aligned text
        self.textGeo[textId].x = nextElementX -- move text
      end
    end

    -- Apply to flag object as we're using it instead of a separate geometry table
    if self.flagObjectArray and self.flagObjectArray[textId] then
      self.flagObjectArray[textId]:setGeometry(currentFlagGeo)
    end

    -- limit text w in case of text overflow
    if self.textGeo and self.textGeo[textId] then
      local maxTextW = self.x + self.w - self.textGeo[textId].x - self.sidePadding
      self.textGeo[textId].w = math.min(self.textGeo[textId].w, maxTextW)
    end

  end -- For each text in banner

  return true
end

--- class instance

local self = ui_banner_create(paramTable) -- Construct parent class
if self == nil then -- Catch parent construction failure
  log(2,'Unable to instanciate bannerPlus (Parent class constructor failed)')
  return nil
end

--- public members

if type(paramTable.prefix) == 'string' then
  self.prefixArray = {[1]=paramTable.prefix} -- Build a single entry prefixArray
  -- TODO, warning if param.prefixArray is also set
else
  self.prefixArray = paramTable.prefixArray
  -- TODO protect against invalid table content
end
if type(paramTable.flagKey) == 'string' then
  self.flagKeyArray = {[1]=paramTable.flagKey} -- Build a single entry flagKeyArray
  -- TODO, warning if param.flagKeyArray is also set
else
  self.flagKeyArray = paramTable.flagKeyArray
  -- TODO protect against invalid table content
end

if createSdlObjects(self) ~= true then
  log(2,'Unable to instanciate bannerPlus (Failed to create Sdl objects)')
  return nil
end

if computeGeometry(self) ~= true then
  log(2,'Unable to instanciate bannerPlus (Failed to compute geometry)')
  return nil
end

--- public methods
self.parentBannerDraw = self.draw -- Keep a reference to parent draw
function self:draw()
  self:parentBannerDraw()

  -- Copy prefix text textures
  if self.prefixArray ~= nil then
    for textId=1,self.textCount do
      ui_renderer_renderer:copy(self.prefixTextTextureArray[textId], nil, self.prefixTextGeo[textId])
    end
  end

  -- Draw flags
  if self.flagObject ~= nil then
    for flagId=1,self.textCount do
      if self.flagObjectArray[flagId] ~= nil then
        self.flagObjectArray[flagId]:draw()
      end
    end
  end
end

return self
end
