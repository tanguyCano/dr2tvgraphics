--- Object in charge of SDL window/renderer setup
-- Manage all rendering

--- Requires

require 'utils.misc'
require 'utils.table'
local config = require 'config'

--- Constants

local renderX = 0
local renderY = 0
local renderW = 100
local renderH = 100

--- Private variables

local rendererWindow, rendererRenderer, rendererTimer, rendererDrawList
local rendererActive = false
ui_renderer_renderer = nil -- Global

--- Private functions

-- TODO, try to use a closure to 'cache' the requires?
local function ui_renderer_frametickTimerCb(interval)
  require 'threads.utils'
  currentThreadId = 'ui' -- Emulate the message being sent to ui
  local SDL = require "SDL"
  local config = require 'config' -- TODO, not ideal, this will parse some lua file every frametick
  SDL.init()
  local currentTick = SDL.getTicks()
  local tickPerFrame = 1000 / config.Ui.framerate
  local delta = SDL.getTicks() / tickPerFrame
  delta = delta - math.floor(delta)

  if (delta <0.45) then
    interval = tickPerFrame - 1
  elseif (delta > 0.55) then
    interval = tickPerFrame + 1
  else
    interval = tickPerFrame
  end

  threads_utils_sendMessage('ui',threads_message_build('UI_FRAMETICK'))
  return interval
end

--- Public functions

function ui_renderer_init()
  -- Attempt to start graphic stack of SDL
  local ret, err = SDL.init({ SDL.flags.Video })
  if ret == nil then
    log(1, 'Unable to SDL.init('..(err or 'nil')..')')
    return false
  end

  local ret, err = SDLFont.init()
  if ret == nil then
    log(1, 'Unable to SDLFont.init('..(err or 'nil')..')')
    return false
  end

  rendererActive = false
  ui_renderer_renderer = nil
end

function ui_renderer_configure()
  -- Retrieve last screen bounds
  local screenCount, err = SDL.getNumVideoDisplays()
  if type(screenCount) ~= 'number' then
    log(2, 'Unable to get screen count ('..(err or 'nil')..')')
    --TODO, handle complete failure (restore default values)
    return false
  end

  log(3, 'Number of screens detected: '..(screenCount or 'nil'))
  local lastScreenBounds = SDL.getDisplayBounds(screenCount-1)
  if type(lastScreenBounds) ~= 'table' then
    log(2, 'Unable to get last screen display bounds')
    --TODO, handle complete failure (restore default values)
    return false
  end

  log(3, 'Last detected screen bounds: '..
         (lastScreenBounds.w or 'nil')..'x'..(lastScreenBounds.h or 'nil')..
         ' @ '..(lastScreenBounds.x or 'nil')..','..(lastScreenBounds.y or 'nil'))

  renderX = lastScreenBounds.x
  renderY = lastScreenBounds.y
  renderW = lastScreenBounds.w
  renderH = lastScreenBounds.h
  return true
end

function ui_renderer_start()
  if rendererActive == true then
    log(2, "Unable to start renderer (already started)")
    return false
  end

  log(4, "Start renderer")
  -- Create SDL output window
  local err = nil
  rendererWindow, err = SDL.createWindow(
    {
      title   = "Dr2TvGraphics",
      x       = renderX,
      y       = renderY,
      width   = renderW,
      height  = renderH,
      flags = {SDL.window.Borderless}
    })
  if rendererWindow == nil then
    log(2, 'Unable to create renderer window, createWindow failed ('..(err or 'nil')..')')
    return false
  end
  SDL.showCursor(false) -- Ensure no mouse cursor is displayed

  -- Create SDL renderer
  rendererRenderer, err = SDL.createRenderer(rendererWindow, 0, 0)
  if rendererRenderer == nil then
    log(2, 'Unable to create renderer renderer, createRenderer failed ('..(err or 'nil')..')')

    -- Clean already created object (window)
    ui_renderer_stop()
    return false
  end
  rendererRenderer:setDrawColor(config.Ui.colorKey)

  -- Create SDL timer for each frametick (if using framerate)
  if config.Ui.useFramerate == true then
    rendererTimer, err = SDL.addTimer(math.ceil(1000/config.Ui.framerate), ui_renderer_frametickTimerCb)
    if rendererTimer == nil then
      log(2, 'Unable to create renderer timer, addTimer failed ('..(err or 'nil')..')')

      -- Clean already created object (window and renderer)
      ui_renderer_stop()
      return false
    end
  end

  -- Create draw list
  rendererDrawList = {}

  ui_renderer_renderer = rendererRenderer

  rendererActive = true
  return true
end

function ui_renderer_stop()
  log(4, "Stop renderer")

  -- Destroy SDL timer renderer and window
  if renderTimer ~= nil then
    renderTimer:remove()
    renderTimer = nil
  end
  if rendererRenderer ~= nil then
    rendererRenderer = nil -- next gc cycle will trigger SDL_DestroyRenderer
  end
  ui_renderer_renderer = nil
  if rendererWindow ~= nil then
    rendererWindow:hide()
    rendererWindow = nil -- next gc cycle will trigger SDL_DestroyWindow
  end

  -- Clear the drawList
  -- TODO, notify objects in the list?
  rendererDrawList = nil

  rendererActive = false
end

function ui_renderer_frametick()
  if rendererActive ~= true then
    log(2, 'Received frametick despite renderer not being started')
    return false
  end

  if rendererRenderer ~= nil then
    rendererRenderer:clear()
    -- Draw all objects
    for i=1,#rendererDrawList do
      -- Assumes the object has a draw method (checked by ui_renderer__add)
      rendererDrawList[i]:draw()
    end
    rendererRenderer:present()
  end
end

function ui_renderer_removeAll()
  if rendererActive ~= true then
    log(2, 'Object added despite renderer not being started')
    return false
  end

  for k in ipairs(rendererDrawList) do
    rendererDrawList[k] = nil
  end
end

function ui_renderer_removeMany(arrayOfDrawableObjectToRemove)
  if rendererActive ~= true then
    log(2, 'Objects removed despite renderer not being started')
    return false
  end

  local initialRendererDrawListLength = #(rendererDrawList or {})
  -- Remove (creates holes in array)
  for i=1,initialRendererDrawListLength do
    if table_find(rendererDrawList[i],arrayOfDrawableObjectToRemove) == true then
      rendererDrawList[i] = nil
    end
  end

  -- Remove holes in array
  for i=initialRendererDrawListLength,2,-1 do
    if rendererDrawList[i-1] == nil then
      rendererDrawList[i-1] = rendererDrawList[i] -- Shift left to fill hole
    end
  end

  -- TODO, what should we return?
end

function ui_renderer_addMany(arrayOfDrawableObjectsToAdd)
  -- Call ui_renderer_add on all array entries
  if type(arrayOfDrawableObjectsToAdd) ~= 'table' or #arrayOfDrawableObjectsToAdd == 0 then
    log(2, 'ui_renderer_addMany expects a non-empty array (type)', type(arrayOfDrawableObjectsToAdd))
    return false
  end

  local allAddSucceeded = true -- will be overriden when one fails
  for i,object in ipairs(arrayOfDrawableObjectsToAdd) do -- Try to add them all
    local added = ui_renderer_add(object)
    if added == false then
      allAddSucceeded = false
    end
  end
  return allAddSucceeded
end

function ui_renderer_add(drawableObjectToAdd)
  if rendererActive ~= true then
    log(2, 'Object added despite renderer not being started')
    return false
  end

  -- Return early if not a table with a draw() method
  if drawableObjectToAdd == 'nil' then
    return false
  end
  if type(drawableObjectToAdd) ~= 'table' or type(drawableObjectToAdd.draw) ~= 'function' then
    log(2, "Object to add to renderer isn't a drawable object, skipping")
    return false
  end

  -- Add to draw list
  table.insert(rendererDrawList, drawableObjectToAdd)
  return true
end

--- @return w,h
function ui_renderer_getScreenSize()
  return renderW, renderH
end
