--- UI table row graphic object
-- Single or multi line text over a solid background rectangle, lead by optional flag and/or prefix, surrounded by optional first and last column
-- Lua Class, inherit from bannerPlus

--- Requires

require 'ui.fontManager'
require 'ui.bannerPlus'

--- Constants

--- Class constructor

--- @param #table paramTable {firstColumnText, lastColumnText, [prefixXOffset, textXOffset, columnFontName, columnTextColor, lastColumnFontSizeRatio], [+ all bannerPlus]}
function ui_tableRow_create(paramTable)

--- private members

--- private methods

local function createSdlObjects(self)

  -- Use fontManager to compute the ideal font size and sidePadding if the case of a single text row
  local singleTextHeight = self.h / (self.textCount + 1) * 2
  self.firstColumnFontSize, _, _, self.columnTextSidePadding = ui_fontManager_autoSize(self.columnFontName, singleTextHeight, 1)

  if self.firstColumnFontSize == nil then
    log(2,'Unable to autoSize tableRow (font: '.. (self.columnFontName or 'nil') ..', h:'.. (singleTextHeight or 'nil') ..' )')
    return false
  end

  -- Instantiate prefix font
  self.firstColumnFont = ui_fontManager_getFont(self.columnFontName, self.firstColumnFontSize)
  if self.firstColumnFont == nil then
    return false
  end

  -- Instantiate suffix font
  self.lastColumnFont = ui_fontManager_getFont(self.columnFontName, math.round(self.firstColumnFontSize*self.lastColumnFontSizeRatio))
  if self.lastColumnFont == nil then
    return false
  end

  -- column text textures
  if self.firstColumnText ~= nil then
    local textSurface, err = self.firstColumnFont:renderUtf8(self.firstColumnText, "blended", self.columnTextColor)
    if textSurface == nil then
      log(2, "Error when rendering column text '" .. (self.firstColumnText or 'nil') .. "' : " .. (err or 'nil'))
      return false
    end
    self.firstColumnTextTexture = ui_renderer_renderer:createTextureFromSurface(textSurface)
  end
  if self.lastColumnText ~= nil then
    local textSurface, err = self.lastColumnFont:renderUtf8(self.lastColumnText, "blended", self.columnTextColor)
    if textSurface == nil then
      log(2, "Error when rendering column text '" .. (self.lastColumnText or 'nil') .. "' : " .. (err or 'nil'))
      return false
    end
    self.lastColumnTextTexture = ui_renderer_renderer:createTextureFromSurface(textSurface)
  end

  return true
end

local function computeGeometry(self)
  -- Note: banner createSdlObjects() already computed self.sidePadding and self.textCenterYOffsetFloat[]
  --       banner computeGeometry() already computed self.backgroundGeo, self.textGeo and self.textGeo[]
  --       bannerPlus createSdlObjects() self.flagObjectArray (owns its geometry)
  --       banner computeGeometry() already computed self.prefixTextGeo and updated self.flagObjectArray internal geometry

  -- If prefixXOffset is set, override flag and prefixText geometry (ignore alignment)
  if self.prefixXOffset ~= nil then
    for textId=1,self.textCount do
      -- Flag ends on prefixXOffset, prefix text starts after padding and offset
      if self.prefixTextGeo and self.prefixTextGeo[textId] then
        self.prefixTextGeo[textId].x = self.x + self.prefixXOffset + self.sidePadding
      end
      if self.flagObjectArray and self.flagObjectArray[textId] then
        local geo = self.flagObjectArray[textId]:getGeometry()
        geo.x = self.x + self.prefixXOffset - geo.w
        self.flagObjectArray[textId]:setGeometry(geo)
      end
    end
  end

  -- If textXOffset is set, override text geometry (ignore alignment)
  if self.textXOffset ~= nil then
    for textId=1,self.textCount do
      if self.textGeo and self.textGeo[textId] then
        -- text starts after padding and offset
        self.textGeo[textId].x = self.x + self.textXOffset + self.sidePadding
      end
    end
  end

  -- Compute the position and size of the first column text (if any)
  if self.firstColumnText ~= nil then
    self.firstColumnTextGeo = {}

    -- W and H come from text texture size
    _,_,self.firstColumnTextGeo.w,self.firstColumnTextGeo.h = self.firstColumnTextTexture:query()

    -- X is left aligned with a double padding
    self.firstColumnTextGeo.x = self.x + self.columnTextSidePadding*2

    -- Y is vertically centered
    self.firstColumnTextGeo.y = math.round(self.y + (self.h - self.firstColumnTextGeo.h)/2)
  end

  -- Compute the position and size of the last column text (if any)
  if self.lastColumnText ~= nil then
    self.lastColumnTextGeo = {}

    -- W and H come from text texture size
    _,_,self.lastColumnTextGeo.w,self.lastColumnTextGeo.h = self.lastColumnTextTexture:query()

    -- X is right aligned with a double padding
    self.lastColumnTextGeo.x = self.x + self.w - self.lastColumnTextGeo.w - self.columnTextSidePadding*2

    -- Y is vertically centered
    self.lastColumnTextGeo.y = math.round(self.y + (self.h - self.lastColumnTextGeo.h)/2)
  end

  -- limit text w in case of text overflow
  for textId=1,self.textCount do
    if self.textGeo and self.textGeo[textId] then
      local nextObjectX = self.x + self.w
      if type(self.lastColumnTextGeo) == 'table' and type(self.lastColumnTextGeo.x) == 'number' then
        nextObjectX = self.lastColumnTextGeo.x
      end
      local maxTextW = nextObjectX - self.textGeo[textId].x - self.sidePadding
      self.textGeo[textId].w = math.min(self.textGeo[textId].w, maxTextW)
    end
  end

  return true
end

--- class instance

-- Reformat input params for bannerPlus -- TODO move to a private method
paramTable.textArray = table_arrayEntryKey_to_valueArray(paramTable.personArray, 'name')
paramTable.prefixArray = table_arrayEntryKey_to_valueArray(paramTable.personArray, 'club')
paramTable.flagKeyArray = table_arrayEntryKey_to_valueArray(paramTable.personArray, 'country')

local self = ui_bannerPlus_create(paramTable) -- Construct parent class
if self == nil then -- Catch parent construction failure
  log(2,'Unable to instanciate tableRow (Parent class constructor failed)')
  return nil
end

--- public members

self.firstColumnText = paramTable.firstColumnText
self.lastColumnText = paramTable.lastColumnText
self.prefixXOffset = paramTable.prefixXOffset
self.textXOffset = paramTable.textXOffset
self.columnFontName = paramTable.columnFontName or 'bold'
self.columnTextColor = paramTable.columnTextColor or self.textColor -- textColor was set by ui_bannerPlus_create()
self.lastColumnFontSizeRatio = paramTable.lastColumnFontSizeRatio or 0.85

if createSdlObjects(self) ~= true then
  log(2,'Unable to instanciate tableRow (Failed to create Sdl objects)')
  return nil
end

if computeGeometry(self) ~= true then
  log(2,'Unable to instanciate tableRow (Failed to compute geometry)')
  return nil
end

--- public methods
self.parentBannerPlusDraw = self.draw -- Keep a reference to parent draw
function self:draw()
  self:parentBannerPlusDraw()

  -- Copy firstColumn and lastColumn text textures
  if self.firstColumnTextTexture ~= nil then -- TODO, maybe rename this to ...TextTexture ?
    ui_renderer_renderer:copy(self.firstColumnTextTexture, nil, self.firstColumnTextGeo)
  end
  if self.lastColumnTextTexture ~= nil then
    ui_renderer_renderer:copy(self.lastColumnTextTexture, nil, self.lastColumnTextGeo)
  end
end

return self
end
