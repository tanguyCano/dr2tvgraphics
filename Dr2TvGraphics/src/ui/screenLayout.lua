--- Object in charge of computing screen proportions
-- for graphic elements
-- Lua Class

--- Requires

require 'utils.misc'

--- Constants

local titleSafeAreaRatio = 0.90 -- TODO, move to config
local defaultScreenW = 1920
local defaultScreenH = 1080

local fullScreenLayout
function ui_screenLayout_setScreenSize(w, h)
  return fullScreenLayout:setScreenSize(w,h)
end

function ui_screenLayout_createAllLayouts()
  fullScreenLayout = ui_screenLayout_create({})
end

function ui_screenLayout_getLayout()
  return fullScreenLayout
end

--- Class constructor

--- @param #table paramTable {}
function ui_screenLayout_create(paramTable)

--- private members

--- private methods

--- Private functions

--- class instance

local self = {}

--- public members

self.screenW = paramTable.screenW or defaultScreenW
self.screenH = paramTable.screenH or defaultScreenH

--- public methods

function self:setScreenSize(w, h)
  -- Check parameters
  if type(w) ~= 'number' or w <= 0 or math.floor(w) ~= w then
    log(2,"Invalid screen layout screen width parameter '" .. (w or 'nil') .. "'")
    return false
  end
  if type(h) ~= 'number' or h <= 0 or math.floor(h) ~= h then
    log(2,"Invalid screen layout screen height parameter '" .. (h or 'nil') .. "'")
    return false
  end

  log(4, 'Setting screen layout screen size to ' .. (w or 'nil') .. 'x' .. (h or 'nil'))
  self.screenW = w
  self.screenH = h

  return true
end

--- @param #table paramTable {wDivisor, hDivisor, xDivCount, yDivCount, wDivCount, hDivCount, extendToLeftEdge, extendToRightEdge}
---@return Return {x,y,w,h}
-- TODO add support for split screen
function self:getScreenFraction(paramTable)
  if type(paramTable) ~= 'table' then
    log(2,'No paramTable passed to :getScreenFraction (type)', type(paramTable))
    return nil
  end
  -- Check numeric params
  local numberParamList = { "wDivisor", "hDivisor", "xDivCount", "yDivCount", "wDivCount", "hDivCount"}
  for i,numberParamName in pairs(numberParamList) do
    if type(paramTable[numberParamName]) ~= 'number' then
      log(2,"Invalid ".. numberParamName .." parameter '" .. (paramTable[numberParamName] or 'nil') .. "'")
      return nil
    end
  end
  if paramTable.wDivisor == 0 or paramTable.hDivisor == 0 then
      log(2,"Invalid divisors (w: " .. (paramTable.wDivisor or 'nil') .. ", h: " .. (paramTable.hDivisor or 'nil') .. ")")
      return nil
  end

  -- Compute the title-safe rectangle
  local titleW = self.screenW * titleSafeAreaRatio
  local titleH = self.screenH * titleSafeAreaRatio
  local titleX = (self.screenW - titleW) / 2
  local titleY = (self.screenH - titleH) / 2

  -- Compute the dimension of the title-safe fractions
  local dimension = {}
  dimension.w = math.round(titleW / paramTable.wDivisor * paramTable.wDivCount)
  dimension.x = math.round(titleX + titleW / paramTable.wDivisor * paramTable.xDivCount)
  dimension.h = math.round(titleH / paramTable.hDivisor * paramTable.hDivCount)
  dimension.y = math.round(titleY + titleH / paramTable.hDivisor * paramTable.yDivCount)

  -- Handle title-safe bypassing flags
  if paramTable.extendToLeftEdge == true and dimension.x <= titleX then
    local delta = math.round(titleX)
    dimension.w = dimension.w + delta
    dimension.x = dimension.x - delta
  end
  if paramTable.extendToRightEdge == true and (dimension.x + dimension.w) >= (titleX + titleW) then
    local delta = math.round(self.screenW - (titleX + titleW))
    dimension.w = dimension.w + delta
  end

  return dimension
end

return self
end
