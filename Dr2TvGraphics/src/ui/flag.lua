--- UI flag graphic object
-- Draw a country flag of a given size
-- Lua Class

--- Requires

--- Constants

local imageFlagResourceFolder = 'resources/flags'

local flagTable =
{
  ['GBR'] = { type='image', filename='GBR.svg'},
  ['ESP'] = { type='image', filename='ESP.svg'},
  ['FRA'] = { type='bars', horizontalColors = {[1]=0x0055A4, [2]=0xFFFFFF, [3]=0xEF4135}, w_to_h=2/3},
  ['ITA'] = { type='bars', horizontalColors = {[1]=0x008C45, [2]=0xF4F5F0, [3]=0xCD212A}, w_to_h=2/3},
  ['MON'] = { type='bars', verticalColors =   {[1]=0xC8102E, [2]=0xFFFFFF}, w_to_h=4/5},
  ['SCO'] = { type='image', filename='SCO.svg'},
  ['SUI'] = { type='hardcoded', w_to_h=1},
  ['UKR'] = { type='bars', verticalColors =   {[1]=0x0057b7, [2]=0xffd700}, w_to_h=2/3},
  ['UKN'] = { type='bars', horizontalColors = {[1]=0x000000}},
}

--- Class constructor

--- @param #table paramTable {flagKey, x, y, w, h} -- TODO add alignement and stretching (for flag not following a 2:3 aspect ratio)
function ui_flag_create(paramTable)

--- private members

local personsBanner
local titleBanner

--- private methods

local function createSdlObjects(self)
  local err
  self.whiteSurface, err = SDLImage.load("resources/White.bmp")
  if err then
    log(2,'Unable to open image ('.. (err or 'nil') ..')')
    return false
  end

  if flagTable[self.flagKey].type == 'image' then
    local flagFile = (imageFlagResourceFolder or './') .. '/' .. (flagTable[self.flagKey].filename or '')
    self.imageSurface, err = SDLImage.load(flagFile)
    if err then
      log(2,'Unable to open image \'' .. (flagFile or 'nil') .. '\' ('.. (err or 'nil') ..')')
      return false
    end
  end

  return true
end

local function draw_bar_flag(self)

  -- Compute drawing area based on width to height ratio (either reduce h or w)
  local currentW_to_h = self.h / self.w
  local targetW_to_h = flagTable[self.flagKey].w_to_h or 2/3
  local drawingX = self.x
  local drawingY = self.y
  local drawingW = self.w
  local drawingH = self.h
  if (targetW_to_h < currentW_to_h) then
    -- Reduce drawing height, center vertically
    drawingH = math.round(targetW_to_h * drawingW)
    drawingY = self.y + math.round((self.h - drawingH)/2)
  else
    -- Reduce drawing width, , center horizontally
    drawingW = math.round(drawingH / targetW_to_h)
    drawingX = self.x + math.round((self.w - drawingW)/2)
  end

  -- Prepare variables based on color direction
  local hDiv, vDiv = 1, 1
  local colorTable
  if type(flagTable[self.flagKey].horizontalColors) == 'table' then
    colorTable = flagTable[self.flagKey].horizontalColors
    hDiv = #colorTable
  elseif type(flagTable[self.flagKey].verticalColors) == 'table' then
    colorTable = flagTable[self.flagKey].verticalColors
    vDiv = #colorTable
  else
    log(2, "Unable to parse flag colors for flag '" .. (self.flagKey or 'nil') .. "'")
  end

  -- Compute each rectangle size and position
  local rectangleCount = #colorTable
  local rectangleDim = {}
  for r=1,rectangleCount do
    rectangleDim[r] = {}

    if hDiv > 1 then
      local startXOffset = math.round((drawingW - 1) / hDiv * (r-1))
      local endXOffset = math.round((drawingW - 1) / hDiv * (r))
      rectangleDim[r].x = drawingX + startXOffset
      rectangleDim[r].w = endXOffset - startXOffset
    else
      rectangleDim[r].x = drawingX
      rectangleDim[r].w = drawingW
    end
    if vDiv > 1 then
      local startYOffset = (drawingH - 1) / vDiv * (r-1)
      local endYOffset = (drawingH - 1) / vDiv * (r)
      rectangleDim[r].y = drawingY + math.round(startYOffset)
      rectangleDim[r].h = math.round(endYOffset - startYOffset)
    else
      rectangleDim[r].y = drawingY
      rectangleDim[r].h = drawingH
    end
  end

  -- Draw the rectangles
  for r=1,rectangleCount do
    self.whiteSurface:fillRect(nil,colorTable[r])
    local rTexture = ui_renderer_renderer:createTextureFromSurface(self.whiteSurface)
    local rSrcRect = {x=0,y=0,w=10,h=10}
    local rDestRect = rectangleDim[r]
    ui_renderer_renderer:copy(rTexture, rSrcRect, rDestRect)
  end

end

local function draw_image_flag(self)

  -- Retrieve image flag texture dimension
  if self.imageSurface == nil then
    log(2, 'No flag image surface available')
    return
  end

  local imageTexture = ui_renderer_renderer:createTextureFromSurface(self.imageSurface)
  local _,_, imageW, imageH = imageTexture:query()
  if imageW == nil or imageH == nil or imageW == 0 then
    log(2, 'Unable to retrieve valid flag image surface dimension (w,h)', imageW, imageH)
    return
  end

  -- Compute drawing area based on width to height ratio (either reduce h or w)
  local currentW_to_h = self.h / self.w
  local targetW_to_h = imageH / imageW
  local drawingX = self.x
  local drawingY = self.y
  local drawingW = self.w
  local drawingH = self.h
  if (targetW_to_h < currentW_to_h) then
    -- Reduce drawing height, center vertically
    drawingH = math.round(targetW_to_h * drawingW)
    drawingY = self.y + math.round((self.h - drawingH)/2)
  else
    -- Reduce drawing width, , center horizontally
    drawingW = math.round(drawingH / targetW_to_h)
    drawingX = self.x + math.round((self.w - drawingW)/2)
  end

  -- Draw the image
  local rSrcRect = {x=0,y=0,w=imageW,h=imageH}
  local rDestRect = {x=drawingX, y=drawingY, w=drawingW, h=drawingH}
  ui_renderer_renderer:copy(imageTexture, rSrcRect, rDestRect)
end

local function draw_swiss_flag(self)

  -- Compute drawing area based on width to height ratio (either reduce h or w)
  local currentW_to_h = self.h / self.w
  local targetW_to_h = flagTable[self.flagKey].w_to_h or 2/3
  local drawingX = self.x
  local drawingY = self.y
  local drawingW = self.w
  local drawingH = self.h
  if (targetW_to_h < currentW_to_h) then
    -- Reduce drawing height, center vertically
    drawingH = math.round(targetW_to_h * drawingW)
    drawingY = self.y + math.round((self.h - drawingH)/2)
  else
    -- Reduce drawing width, , center horizontally
    drawingW = math.round(drawingH / targetW_to_h)
    drawingX = self.x + math.round((self.w - drawingW)/2)
  end

  -- Prepare variables based on color direction
  -- Build the rectangles size, position and colors
  local rectangleDim = {}
  local colorTable = {}
  -- red background
  colorTable[1] = 0xFF0000
  rectangleDim[1] = {x=drawingX, y=drawingY, w=drawingW, h=drawingH}

  -- white cross
  colorTable[2] = 0xFFFFFF
  rectangleDim[2] = {x=math.round(drawingX+drawingW*6/32), y=math.round(drawingY+drawingH*13/32), w=math.round(drawingW*20/32), h=math.round(drawingH*6/32)}

  colorTable[3] = 0xFFFFFF
  rectangleDim[3] = {x=math.round(drawingX+drawingW*13/32), y=math.round(drawingY+drawingH*6/32), w=math.round(drawingW*6/32), h=math.round(drawingH*20/32)}

  -- Draw the rectangles
  local rectangleCount = #colorTable
  for r=1,rectangleCount do
    self.whiteSurface:fillRect(nil,colorTable[r])
    local rTexture = ui_renderer_renderer:createTextureFromSurface(self.whiteSurface)
    local rSrcRect = {x=0,y=0,w=10,h=10}
    local rDestRect = rectangleDim[r]
    ui_renderer_renderer:copy(rTexture, rSrcRect, rDestRect)
  end

end


--- class instance

local self = {}

--- public members

self.flagKey = (paramTable.flagKey or 'UKN'):upper()
self.x = paramTable.x or 0
self.y = paramTable.y or 0
self.w = paramTable.w or 30
self.h = paramTable.h or 20

if (flagTable[self.flagKey] == nil) then
  log(2,"Unable to instanciate flag (Invalid flagKey '" .. (paramTable.flagKey or 'nil') .. "')")
  return nil
end

if createSdlObjects(self) ~= true then
  log(2,'Unable to instanciate flag (Failed to create Sdl objects)')
  return nil
end

--- public methods

--- @return #table geo {x, y, w, h}
function self:getGeometry()
  local geometry = {}
  for _,fieldName in pairs({'x','y','w','h'}) do  -- Copy internal geometry fields to a new table
    geometry[fieldName] = self[fieldName]
  end
  return geometry
end

--- @param #table paramTable {x, y, w, h}
function self:setGeometry(paramTable)
  if type(paramTable) ~= 'table' then
    return false
  end

  for _,fieldName in pairs({'x','y','w','h'}) do  -- Copy object geometry, when the field exists (allows partial set)
    if type(paramTable[fieldName]) == 'number' then
      self[fieldName] = paramTable[fieldName]
    end
  end

  return true
end

function self:draw()
  -- Temporary vectorial flag drawing (very limited)

  local flagDetails = flagTable[self.flagKey]
  if flagDetails == nil then
    return
  end

  if flagDetails.type == 'bars' then
    draw_bar_flag(self)
  elseif flagDetails.type == 'image' then
    draw_image_flag(self)
  elseif self.flagKey == 'SUI' then
    draw_swiss_flag(self)
  end

end

return self
end
