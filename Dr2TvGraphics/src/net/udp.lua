--- Hides udp polling

--- Requires

require 'utils.misc'

--- Constants

local udpReceiveBufferSize = 256 -- Plenty enough for the UPDATE message
local duplicateTickDelta -- disabled when nil

--- Private variables

local SDL
local SDLNet
local udpPort
local udpSocket

-- associative array of [msg] = tick
local previousUdpMessagesArrivalTick = {}

--- Private functions

local function net_udp_isDuplicate(udpMessage, arrivalTick)
  -- check parameters
  if type(udpMessage) ~= 'string' or type(arrivalTick) ~= 'number' then
    log(3, "Invalid parameters to net_udp_isDuplicate", udpMessage, arrivalTick)
    return true
  end

  -- Just store the message if duplicate detection deactivated
  if duplicateTickDelta == nil then
    previousUdpMessagesArrivalTick[udpMessage] = arrivalTick
    return false
  end

  -- Check if this message has been received before
  if previousUdpMessagesArrivalTick[udpMessage] ~= nil then
    -- Check tick difference since previous
    local tickDeltaSinceOriginal = arrivalTick - previousUdpMessagesArrivalTick[udpMessage]
    if tickDeltaSinceOriginal < duplicateTickDelta then
      return true -- Within tick deadzone
    else
      -- Out of deadzone, update arrival tick and return false
      previousUdpMessagesArrivalTick[udpMessage] = arrivalTick
      return false
    end
  else
     -- Never seen before, save and return false
    previousUdpMessagesArrivalTick[udpMessage] = arrivalTick
    return false
  end
end

--- Public functions

function net_udp_init(sdlInstance, sdlNetInstance, port)
  -- copy params
  SDL = sdlInstance
  SDLNet = sdlNetInstance
  udpPort = port

  -- check init
  if SDL == nil or SDLNet == nil
  then
    FATAL("Invalid SDL instance passed")
  end

  SDLNet.init()
end

function net_udp_setDuplicateTickDelta(tickDelta)
  if tickDelta == nil then
    duplicateTickDelta = nil -- deactivate duplicate filtering
  end
  if type(tickDelta) == 'number' and tickDelta > 0 then
    duplicateTickDelta = tickDelta
  end
end

function net_udp_openSocket()
  -- Check all variables are initialised
  if SDL == nil or SDLNet == nil then
    log(2,'Unitialised udp module')
    return false
  end
  if udpPort == nil then
    log(2,'No udp port provided')
    return false
  end

  -- Open udp socket
  local err
  udpSocket, err = SDLNet.openUdp(udpPort)
  -- Handle failure to open socket
  if udpSocket == nil then
    log(1,'Unable to open udp socket on port', udpPort)
    return false
  end

  -- Clear duplicate checker history
  previousUdpMessagesArrivalTick = {}

  return true
end

function net_udp_closeSocket()
  if tcpSocket == nil then return end
  udpSocket:close()
  udpSocket = nil
end

function net_udp_receive()
  local udpMessage = udpSocket:recv(udpReceiveBufferSize)
  if udpMessage then
    local arrivalTick = SDL.getTicks()
    -- Only return the received message if not a duplicate
    if net_udp_isDuplicate(udpMessage, arrivalTick) == false then
      log(4,"Udp new message received @", arrivalTick, udpMessage)
      return udpMessage, arrivalTick
    else
      log(5,"Udp duplicate received @", arrivalTick)
      return nil
    end
  else
    return nil
  end
end
