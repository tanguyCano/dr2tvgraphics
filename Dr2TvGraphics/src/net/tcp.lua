--- Hides udp polling

--- Requires

require 'utils.misc'

--- Constants

--- Private variables

local SDL
local SDLNet
local tcpSocket
local set
local clients
local SetSize = 8 -- TODO rename setSze to maxConnection? put in setting or in function arg?

--- Public functions

function net_tcp_init(sdlInstance, sdlNetInstance)
  -- copy params
  SDL = sdlInstance
  SDLNet = sdlNetInstance

  -- check init
  if SDL == nil or SDLNet == nil
  then
    FATAL("Invalid SDL instance passed")
  end

  SDLNet.init()
end

function tcp_startServer(serverPort)
  if not SDL or not SDLNet then FATAL("tcp not initialized") end
  if tcpSocket ~= nil then FATAL("TCP server already started") end

  set = SDLNet.set(SetSize) -- TODO, move these two lines to startServer?
  clients = {}

  --Start network
  local addr = SDLNet.resolveHost(nil, serverPort)
  tcpSocket = SDLNet.openTcp(addr)
  set:add(tcpSocket)
  clients = {}

  log(3,"TCP server started")
end

function tcp_stopServer()
  --Clear the arrays
  for i,client in ipairs(clients) do
    set:del(client)
  end
  set:del(tcpSocket)
  set = nil

  --Close the socket
  tcpSocket:close()
  tcpSocket = nil

  -- Clean settings
end

-- return an array of {client, bytes}
function tcp_serverPoll(pollingTimeout)
  local clientRequestArray = {}
  local socketReadyCount = set:checkSockets(pollingTimeout)
  if socketReadyCount > 0 then
    --Check if it is the socket
    if tcpSocket:ready() then
      local newClient = tcpSocket:accept()
      if newClient then
        log(3,"New client on TCP socket") -- TODO, show ip and stuff
        -- Add client to arrays
        table.insert(clients, newClient)
        set:add(newClient)
      end

    --Or if it a client
    else
      -- Loop over clients to check for ready ones
      for clientIndex, client in ipairs(clients) do
        if client:ready() then
          local byteReceived, byteReceivedCount = client:recv(4096)
          if not byteReceived then
            log(3,"TCP client disconnected")
            -- Remove client from arrays
            set:del(client)
            local _,i = table_find(clients, client)
            table.remove(clients, i)
          else
            -- Process received bytes
            log(4,"TCP socket received ".. (byteReceivedCount or 'nil') .." bytes")
            log(5,'Bytes received', stringbytes_to_hexdump(byteReceived))
            table.insert(clientRequestArray, {client=client, bytes=byteReceived})
          end -- check if bytes received
        end
      end -- client loop
    end -- socket or client ready if
  end -- socketReadCount > 0
  return clientRequestArray
end

function tcp_serverRespond(clientResponseArray)
  if type(clientResponseArray) ~= 'table' then
    return
  end

  -- TODO add a lot of sanity checks (net is open, we're in server mode, the client are in our clients array)

  for _,clientResponse in ipairs(clientResponseArray) do
    if clientResponse.client ~= nil and type(clientResponse.bytes) == 'string' then
      log(5,'Bytes send', stringbytes_to_hexdump(clientResponse.bytes))
      clientResponse.client:send(clientResponse.bytes)
      -- TODO, check the success
    end
  end
end

function net_tcp_openSocket(ip, port)
  -- Check all variables are initialised
  if SDL == nil or SDLNet == nil then
    log(2,'Unitialised tcp module')
    return false
  end
  if port == nil then
    log(2,'No tcp port provided')
    return false
  end

  -- Create TCP socket and connect
  local addr = SDLNet.resolveHost(ip, port)
  local err
  tcpSocket, err = SDLNet.openTcp(addr)
  -- Handle failure to open socket
  if tcpSocket == nil then
    log(1,'Unable to open tcp socket to ip and port', ip, port)
    return false
  end

  return true
end

function net_tcp_closeSocket()
  if tcpSocket == nil then return end
  tcpSocket:close()
  tcpSocket = nil
end

function net_tcp_send(str)
  -- Check all variables are initialised
  if SDL == nil or SDLNet == nil then
    log(2,'Unitialised tcp module')
    return false
  end

  -- Check the socket is open
  if tcpSocket == nil then
    log(2,'Tcp socket not opened')
    return false
  end

  -- Send the data
  local countToSend = string.len(str)
  log(4,'Send '..(countToSend or 'nil')..' bytes of tcp data')
  log(5,'Send tcp data', '\n'..stringbytes_to_hexdump(str))
  local countSent, err = tcpSocket:send(str)

  -- Check send outcome
  if countSent == nil then
    log(2,'Tcp socket send failed ('.. (err or 'nil')..')')
    return false
  end
  if countSent ~= countToSend then
    log(2,'Tcp socket sent '.. (countSent or 'nil') .. ' instead of '.. (countToSend or 'nil') .. ' bytes')
    return false
  end
  return true
end

function net_tcp_receive(countToReceive)
  -- Check all variables are initialised
  if SDL == nil or SDLNet == nil then
    log(2,'Unitialised tcp module')
    return false
  end

  -- Check the socket is open
  if tcpSocket == nil then
    log(2,'Tcp socket not opened')
    return false
  end

  -- Receive the data
  local dataReceived, countReceived = '', 0
  local remainingCountToReceived = countToReceive
  while countReceived < countToReceive do
    log(5,'Wait for '.. (remainingCountToReceived or 'nil') .. ' bytes on tcp')
    local chunckReceived, chunkCountReceived, err = tcpSocket:recv(remainingCountToReceived)

    -- Check receive outcome
    if chunckReceived == nil or chunkCountReceived == nil then
      log(2,'Tcp socket receive failed ('.. (err or 'nil')..')')
      return false
    end

    -- Check quantity received
    log(5,'Received '..(chunkCountReceived or 'nil')..' bytes of tcp data')
    if chunkCountReceived == 0 then -- last receive failed
      log(2,'Tcp socket received '.. (countReceived or 'nil') .. ' instead of '.. (countToReceive or 'nil') .. ' bytes')
      return false
    elseif chunkCountReceived > remainingCountToReceived then -- Received too much
      local totalReceived = countReceived + chunkCountReceived
      log(2,'Tcp socket received '.. (totalReceived or 'nil') .. ' instead of '.. (countToReceive or 'nil') .. ' bytes')
      return false
    else -- Received enough or less
      countReceived = countReceived + chunkCountReceived
      dataReceived = dataReceived .. chunckReceived
      remainingCountToReceived = remainingCountToReceived - chunkCountReceived
    end
  end -- Receiving loop

  log(4,'Received '..(countReceived or 'nil')..' bytes of tcp data')
  log(5,'Received tcp data', '\n'..stringbytes_to_hexdump(dataReceived))
  return true, dataReceived
end
