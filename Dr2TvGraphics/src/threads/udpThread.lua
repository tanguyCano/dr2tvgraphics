--- Manage thread starting and stopping
-- keeps a reference on thread objects

--- Requires

require 'utils.misc'
require 'threads.utils'
require 'net.udp'
require 'dr.updateMsg'
require 'dr.updateFile'
local config = require 'config'
local SDL = require 'SDL'
local SDLNet = require 'SDL.net'

--- Constants

currentThreadId = 'udp'
threadName = threads_utils_getThreadName(currentThreadId)
oldPrint = print; print = function(...) oldPrint(threadName,...) end

--- Private variables

local incomingChannel

--- Private functions

local function initThread()
  incomingChannel = SDL.getChannel(threads_utils_getThreadChannelName(currentThreadId))
  if not incomingChannel then FATAL("Unable to open incoming channel") end

  -- Init and start udp module
  net_udp_init(SDL,SDLNet,config.Dr.UdpPort)
  net_udp_setDuplicateTickDelta(config.Dr.UdpDuplicateTickDelta)
  if net_udp_openSocket() ~= true then FATAL("Unable to start udp socket") end
end

local function sendUpdateToTcp(updateObject)
  -- Check parameters
  if type(updateObject) ~= 'table' then
    log(2, "Invalid first parameter to sendUpdateToTcp (not a table)")
    return false
  end

  -- Parse the update file path to retrieve filename
  local updateFileName
  -- TODO, Actually implement this (it's always the same, I'm taking a shortcut...)
  updateFileName = 'Update.txt'

  -- Build the thread message data for TCP
  local fileToLoadDetails = dr_updateFile_buildFileDetails(updateObject.senderIp, updateFileName, updateObject.arrivalTick)
  if type(fileToLoadDetails) ~= 'table' then
    log(2, "Unable to build fileToLoadDetails table")
  end
  local threadMessageDataString = threads_message_serializeTableToDataString(fileToLoadDetails)
  if type(threadMessageDataString) ~= 'string' then
    log(2, "Unable to serialize fileToLoadDetails table")
  end

  -- Build and send the message
  local messageObject = threads_message_build('LOAD_TCP_FILE',threadMessageDataString)
  if messageObject == nil then
    log(3, "sendUpdateToTcp: Unable to build message")
    return false
  end

  if threads_utils_sendMessage('tcp', messageObject) ~= true then
    log(3, "sendUpdateToTcp: Unable to send message")
    return false
  end
  return true
end

local function udpThreadLoop()
  while true do
    --wait and parse next channel message if any
    local channelMsgString = incomingChannel:first()
    if channelMsgString ~= nil then
      incomingChannel:pop()
      threads_message_logMessage(4, "Incoming message", channelMsgString)
      local msgSrc, msgType, msgData = threads_message_parse(channelMsgString)

      --handle message
      if msgType == "TERMINATE" then
        return -- terminate thread
      end
    end

    local udpMessage, arrivalTick = net_udp_receive()
    if udpMessage ~= nil then
      if dr_updateMsg_isUpdate(udpMessage) then
        log(3,"udp update message received")
        local updateObject = dr_updateMsg_parseUpdate(udpMessage)
        if updateObject == nil then
          log(2,"Unable to parse update message", udpMessage)
        else
          -- TODO, filter on host, event, and eventMode
          -- Forward to tcp task
          updateObject.arrivalTick = arrivalTick -- This will be used to save a replay file
          if sendUpdateToTcp(updateObject) ~= true then
            log(2,"Unable to send update to Tcp", udpMessage)
          end
        end
      end
    end

    SDL.delay(config.UdpPollingIntervalMs)
  end
end

local function deinitThread()
  net_udp_closeSocket()
end

--- Thread execution

log(4,'Thread startup')
initThread()
udpThreadLoop()
deinitThread()
log(4,'Thread shutdown')
return 0
