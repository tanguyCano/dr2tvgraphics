--- Manage thread starting and stopping
-- keeps a reference on thread objects

--- Requires

require 'utils.misc'
require 'threads.utils'
require 'net.tcp'
require 'command.command'
require 'command.timetable'
require 'threads.drToUi'
require 'threads.cmdToUi'
local config = require 'config'
local SDL = require 'SDL'
local SDLNet = require 'SDL.net'

--- Constants

currentThreadId = 'cmd'
threadName = threads_utils_getThreadName(currentThreadId)
oldPrint = print; print = function(...) oldPrint(threadName,...) end

--- Private variables

local incomingChannel

--- Private functions

local function initThread()
  incomingChannel = SDL.getChannel(threads_utils_getThreadChannelName(currentThreadId))
  if not incomingChannel then FATAL("Unable to open incoming channel") end

  -- Init and start tcp module
  net_tcp_init(SDL,SDLNet)
  tcp_startServer(config.cmd.tcpPort)
  command_timetable_loadFromFileSystem()
end

local function sendCommandObjectToScheduler(commandObject)
  -- Check parameters
  if type(commandObject) ~= 'table' then
    log(2, "Invalid first parameter to sendCommandObjectToScheduler (not a table)")
    return false
  end

  local threadMessageDataString = threads_message_serializeTableToDataString(commandObject)
  if type(threadMessageDataString) ~= 'string' then
    log(2, "Unable to serialize commandObject table")
  end

  -- Build and send the message
  local messageObject = threads_message_build('NEW_COMMAND_OBJECT',threadMessageDataString)
  if messageObject == nil then
    log(3, "sendCommandObjectToScheduler: Unable to build message")
    return false
  end

  if threads_utils_sendMessage('sch', messageObject) ~= true then
    log(3, "sendCommandObjectToScheduler: Unable to send message")
    return false
  end

  return true
end

local function sendMessageToUi(msgType, msgData)
  -- Params wll be checked by following funcion

  -- Build the thread message data for UI
  local threadMessageDataString = threads_message_serializeTableToDataString(msgData)
  if type(threadMessageDataString) ~= 'string' then
    log(2, "sendMessageToUi: Unable to serialize msgData table")
    return false
  end

  -- Build and send the message
  local messageObject = threads_message_build(msgType,threadMessageDataString)
  if messageObject == nil then
    log(3, "sendMessageToUi: Unable to build message")
    return false
  end

  if threads_utils_sendMessage('ui', messageObject) ~= true then
    log(3, "sendMessageToUi: Unable to send message")
    return false
  end

  return true
end

local function cmdThreadLoop()
  while true do
    --wait and parse next channel message if any
    local channelMsgString = incomingChannel:first()
    if channelMsgString ~= nil then
      incomingChannel:pop()
      threads_message_logMessage(4, "Incoming message", channelMsgString)
      local msgSrc, msgType, msgData = threads_message_parse(channelMsgString)

      --handle message
      if msgType == "TERMINATE" then
        return -- terminate thread
      end
    end

    local clientCommandArray = tcp_serverPoll(config.cmd.tcpPollingTimeoutMs)
    if type(clientCommandArray) == 'table' then
      local clientResponseArray = {}
      for _,clientCommand in ipairs(clientCommandArray or {}) do
        local handled, commandObject, commandOutcome, errorDetails = command_socketBytesToObject(clientCommand.bytes)
        local responseBytes = (commandOutcome or 'Fail')
        if type(errorDetails) == 'string' then
          responseBytes = responseBytes .. ' (' .. errorDetails .. ')'
        end

        if handled == true then
          log(3, 'Executing command (type)', commandObject.typeStr)
          if commandObject.typeStr == commandType.PING then
            -- Nothing to do

          elseif commandObject.typeStr == commandType.CLEAR or
                 commandObject.typeStr == commandType.SET_MEDAL_RANKING or
                 commandObject.typeStr == commandType.SHOW_NEXT_EVENT or
                 commandObject.typeStr == commandType.SHOW_TIME then
            sendCommandObjectToScheduler(commandObject)

          elseif commandObject.typeStr == commandType.SHOW_SESSION_TIMETABLE then
            -- Bypass statemachine and directly send to ui

            local cmdTypeStrToCmdToUiBuildMessage = {
              [commandType.SHOW_SESSION_TIMETABLE] = threads_cmdToUi_showSessionTimetable_buildMessageData,
            }

            sendMessageToUi(threads_drToUi_clearAll_buildMessageData{eventAorB='a'}) -- Remove any other ui element
            if cmdTypeStrToCmdToUiBuildMessage[commandObject.typeStr] ~= nil then
              sendMessageToUi(cmdTypeStrToCmdToUiBuildMessage[commandObject.typeStr](commandObject))
            end
            if config.Ui.useFramerate ~= true then
              sendMessageToUi(threads_drToUi_render_buildMessageData({eventAorB='a'})) -- Force a frametick
            end

          end -- Command type if
        end -- If parsed

        table.insert(clientResponseArray, {client=clientCommand.client, bytes=(responseBytes or '')..'\n'})
      end -- For each client command
      tcp_serverRespond(clientResponseArray)
    end

    SDL.delay(config.cmd.pollingIntervalMs)
  end
end

local function deinitThread()
  tcp_stopServer()
  --Net.quit() -- Is this important?
end

--- Thread execution

log(4,'Thread startup')
initThread()
cmdThreadLoop()
deinitThread()
log(4,'Thread shutdown')
return 0
