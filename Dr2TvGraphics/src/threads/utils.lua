--- Manage thread starting and stopping
-- keeps a reference on thread objects

--- Requires
require 'threads.list'
require 'threads.message'
SDL = require 'SDL'

--- Constants

--- Private variables

--- Private functions

--- Public functions

function threads_utils_startAllThreads()

  -- Create and store the thread object for each entry in the config table
  log(3, 'Starting threads')
  local threadCount = 0
  for threadId, threadEntry in pairs(threads_list) do
    -- Check there's a thread source file
    if type(threadEntry.luaFile) ~= 'string' then
      log(1,'Invalid or no lua file for thread', threadId)
      return false
    end

    -- Build the required thread strings
    local srcFile = 'src/threads/' .. threadEntry.luaFile
    log(4, 'Thread source file is ' .. srcFile)
    local threadName = 'th' .. threadId

    -- Start the thread
    local threadHandler, err = SDL.createThread(threadName, srcFile)
    if not threadHandler then
      FATAL('Unable to create '..threadId..' thread (err: '.. (err or 'nil')..')')
    else
      threadEntry.threadHandler = threadHandler -- Save a reference to the thread
    end
    threadCount = threadCount + 1
  end
  log(4, 'All threads ('..threadCount..') successfully started')

  return true
end

function threads_utils_stopAllThreads()
  log(3, 'Stopping threads')

  -- Loop over all running threads
  local threadRunning = 0
  local threadStopped = 0
  for threadId, threadEntry in pairs(threads_list) do
    -- Check if this thread is running
    if threadEntry.threadHandler ~= nil then
      threadRunning = threadRunning + 1

      -- Send terminate message and wait for end of each thread
      local terminateMessageString = threads_message_build('TERMINATE')
      if threads_utils_sendMessage(threadId, terminateMessageString) == true then
        log(4,"Waiting for end of thread "..threadId)
        threadEntry.threadHandler:wait() -- FIXME infinite loop if the thread refuses to terminate
        threadEntry.threadHandler = nil -- Clear thread handler reference
        log(4,threadId .. " terminated")
        threadStopped = threadStopped + 1
      else
        log(2,"Unable to send terminate to thread "..threadId)
      end
    end
  end

  if threadStopped == threadRunning then
    log(4, 'All threads ('..threadStopped..') successfully stopped')
  else
    log(2, 'Only '..threadStopped..' threads stopped out of '..threadRunning)
  end
end

function threads_utils_waitForAllThreads()
  log(3, 'Wait for all threads to stop')
  for threadId, threadEntry in pairs(threads_list) do
    -- For all thread that are running, wait for their end
    if threadEntry.threadHandler ~= nil then
      log(4,"Waiting for end of thread "..threadId)
      threadEntry.threadHandler:wait()
      log(4,threadId .. " terminated")
    end
  end
  log(3, 'All threads have stopped')
end

local cachedChannels = {}
function threads_utils_sendMessage(destThreadId, messageString)
  threads_message_logMessage(4,"Send message    ",messageString)
  -- Check the thread ids are valid
  local srcThreadId = currentThreadId
  if threads_list_isValidThreadId(srcThreadId) ~= true then
    log(2, 'Invalid source threadId', srcThreadId)
    return false
  end
  if threads_list_isValidThreadId(destThreadId) ~= true then
    log(2, 'Invalid destination threadId', destThreadId)
    return false
  end

  -- Get the destination thread channel
  local threadChannelName = threads_utils_getThreadChannelName(destThreadId)
  if threadChannelName == nil then
    log(2, 'Unable to get channel name for threadId', destThreadId)
    return false
  end

  cachedChannels[threadChannelName] = cachedChannels[threadChannelName] or SDL.getChannel(threadChannelName)
  local threadChannel = cachedChannels[threadChannelName]

  if threadChannel == nil then
    log(2, 'Unable to get channel ', threadChannelName)
    return false
  end

  -- Send the message
  threadChannel:push(messageString)

  return true
end

function threads_utils_getThreadName(threadId)
  -- Check the thread id is valid
  if threads_list_isValidThreadId(threadId) == true then
    return 'th'..firstLetterToUpper(threadId)
  else
    log(2, 'Invalid threadId', threadId)
    return nil
  end
end

function threads_utils_getThreadChannelName(threadId)
  -- Check the thread id is valid
  if threads_list_isValidThreadId(threadId) == true then
    return 'ch'..firstLetterToUpper(threadId)
  else
    log(2, 'Invalid threadId', threadId)
    return nil
  end
end
