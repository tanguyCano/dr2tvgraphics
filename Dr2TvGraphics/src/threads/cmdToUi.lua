--- Build ui objects from command objects
-- Operate on each side of the scheduler -> ui thread messaging

--- Requires

require 'ui.presenter'
require 'ui.banner'
require 'ui.table'

--- Constants

-- TODO, extract language constants
local monthStr = {
  [1] = 'Janvier',
  [2] = 'Février',
  [3] = 'Mars',
  [4] = 'Avril',
  [5] = 'Mai',
  [6] = 'Juin',
  [7] = 'Juillet',
  [8] = 'Août',
  [9] = 'Septembre',
  [10] = 'Octobre',
  [11] = 'Novembre',
  [12] = 'Décembre',
}
local weekDayStr = {
  [1] = 'Dimanche',
  [2] = 'Lundi',
  [3] = 'Mardi',
  [4] = 'Mercredi',
  [5] = 'Jeudi',
  [6] = 'Vendredi',
  [7] = 'Samedi',
}

--- Private variables

--- Private functions

--- Public functions

function threads_cmdToUi_clearAll_buildMessageData(stateMachine)
  if type(stateMachine) ~= 'table' then
    return nil
  end

  local msgType = 'UI_CLEAR_ALL' -- Shared with drToUi
  local msgData = {}
  msgData.eventAorB = stateMachine.eventAorB

  -- Nothing else to pass
  return msgType, msgData
end

function threads_cmdToUi_showNextEvent_buildMessageData(commandObject)
  if type(commandObject) ~= 'table' then
    return nil
  end
  local msgType = 'UI_SET_EVENT_INFO' -- Shared with drToUi
  local msgData = {}
  msgData.eventAorB = commandObject.eventAorB

  -- Search in timetable based on current time
  local nextEventGroup = commandObject.nextEventGroup

  if nextEventGroup == nil then
    log(3, 'No eventGroup to build next event info from, nothing will be displayed')
    return nil -- Note, we shouldn't have reached cmdToUi, but now that we have, returning nil
    -- won't redraw anything new, but a clear probably happened
  end

  -- Build the event info field (time of the next event group)
  local nextEventTimeStr
  if type(nextEventGroup.startTimestamp) ~= 'number' then
    log(3, 'EventGroup does not have a startTimestamp')
  else
    nextEventTimeStr = os.date("%Hh%M", nextEventGroup.startTimestamp)
  end
  msgData.infoStr = 'Prochain concours : ' .. nextEventTimeStr or ''  -- TODO, extract language constants

  -- Insert all events names for this eventGroup
  msgData.eventNameArray = {}
  msgData.activeEventIndex = nil -- This won't be set as we aren't in-event
  for _,event in ipairs(nextEventGroup.eventArray or {}) do
    -- If the timetable doesn't have the eventAorB field, default to eventA
    if msgData.eventAorB == (event.eventAorB or 'a') then
      table.insert(msgData.eventNameArray, event.eventName)
    end
  end

  -- Abort if no event for this eventGroup (given the stateMachine inherited eventAorB)
  if #msgData.eventNameArray == 0 then
    log(3, 'No events in this eventGroup for this stateMachine (startTimestamp, eventAorB)', os.date('%c', nextEventGroup.startTimestamp or 0), msgData.eventAorB)
    return nil
  end

  return msgType, msgData
end

function threads_cmdToUi_showSessionTimetable_buildMessageData(commandObject)
  if type(commandObject) ~= 'table' then
    return nil
  end
  local msgType = 'UI_SET_TABLES' -- Shared with drToUi
  local msgData = {}

  local meetNameStr = commandObject.meetName -- check this isn't nil ?
  local meetLocationStr = commandObject.meetLocation

  -- The session was provided by the command parser
  local session = commandObject.session

  if session == nil then
    log(3, 'No session to build session timetable from, nothing will be displayed')
    return nil -- Note, we shouldn't have reached cmdToUi, but now that we have, returning nil
    -- won't redraw anything new, but a clear probably happened
  end

  -- Build session human readable start date plus name
  local sessionFullNameStr
  if type(session.startTimestamp) ~= 'number' then
    log(3, 'Session does not have a startTimestamp')
  else
    local sessionStartTimestampFields = os.date("*t", session.startTimestamp)
    if type(sessionStartTimestampFields) == 'table' then
      -- TODO extract full date building
      sessionFullNameStr = '' ..
        ((weekDayStr[sessionStartTimestampFields.wday or -1]) or '') ..
        string.format(" %02d ", sessionStartTimestampFields.day or 0) ..
        ((monthStr[sessionStartTimestampFields.month or -1]) or '') ..
        string.format(" %04d ", sessionStartTimestampFields.year or 0)
      if type(session.name) == 'string' then
        sessionFullNameStr = sessionFullNameStr .. ' - ' .. session.name
      end
    end
  end

  msgData.tableArray = {}

  -- Create a table for all the eventGroup in this session
  -- with the time of each eventGroup mentionned once
  local entryTableData = {}
  entryTableData.title1 = meetNameStr or ' '
  entryTableData.title2 = sessionFullNameStr or ' '
  entryTableData.footerText = meetLocationStr

  -- Create a row per event in each eventGroup
  entryTableData.rowArray = {}
  for _,eventGroup in ipairs(session.eventGroupArray or {}) do

    -- Check how many eventAorB values exist
    local showEventAorB = false
    for i,event in ipairs(eventGroup.eventArray) do
      -- At least one eventB
      if event.eventAorB ~= nil and event.eventAorB ~= 'a' then
        showEventAorB = true
        break
      end
    end

    -- Create the row for this eventGroup
    for i,event in ipairs(eventGroup.eventArray) do

      -- Show time for first event in a group
      local firstColumnText
      if i == 1 then
        -- Build eventGroup human readable start time
        if type(eventGroup.startTimestamp) ~= 'number' then
          log(3, 'Event group does not have a startTimestamp')
        else
          firstColumnText = os.date("%Hh%M", eventGroup.startTimestamp)
        end
      end

      -- Handle eventAorB (display on lastColumnText, except if all 'a')
      local lastColumnText = nil
      if showEventAorB then
        lastColumnText = 'Concours ' .. (event.eventAorB or 'a'):upper() -- TODO, extract language constants
      end

      local row = {
        personArray = {{name=event.eventName}},
        firstColumnText = firstColumnText,
        lastColumnText = lastColumnText,
      }
      table.insert(entryTableData.rowArray, row)
    end
  end

  -- Insert the table in the tableArray
  table.insert(msgData.tableArray, entryTableData)

  return msgType, msgData
end

function threads_cmdToUi_hideTime_buildMessageData(commandObject)
  if type(commandObject) ~= 'table' then
    log(2, 'No command object passed to cmdToUi_hideTime (type)', type(commandObject))
    return nil
  end
  local msgType = 'UI_HIDE_TIME' -- Shared with drToUi
  local msgData = {}
  msgData.eventAorB = commandObject.eventAorB

  return msgType, msgData
end

function threads_cmdToUi_showTime_buildMessageData(commandObject)
  if type(commandObject) ~= 'table' then
    log(2, 'No command object passed to cmdToUi_showTime (type)', type(commandObject))
    return nil
  end
  local msgType = 'UI_SET_TIME' -- Shared with drToUi
  local msgData = {}
  msgData.eventAorB = commandObject.eventAorB

  -- Retrieve current time
  if type(commandObject.timeObject) ~= 'table' or commandObject.timeObject.hour == nil or commandObject.timeObject.min == nil then
    log(2, 'No valid timeObject provided to cmdToUi_showTime (type(timeObject), hour, min)',
          type(commandObject.timeObject), (commandObject.timeObject or {}).hour, (commandObject.timeObject or {}).min)
    return nil
  end

  -- Build string from passed time
  msgData.timeStr = string.format('%02d:%02d', commandObject.timeObject.hour, commandObject.timeObject.min)

  return msgType, msgData
end
