--- List of threads and their configuration

--- Requires

require 'utils.table'

--- Global variables

--- Table describing each thread
-- Additional fields will be added in main thread instance to track running threads
-- luaFile: File in src/threads to pass to startThread
-- threadHandler: Reference to the SDL thread (once created)
threads_list =
{
  ['udp'] = { luaFile='udpThread.lua'},
  ['tcp'] = { luaFile='tcpThread.lua'},
  ['sch'] = { luaFile='schedulerThread.lua'},
  ['ui'] = { luaFile='uiThread.lua'},
  ['cmd'] = { luaFile='commandThread.lua'},
}

--- Private variables

local cachedThreadList = nil -- Built on first use

--- Public functions

function threads_list_isValidThreadId(threadId)
  -- Retrieve threadId list
  local threadIdList = threads_list_threadIdList()
  if threadIdList == nil or #threadIdList == 0 then
    log(2, "Unable to retrieve threadId list")
    return false -- Can't get a list, unable to check if valid threadId, default to not-valid
  end

  -- Look for the treadId in the table
  return table_find(threadId, threadIdList)
end

function threads_list_threadIdList()
  -- Return cached list if previously built
  if cachedThreadList ~= nil then return cachedThreadList end

  -- Build list
  local threadIdList = {}
  table.insert(threadIdList, 'Main')
  for threadId,unused in pairs(threads_list) do
    table.insert(threadIdList, threadId)
  end

  -- Cache list and return
  cachedThreadList = threadIdList
  return threadIdList
end
