--- Manage thread starting and stopping
-- keeps a reference on thread objects

--- Requires

require 'utils.misc'
require 'threads.utils'
require 'threads.drToUi'
require 'dr.stateMachine'
require 'save.diveTimestamp'
local config = require 'config'
local SDL = require 'SDL'
local SDLNet = require 'SDL.net'

--- Constants

currentThreadId = 'sch'
threadName = threads_utils_getThreadName(currentThreadId)
oldPrint = print; print = function(...) oldPrint(threadName,...) end

--- Private variables

local incomingChannel
local stateMachineA
local tickTimer

--- Private functions

local function initThread()
  incomingChannel = SDL.getChannel(threads_utils_getThreadChannelName(currentThreadId))
  if not incomingChannel then FATAL("Unable to open incoming channel") end

  stateMachineA = dr_stateMachine_create({eventAorB='a'})
  if not stateMachineA then FATAL("Unable to create state machine A") end

  -- Init divestamp mechanism
  if config.save.saveDivesTimestamp == true then
    save_diveTimestamp_start()
  end
end

local function sendMessageToUi(msgType, msgData)
  -- Params wll be checked by following funcion

  -- Build the thread message data for UI
  local threadMessageDataString = threads_message_serializeTableToDataString(msgData)
  if type(threadMessageDataString) ~= 'string' then
    log(2, "sendMessageToUi: Unable to serialize msgData table")
    return false
  end

  -- Build and send the message
  local messageObject = threads_message_build(msgType,threadMessageDataString)
  if messageObject == nil then
    log(3, "sendMessageToUi: Unable to build message")
    return false
  end

  if threads_utils_sendMessage('ui', messageObject) ~= true then
    log(3, "sendMessageToUi: Unable to send message")
    return false
  end

  return true
end

local function schThreadLoop()
  while true do
    --wait and parse next channel message if any
    local channelMsgString = incomingChannel:wait()
    if channelMsgString ~= nil then -- Not needed anymore
      incomingChannel:pop()
      threads_message_logMessage(4, "Incoming message", channelMsgString)
      local msgSrc, msgType, msgData = threads_message_parse(channelMsgString)

      --handle message
      if msgType == "TERMINATE" then
        return -- terminate thread
      elseif msgType == "NEW_UPDATE_OBJECT" or msgType == "NEW_COMMAND_OBJECT" or msgType == "SCH_TICK" then
        local outcomeFunctionArray

        -- Have the stateMachine process either the update, the command or the tick
        if msgType == "NEW_UPDATE_OBJECT" then
          local updateObject = threads_message_deserializeTableFromDataString(msgData)
          if updateObject == nil then
            log(2, "No Update object passed with NEW_UPDATE_OBJECT message")
          else
            log(4, "Update object (type)", updateObject.typeStr)
            -- Process update object in state machine
            outcomeFunctionArray = stateMachineA:processUpdate(updateObject)
          end
        elseif msgType == "NEW_COMMAND_OBJECT" then
          local commandObject = threads_message_deserializeTableFromDataString(msgData)
          if commandObject == nil then
            log(2, "No command object passed with NEW_COMMAND_OBJECT message")
          else
            log(4, "Command object (type)", commandObject.typeStr)
            -- Process command object in state machine
            outcomeFunctionArray = stateMachineA:processCommand(commandObject)
          end
        elseif msgType == "SCH_TICK" then
          -- Process tick (no data) in state machine
          outcomeFunctionArray = stateMachineA:processTick()
        end

        if type(outcomeFunctionArray) == 'table' then
          -- Build ui messages from state machine processing outcome
          if #(outcomeFunctionArray or {}) > 0 then
            if config.Ui.useFramerate ~= true then
              table.insert(outcomeFunctionArray, {func=threads_drToUi_render_buildMessageData, data=stateMachineA}) -- Finish with a render
            end
            for i,outcomeFunction in ipairs(outcomeFunctionArray or {}) do
              if type(outcomeFunction) ~= 'table' or  type(outcomeFunction.func) ~= 'function' then
                log(2, 'Outcome function array entry is not a {function, data} pair (index, type, type(func))', i, type(outcomeFunction), type((outcomeFunction or {}).func))
              else
                local msgType, msgData = outcomeFunction.func(outcomeFunction.data)
                if msgType ~= nil then
                  if sendMessageToUi(msgType, msgData) ~= true then
                    log(2, 'Unable to send message drToUi message to ui')
                  end
                end
              end
            end -- For all outcomeFunction
          end -- If some outcome functions
        end -- if the message was processed into outcome functions

      end
    end

  end
end

local function deinitThread()
  -- Destroy tick SDL timer
  if tickTimer ~= nil then
    tickTimer:remove()
    tickTimer = nil
  end
end

local function tickTimerCb(interval)
  require 'threads.utils'
  currentThreadId = 'sch' -- Emulate the message being sent from the scheduler
  threads_utils_sendMessage('sch',threads_message_build('SCH_TICK'))
  return nil
end

--- Public functions

function scheduleTick(period, data)
  -- Check params
  if type(period) ~= 'number' or period < 1 then
    log(2, 'Unable to start tick timer, invalid parameters (type, value)', type(period), period)
    return
  end

  -- Start timer if not already running
  if tickTimer ~= nil then
    log(2, 'Tick timer already running, aborting new start')
  end

  log(3, 'Starting SCH tick timer (period)', period)
  tickTimer, err = SDL.addTimer(period*1000, tickTimerCb)
  if tickTimer == nil then
    log(2, 'Unable to create ttl timer, addTimer failed ('..(err or 'nil')..')')
  end
end

--- Thread execution

log(4,'Thread startup')
initThread()
schThreadLoop()
deinitThread()
log(4,'Thread shutdown')
return 0
