--- Manage thread starting and stopping
-- keeps a reference on thread objects

--- Requires

require 'utils.misc'
require 'threads.utils'
require 'threads.drToUi'
require 'threads.cmdToUi'
require 'ui.colorSwatch'
require 'ui.fontManager'
require 'ui.screenLayout'
require 'ui.renderer' -- Some of these are probably not necessary anymore (drToUi is taking care of it)
require 'ui.banner'
local config = require 'config'
SDL = require 'SDL'
SDLNet = require 'SDL.net'
SDLFont  = require "SDL.ttf"
SDLImage = require "SDL.image"

--- Constants

currentThreadId = 'ui'
threadName = threads_utils_getThreadName(currentThreadId)
oldPrint = print; print = function(...) oldPrint(threadName,...) end

--- Private variables

local incomingChannel
local ttlTimer
local ttlCounterMs
local ttlReminderArray -- Array of {endOfLifeCounter, uiObjectCollectionKeyToDelete}
local uiObjectCollections -- We'll store the eventInfo, diveCard, ... objects in here, until a better system is needed

--- Private functions

local function sendDatalessMessageToUi(msgType)
  -- Build and send the message
  local messageObject = threads_message_build(msgType, nil)
  if messageObject == nil then
    log(3, "sendMessageToUi: Unable to build message")
    return false
  end

  if threads_utils_sendMessage('ui', messageObject) ~= true then
    log(3, "sendMessageToUi: Unable to send message")
    return false
  end

  return true
end

local function timeToLiveTimerCb(interval)
  require 'threads.utils'
  currentThreadId = 'ui' -- Emulate the message being sent from the ui
  threads_utils_sendMessage('ui',threads_message_build('UI_TTL_TICK'))
  return interval
end

local function initThread()
  incomingChannel = SDL.getChannel(threads_utils_getThreadChannelName(currentThreadId))
  if not incomingChannel then FATAL("Unable to open incoming channel") end

  ui_renderer_init()

  ui_fontManager_checkFontsAvailability()
  ui_colorSwatch_init()

  uiObjectCollections = {}

  ui_screenLayout_createAllLayouts()

  -- Create SDL timer for time to live (if using feature)
  if config.Ui.useTtl == true then
    log(3, 'Starting UI TTL timer')
    ttlTimer, err = SDL.addTimer(config.Ui.ttlPeriodMs, timeToLiveTimerCb)
    if ttlTimer == nil then
      log(2, 'Unable to create ttl timer, addTimer failed ('..(err or 'nil')..')')
    end
    ttlCounterMs = 0
    ttlReminderArray = {}
  else
    log(3, 'UI TTL is not enabled')
  end

  if config.general.autostart == true then
    -- Start and render
    sendDatalessMessageToUi('UI_INIT_DISPLAY')
    sendDatalessMessageToUi('UI_FRAMETICK')
  end
end

local function uiThreadLoop()
  while true do
    --wait and parse next channel message if any
    local channelMsgString = incomingChannel:wait()
    if channelMsgString ~= nil then -- Not needed anymore
      incomingChannel:pop()
      threads_message_logMessage(4, "Incoming message", channelMsgString)
      local msgSrc, msgType, msgData = threads_message_parse(channelMsgString)

      --handle message
      if msgType == "TERMINATE" then
        return -- terminate thread
      elseif msgType == "UI_INIT_DISPLAY" then
        ui_renderer_configure()
        ui_renderer_start()
        ui_screenLayout_setScreenSize(ui_renderer_getScreenSize())
      elseif msgType == "UI_FRAMETICK" then
        ui_renderer_frametick()
      elseif msgType == "UI_TTL_TICK" then
        local newRenderingRequired = false
        ttlCounterMs = ttlCounterMs + config.Ui.ttlPeriodMs
        log(5, 'TTL counter is ', ttlCounterMs)
        for i,reminder in pairs(ttlReminderArray or {}) do
          if reminder.endOfLifeCounter <= ttlCounterMs then
            local keyToDelete = reminder.uiObjectCollectionKeyToDelete
            if type(keyToDelete) == 'string' and type(uiObjectCollections[keyToDelete]) == 'table' then
              log(4, 'Ui objects TTL expired (key, count)', keyToDelete, #uiObjectCollections[keyToDelete])
              ui_renderer_removeMany(uiObjectCollections[keyToDelete])
              newRenderingRequired = true -- TODO, only if actual objects removed from renderer (check future return)
              uiObjectCollections[keyToDelete] = nil -- Remove objects to align with renderer
            else
              log(2, 'No ui object collection TTL expiration (key)', keyToDelete)
            end
             ttlReminderArray[i] = nil -- Remove reminder --TODO, check if this is legal in a pair() loop
          end
        end
        if newRenderingRequired == true and config.Ui.useFramerate == false then
          ui_renderer_frametick() -- Can't rely on the framerate to force a render
        end
      elseif msgType == "UI_CLEAR_ALL" then
        ui_renderer_removeAll()
        uiObjectCollections = {}
      elseif msgType == "UI_SET_EVENT_INFO" then
        local newUiObjectArray = threads_drToUi_setEventInfo_buildUiObjects(msgData)
        if #(newUiObjectArray or {}) > 0 then
          ui_renderer_removeMany(uiObjectCollections.eventInfoObjectsArray)
          ui_renderer_addMany(newUiObjectArray)
          uiObjectCollections.eventInfoObjectsArray = newUiObjectArray
        end

      elseif msgType == "UI_SET_DIVE_INFO" then
        local newUiObjectArray = threads_drToUi_setDiveInfo_buildUiObjects(msgData)
        if #(newUiObjectArray or {}) > 0 then
          -- Remove old ttl (if any)
          if config.Ui.useTtl == true then
            local ttlFinder = table_finderBuilder_keyValue('uiObjectCollectionKeyToDelete')
            local previousDiveTTlExists, previousTtlReminderArrayIndex = table_find('dive', ttlReminderArray, ttlFinder)
            if previousDiveTTlExists == true then
              log(4,'Drop outdated TTL counter for dive to ', ttlReminderArray[previousTtlReminderArrayIndex].endOfLifeCounter)
              table.remove(ttlReminderArray, previousTtlReminderArrayIndex)
            end
          end
          -- Update renderer objects
          ui_renderer_removeMany(uiObjectCollections.dive)
          ui_renderer_addMany(newUiObjectArray)
          uiObjectCollections.dive = newUiObjectArray
          -- Create new ttl
          if config.Ui.useTtl == true then
            local ttlReminder = {
              endOfLifeCounter=ttlCounterMs + config.Ui.ttlDiveInfoMs,
              uiObjectCollectionKeyToDelete='dive'}
            log(3,'Set TTL counter for dive to ', ttlReminder.endOfLifeCounter)
            table.insert(ttlReminderArray, ttlReminder) -- Auto clear after some time
          end
        end

      elseif msgType == "UI_SET_TABLES" then
        local newUiObjectArray = threads_drToUi_setTables_buildUiObjects(msgData)
        if #(newUiObjectArray or {}) > 0 then
          ui_renderer_removeMany(uiObjectCollections.table)
          ui_renderer_addMany(newUiObjectArray)
          uiObjectCollections.table = newUiObjectArray
        end

      elseif msgType == "UI_SET_TIME" then
        local newUiObjectArray = threads_drToUi_setTime_buildUiObjects(msgData)
        if #(newUiObjectArray or {}) > 0 then
          ui_renderer_removeMany(uiObjectCollections.time)
          ui_renderer_addMany(newUiObjectArray)
          uiObjectCollections.time = newUiObjectArray
        end

      elseif msgType == "UI_HIDE_TIME" then
        ui_renderer_removeMany(uiObjectCollections.time)
        uiObjectCollections.time = nil

      elseif msgType == "UI_SET_MEDALS" then
        local newUiObjectArray = threads_drToUi_setMedals_buildUiObjects(msgData)
        if #(newUiObjectArray or {}) > 0 then
          ui_renderer_removeMany(uiObjectCollections.medals)
          ui_renderer_addMany(newUiObjectArray)
          uiObjectCollections.medals = newUiObjectArray
        end
      end -- msgType ifelse
    end

  end
end

local function deinitThread()
  -- Destroy Ttl SDL timer
  if ttlTimer ~= nil then
    ttlTimer:remove()
    ttlTimer = nil
  end
  ttlReminderArray = {} -- Delete all pending ttl reminder
  ui_renderer_stop()
end

--- Thread execution

log(4,'Thread startup')
initThread()
uiThreadLoop()
deinitThread()
log(4,'Thread shutdown')
return 0
