--- Build ui objects from dr objects
-- Operate on each side of the sceduler -> ui thread messaging

--- Requires

require 'ui.renderer'
require 'ui.banner'
require 'ui.colorSwatch'
require 'ui.diveCard'
require 'ui.screenLayout'
require 'ui.table'
require 'ui.tickerRow'
local config = require 'config'

--- Constants

-- Layout constants
local layoutDiv = 10
-- Event banners (info, name, ticker)
local layoutDiv = 10
local layoutEventBannerWRatio = 2
local layoutEventBannerHRatio = 1/2
local layoutEventBannerExtendToLeftSide = true
local layoutEventBannerXDivPos = 0
local layoutEventBannerBaseYDivPos = 0
-- Time banner
local layoutTimeWRatio = 3/4
local layoutTimeHRatio = 1/2
local layoutTimeXDivPos = (layoutDiv - layoutTimeWRatio)/2 -- centered
local layoutTimeYDivPos = 0

-- Table
local layoutTableIntertableMarginYDivCount = 0.5
-- Medal banners (medal name, persons)
local layoutMedalBannerXDivPos = { [2] = 0, [1] = 3.5, [3] = 7, }
local layoutMedalBannerYDivPos = 9.5 -- Medal name position (person banners will stack above)
local layoutMedalBannerWRatio = 3
local layoutMedalBannerHRatio = 0.5

-- TODO, extract language constants
local penaltyNumberToString =
{
  [0] = nil, -- No penalty
  [1] = 'Pénalité : 0 (Plongeon manqué)',
  [2] = 'Pénalité : -2 (Faux départ)',
  [3] = 'Pénalité : <=2 (Mauvaise position)',
  [4] = 'Pénalité : <=4.5 (Position des bras)',
}

--- Private variables

--- Private functions

--- Public functions

function threads_drToUi_inEventInfo_buildMessageData(stateMachine)
  if type(stateMachine) ~= 'table' then
    return nil
  end

  local msgType = 'UI_SET_EVENT_INFO'
  local msgData = {}

  msgData.eventAorB = stateMachine.eventAorB
  local v = stateMachine.overallInfo -- shortcut
  if v == nil then
    log(2, 'State machine does not contain overallInfo field')
    return nil
  end
  -- TODO, extract language constants
  msgData.infoStr = 'Série  ' .. (v.round or '?') .. '/' .. (v.roundCount or '?') ..
                    ' Plongeur ' .. (v.diver or '?') .. '/' .. (v.diverCount or '?')

  msgData.eventNameArray = {}
  -- The following will be filled during the loop
  msgData.activeEventIndex = nil
  msgData.activeEventAwarded = nil
  msgData.eventTickerDataArray = {} -- 2D table [eventId][entryId]

  for n,eventObject in pairs(stateMachine.ongoingEventsArray or {}) do
    local eventName = eventObject.eventName
    if type(eventName) ~= 'string' or eventName == '' then
      log(2, 'No eventName in statemachine n-th ongoingEventArray entry, skipping from in SET_EVENT_INFO message', n)
    else
      table.insert(msgData.eventNameArray, eventName)
      if stateMachine.activeEvent == eventName then
        -- Get active event basic info
        msgData.activeEventIndex = #msgData.eventNameArray
        msgData.activeEventAwarded = eventObject.awarded
      end

      -- Create tickerData for this event from the entry array fields (use the already sorted by points entry array)
      msgData.eventTickerDataArray[n] = {} -- Create ticker data array
      for _,entry in ipairs(eventObject.entryArray or {}) do
        local trigramArray = nil
        -- Build person array
        for _,diver in ipairs(entry.diverArray or {}) do
          trigramArray = trigramArray or {}
          table.insert(trigramArray, diver.trigram)
        end
        local tickerData = {
          trigramArray = trigramArray,
          rank = entry.eventAwareRankStr or '-',
          points = string.format("%.2f", entry.points),
        }
        -- Fade text if current entry did not receive an award for this event latest awarded round
        if type(entry.lastAwardedRound) == 'number' and type(eventObject.eventAwareLastAwardedRound) == 'number' then
          if entry.lastAwardedRound < eventObject.eventAwareLastAwardedRound then
            tickerData.fadedText = true
          end
        end
        table.insert(msgData.eventTickerDataArray[n], tickerData)
      end -- ticker data for current array building loop
    end
  end -- ongoing events array loop

  return msgType, msgData
end

function threads_drToUi_medalPresentationEvent_buildMessageData(updateObject)
  if type(updateObject) ~= 'table' then
    return nil
  end

  local msgType = 'UI_SET_EVENT_INFO'
  local msgData = {}

  -- Event info field just says it's a medal ceremony
  msgData.infoStr = 'Remise des médailles'  -- TODO, extract language constants

  -- And there's only one event
  msgData.eventNameArray = {updateObject.eventName}
  msgData.activeEventIndex = updateObject.eventName

  return msgType, msgData
end

function threads_drToUi_setEventInfo_buildUiObjects(msgDataSerialized)
  --deserialize the data or return an error
  local eventInfoObject = threads_message_deserializeTableFromDataString(msgDataSerialized)
  if eventInfoObject == nil then
    log(2, "No event info object passed with SET_EVENT_INFO message")
    return
  end

  -- TODO, handle eventAorB
  local eventScreenLayout=ui_screenLayout_getLayout()
  local eventInfoBgColor = ui_colorSwatch_get('eventInfo','bg')
  local eventInfoTxtColor = ui_colorSwatch_get('eventInfo','txt')
  local eventNameBgColor = ui_colorSwatch_get('eventName','bg')
  local eventNameTxtColor = ui_colorSwatch_get('eventName','txt')
  local eventNameFadedTxtColor = ui_colorSwatch_get('eventName','txtFaded')
  local tickerBgColor = ui_colorSwatch_get('ticker','bg')
  local tickerTxtColor = ui_colorSwatch_get('ticker','txt')
  local tickerFadedTxtColor =  ui_colorSwatch_get('ticker','txtFaded')

  --Build ui objects
  local uiObjectArray = {}

  local layoutParamTable = {wDivisor=layoutDiv, hDivisor=layoutDiv} -- y is set for each banner
  layoutParamTable.xDivCount = layoutEventBannerXDivPos
  layoutParamTable.wDivCount = layoutEventBannerWRatio
  layoutParamTable.hDivCount = layoutEventBannerHRatio
  layoutParamTable.extendToLeftEdge = true

  -- 1 event info
  layoutParamTable.yDivCount = layoutEventBannerBaseYDivPos
  local infoGeo = eventScreenLayout:getScreenFraction(layoutParamTable)

  local eventInfoUiObject = ui_banner_create({
    text=eventInfoObject.infoStr,
    fontName='medium',
    bgColor=eventInfoBgColor,
    textColor=eventInfoTxtColor,
    textAlign='right',
    w=infoGeo.w, h=infoGeo.h, x=infoGeo.x, y=infoGeo.y})
  if eventInfoUiObject == nil then
    log(2, 'Unable to create event info ui banner')
  else
    table.insert(uiObjectArray, eventInfoUiObject)
  end

  -- n event names
  local nextEventLayoutYPos = layoutParamTable.yDivCount + layoutParamTable.hDivCount
  for eventNameIndex,eventName in ipairs(eventInfoObject.eventNameArray or {}) do
    layoutParamTable.yDivCount = nextEventLayoutYPos
    local eventGeo = eventScreenLayout:getScreenFraction(layoutParamTable)

    local bannerTextColor = eventNameTxtColor -- Fade if not the active event
    if eventInfoObject.activeEventIndex ~= eventNameIndex then
      bannerTextColor = eventNameFadedTxtColor
    end

    local eventNameUiObject = ui_banner_create({
      text=eventName,
      fontName='light',
      textColor=bannerTextColor,
      bgColor=eventNameBgColor,
      textAlign='right',
      w=eventGeo.w, h=eventGeo.h, x=eventGeo.x, y=eventGeo.y})
    if eventNameUiObject == nil then
      log(2, 'Unable to create event name ui banner')
    else
      table.insert(uiObjectArray, eventNameUiObject)
    end
    nextEventLayoutYPos = nextEventLayoutYPos + layoutParamTable.hDivCount -- Update y pos

    -- Create ticker for the event
    if eventInfoObject.eventTickerDataArray ~= nil and eventInfoObject.eventTickerDataArray[eventNameIndex] ~= nil then
      -- Check if configured to be shown at this stage and for this event
      local showTicker = false
      local trigramOnly = false
      if (eventInfoObject.activeEventAwarded == false) and (config.Ui.ticker.showTickerAfterDive) then
        showTicker = true
        trigramOnly = config.Ui.ticker.beforeDiveMinimalTicker
      end
      if (eventInfoObject.activeEventAwarded == true) and (config.Ui.ticker.showTickerAfterDive) then
        showTicker = true
        trigramOnly = config.Ui.ticker.afterDiveMinimalTicker
      end
      if eventInfoObject.activeEventIndex ~= eventNameIndex and config.Ui.ticker.onlyShowTickerForActiveEvent then
        showTicker = false -- Disable ticker on non-active event according to configuration
      end

      if showTicker == true then
        -- Create ticker rows
        for tickerIndex, tickerData in ipairs(eventInfoObject.eventTickerDataArray[eventNameIndex] or {}) do
          local tickerRowParamTable = {textColor=tickerTxtColor, bgColor=tickerBgColor, screenLayout=eventScreenLayout}
          if tickerData.fadedText == true then
            tickerRowParamTable.textColor = tickerFadedTxtColor
          end
          tickerRowParamTable.textAlign = 'left'
          tickerRowParamTable.yDivPos = nextEventLayoutYPos
          tickerRowParamTable.trigramTextArray = tickerData.trigramArray
          tickerRowParamTable.rankText = tickerData.rank
          tickerRowParamTable.pointsText = tickerData.points
          tickerRowParamTable.trigramOnly = trigramOnly

          local tickerRowUiObject = ui_tickerRow_create(tickerRowParamTable)
          if tickerRowUiObject == nil then
            log(2, 'Unable to create active event n-th ui ticker row (n)', tickerIndex)
          else
            table.insert(uiObjectArray, tickerRowUiObject)
          end
          nextEventLayoutYPos = nextEventLayoutYPos + layoutParamTable.hDivCount -- Update y pos

        end
      end
    end

  end -- event loop

  return uiObjectArray
end

function threads_drToUi_setTime_buildUiObjects(msgDataSerialized)
  --deserialize the data or return an error
  local timeObject = threads_message_deserializeTableFromDataString(msgDataSerialized)
  if timeObject == nil then
    log(2, "No event info object passed with SET_TIME message")
    return
  end

  -- TODO, handle eventAorB
  local eventScreenLayout=ui_screenLayout_getLayout()
  local timeBgColor = ui_colorSwatch_get('time','bg')
  local timeTxtColor = ui_colorSwatch_get('time','txt')

  --Build ui objects
  local uiObjectArray = {}

  local layoutParamTable = {wDivisor=layoutDiv, hDivisor=layoutDiv} -- y is set for each banner
  layoutParamTable.xDivCount = layoutTimeXDivPos
  layoutParamTable.yDivCount = layoutTimeYDivPos
  layoutParamTable.wDivCount = layoutTimeWRatio
  layoutParamTable.hDivCount = layoutTimeHRatio
  local timeGeo = eventScreenLayout:getScreenFraction(layoutParamTable)

  local timeUiObject = ui_banner_create({
    text=timeObject.timeStr,
    fontName='medium',
    bgColor=timeBgColor,
    textColor=timeTxtColor,
    textAlign='center',
    w=timeGeo.w, h=timeGeo.h, x=timeGeo.x, y=timeGeo.y})
  if timeUiObject == nil then
    log(2, 'Unable to create time ui banner')
  else
    table.insert(uiObjectArray, timeUiObject)
  end

  return uiObjectArray
end

function threads_drToUi_render_buildMessageData(stateMachine)
  if type(stateMachine) ~= 'table' then
    return nil
  end

  local msgType = 'UI_FRAMETICK'
  local msgData = {}
  msgData.eventAorB = stateMachine.eventAorB

  -- Nothing else to pass
  return msgType, msgData
end

function threads_drToUi_clearAll_buildMessageData(stateMachine)
  if type(stateMachine) ~= 'table' then
    return nil
  end

  local msgType = 'UI_CLEAR_ALL'
  local msgData = {}
  msgData.eventAorB = stateMachine.eventAorB

  -- Nothing else to pass
  return msgType, msgData
end

function threads_drToUi_inEventDive_buildMessageData(stateMachine)
  if type(stateMachine) ~= 'table' then
    return nil
  end

  local msgType = 'UI_SET_DIVE_INFO'
  local msgData = {}

  msgData.eventAorB = stateMachine.eventAorB
  --Temporary shortcut
  local _,activeEventEntryIndexInOngoingEventsArray = table_find(stateMachine.activeEvent, stateMachine.ongoingEventsArray, table_finderBuilder_keyValue('eventName'))
  if activeEventEntryIndexInOngoingEventsArray == nil then
    log(2, 'State machine does not contain active event object', stateMachine.activeEvent)
    return nil
  end
  if stateMachine.ongoingEventsArray[activeEventEntryIndexInOngoingEventsArray] == nil then
    log(2, 'Internal error, invalid return table_find when looking for active event in state machine ongoingEventArray')
    return nil
  end
  local w = stateMachine.ongoingEventsArray[activeEventEntryIndexInOngoingEventsArray].entryArray[stateMachine.activeEntryIndex]
  if w == nil then
    log(2, 'State machine does not contain active entry field') --TODO, improve
    return nil
  end

  -- Build simple fields
  msgData.rank = w.eventAwareRankStr or '-'
  msgData.diveNumber = '' .. (w.diveNumber or '???') .. (w.divePosition or '?')
  msgData.diveHeight = '' .. (w.height or '?') .. 'm'
  msgData.diveDD = w.dd
  if type(w.diveName) == 'string' and string.len(w.diveName) > 0 then
    -- Fix first letter case
    local firstLetter = (w.diveName:sub(1,1) or '')
    w.diveName = string.upper(firstLetter) .. (w.diveName:sub(2) or '')
    w.diveName = w.diveName:gsub('½', '1/2') -- The font doesn't handle the 1/2 char
  end
  local positionText = {['A']='droit', ['B']='carpé', ['C']='groupé', ['D']='libre'} -- TODO, extract language constants
  msgData.diveName = (w.diveName or '') .. ', ' .. (positionText[w.divePosition] or '')
  -- Convert numbers to strings
  if type(w.points) == 'number' then
    msgData.points = string.format("%.2f", w.points)
  end
  if type(w.diveTotal) == 'number' then
    msgData.diveTotal = string.format("%.2f", w.diveTotal)
  end
  -- Build person array
  msgData.personArray = {}
  for _,diver in ipairs(w.diverArray or {}) do
    local person = {name=diver.fullName, club=diver.club, country=diver.country}
    table.insert(msgData.personArray, person)
  end
  -- Build Awards Array
  msgData.awards = w.awardArray
  msgData.awardsUsed = w.awardUsedArray
  msgData.synchro = w.synchro
  msgData.executionAwardCount = w.executionAwardCount
  msgData.penalty = penaltyNumberToString[w.penalty or 0]

  return msgType, msgData
end

function threads_drToUi_setDiveInfo_buildUiObjects(msgDataSerialized)
  --deserialize the data or return an error
  local diveInfoObject = threads_message_deserializeTableFromDataString(msgDataSerialized)
  if diveInfoObject == nil then
    log(2, "No event info object passed with UI_SET_DIVE_INFO message")
    return
  end

  -- TODO, handle eventAorB
  local diveCardScreenLayout=ui_screenLayout_getLayout()
  local diveCardBgColor = ui_colorSwatch_get('diveCard','bg')
  local diveCardTxtColor = ui_colorSwatch_get('diveCard','txt')
  local diveCardFadedTxtColor =  ui_colorSwatch_get('diveCard','txtFaded')
  local diveCardPenaltyBgColor = ui_colorSwatch_get('diveCard','bgPenalty')

  --Build ui object
  local uiObjectArray = {}

  -- dive card
  local diveCardUiObject = ui_diveCard_create({
    personArray=diveInfoObject.personArray,
    rank=diveInfoObject.rank,
    points=diveInfoObject.points,
    bgColor=diveCardBgColor,
    textColor=diveCardTxtColor,
    textFadedColor=diveCardFadedTxtColor,
    penaltyBgColor=diveCardPenaltyBgColor,
    screenLayout=diveCardScreenLayout,
    diveNumber=diveInfoObject.diveNumber,
    diveHeight=diveInfoObject.diveHeight,
    diveDD=diveInfoObject.diveDD,
    diveName=diveInfoObject.diveName,
    awards=diveInfoObject.awards,
    awardsUsed=diveInfoObject.awardsUsed,
    synchro=diveInfoObject.synchro,
    executionAwardCount=diveInfoObject.executionAwardCount,
    diveTotal=diveInfoObject.diveTotal,
    penalty=diveInfoObject.penalty,
  })
  if diveCardUiObject == nil then
    log(2, 'Unable to create event info ui banner')
  else
    table.insert(uiObjectArray, diveCardUiObject)
  end

  return uiObjectArray
end

function threads_drToUi_inEventRanking_buildMessageData(stateMachine)
  if type(stateMachine) ~= 'table' then
    return nil
  end

  local msgType = 'UI_SET_TABLES'
  local msgData = {}

  msgData.eventAorB = stateMachine.eventAorB
  msgData.firstHalfRowToDisplay = stateMachine.tableScrollFirstHalfRowToDisplay
  msgData.tableArray = {}

  -- Create one table per separate event
  for _,eventData in pairs(stateMachine.ongoingEventsArray or {}) do
    local tableData = {}
    tableData.title1 = eventData.eventName

    -- Build title2 string (whether or not all divers in the event have completed the same round)
    -- TODO protect against non-numeric values
    -- TODO, extract language constants
    if eventData.eventAwareCompletedRound == nil then
      tableData.title2 = 'Résultats intermédiaires en cours de série ' .. (eventData.round or '?')
    else
      tableData.title2 = 'Résultats intermédiaires après ' .. (eventData.eventAwareCompletedRound or '?') .. ' séries de plongeons' -- TODO plural handling
    end

    -- Use the already sorted by points entry array to create one row per entry
    tableData.rowArray = {}
    for _,entry in ipairs(eventData.entryArray or {}) do
      local row = {
        personArray = {},
        firstColumnText = entry.eventAwareRankStr or '-',
        lastColumnText = string.format("%.2f", entry.points),
      }
      -- Build person array
      for _,diver in ipairs(entry.diverArray or {}) do
        local person = {name=diver.fullName, club=diver.club, country=diver.country}
        table.insert(row.personArray, person)
      end
      table.insert(tableData.rowArray, row)
    end

    -- Insert the table for this event in the tableArray
    table.insert(msgData.tableArray, tableData)
  end

  -- Add the Meet name as a footer of the last table
  if #msgData.tableArray > 0 then
    msgData.tableArray[#msgData.tableArray].footerText = stateMachine.meetName
  end

  return msgType, msgData
end

function threads_drToUi_noEventRanking_buildMessageData(updateObject)
  if type(updateObject) ~= 'table' then
    return nil
  end

  local msgType = 'UI_SET_TABLES'
  local msgData = {}

  msgData.eventAorB = updateObject.eventAorB
  msgData.firstHalfRowToDisplay = updateObject.tableScrollFirstHalfRowToDisplay
  msgData.tableArray = {}

  -- Create one table for all the entries
  local entryTableData = {}
  entryTableData.title1 = updateObject.eventName or ' '
  -- TODO, extract language constants
  entryTableData.title2 = 'Résultats définitifs' -- TODO, we assume the event is always over
  entryTableData.footerText = updateObject.meetName

  -- Create a row per entry, sorted by points
  entryTableData.rowArray = {}
  for _,entry in ipairs(updateObject.entryArray or {}) do
    -- Handle guest diver
    if type(entry.rank) == 'string' and entry.rank:sub(1,1) == '-' then
      entry.rank = '(' .. entry.rank:sub(2) .. ')'
    end
    local row = {
      personArray = {{name=entry.name, club=entry.club, country=entry.country}},
      firstColumnText = entry.rank,
      lastColumnText = string.format("%.2f", entry.points),
    }
    table.insert(entryTableData.rowArray, row)
  end

  -- Insert the table in the tableArray
  table.insert(msgData.tableArray, entryTableData)

  return msgType, msgData
end

function threads_drToUi_startList_buildMessageData(updateObject)
  if type(updateObject) ~= 'table' then
    return nil
  end

  local msgType = 'UI_SET_TABLES'
  local msgData = {}

  msgData.eventAorB = updateObject.eventAorB
  msgData.firstHalfRowToDisplay = updateObject.tableScrollFirstHalfRowToDisplay
  msgData.tableArray = {}

  -- Create one table for all the entries
  local entryTableData = {}
  entryTableData.title1 = updateObject.eventName or ' '
  entryTableData.title2 = 'Ordre de départ'
  entryTableData.footerText = updateObject.meetName

  -- Create a row per entry, sorted by points
  entryTableData.rowArray = {}
  for _,entry in ipairs(updateObject.entryArray or {}) do
    local row = {
      personArray = {{name=entry.name, club=entry.club, country=entry.country}},
      firstColumnText = entry.startOrder,
      lastColumnText = nil,
    }
    table.insert(entryTableData.rowArray, row)
  end

  -- Insert the table in the tableArray
  table.insert(msgData.tableArray, entryTableData)

  return msgType, msgData
end

function threads_drToUi_judgeList_buildMessageData(updateObject)
  if type(updateObject) ~= 'table' then
    return nil
  end

  local msgType = 'UI_SET_TABLES'
  local msgData = {}

  msgData.eventAorB = updateObject.eventAorB
  msgData.firstHalfRowToDisplay = updateObject.tableScrollFirstHalfRowToDisplay
  msgData.tableArray = {}

  -- Create one table for all the judges
  local judgeTableData = {}
  judgeTableData.title1 = updateObject.eventName or ' '
  judgeTableData.title2 = 'Liste des juges' -- TODO, handle jury A and B some day?
  judgeTableData.footerText = updateObject.meetName

  -- Create a row per entry, sorted by points
  judgeTableData.rowArray = {}
  for _,judge in ipairs(updateObject.judgeArray or {}) do
    local row = {
      personArray = {{name=judge.name, country=judge.country}},
      firstColumnText = judge.role,
      lastColumnText = nil,
    }
    table.insert(judgeTableData.rowArray, row)
  end

  -- Insert the table in the tableArray
  table.insert(msgData.tableArray, judgeTableData)

  return msgType, msgData
end

function threads_drToUi_setTables_buildUiObjects(msgDataSerialized)
  --deserialize the data or return an error
  local msgData = threads_message_deserializeTableFromDataString(msgDataSerialized)
  if msgData == nil then
    log(2, "No msgData passed with UI_SET_TABLES message")
    return
  end

  -- TODO, handle eventAorB
  local tableScreenLayout = ui_screenLayout_getLayout()
  --TODO, full integration with screen layout
  local maxHalfRowCount = config.Ui.tableMaxRowCount -- TODO this should combine with the layout (the value should be halved)
  local personRowHeightCoef = function(count)  -- TODO only the table/font_manager can tell us this...
    return 4/3+((2/3) * ((count or 1)-1))
  end
  local title1RowCount = 2 -- TODO align with ui/table consts
  local title2RowCount = 1
  local footerRowCount = 1
  local interTableGapRowCount = 1
  local reservedFirstDisplayedTableHeaderRowCount = title1RowCount + title2RowCount
  maxHalfRowCount = maxHalfRowCount - reservedFirstDisplayedTableHeaderRowCount

  -- Get colors
  local tableTitle1BgColor = ui_colorSwatch_get('table','bgTitle1')
  local tableTitle2BgColor = ui_colorSwatch_get('table','bgTitle2')
  local tableRowBgColor = ui_colorSwatch_get('table','bgRow')
  local tableFooterBgColor = ui_colorSwatch_get('table','bgFooter')
  local tableTxtColor = ui_colorSwatch_get('table','txt')

  --TODO, check there's a table array

  -- Prune rowEntries (and possibly tables) to enforce firstHalfRowToDisplay
  local dropTableArray = nil -- array of table[] = true/false
  local dropTableRow2dArray = nil -- 2d array of table[].row[] = true/false and .footer - true/false(naive, but will do for now)
  --count
  local halfRowCount = 0
  for i,tableObject in ipairs(msgData.tableArray or {}) do
    if i > 1 then -- not the first table, count the gap between tables, and the extra table headers
      halfRowCount = halfRowCount + interTableGapRowCount
      if tableObject.title1 ~= nil then
        halfRowCount = halfRowCount + title1RowCount
      end
      if tableObject.title2 ~= nil then
        halfRowCount = halfRowCount + title2RowCount
      end
    end
    for j,row in ipairs(tableObject.rowArray) do
      local rowPersonCount = #(((row or {}).personArray) or {}) -- get the number of person in that row
      halfRowCount = halfRowCount + personRowHeightCoef(rowPersonCount) -- Row height depends on number of person in it
    end
    if tableObject.footerText ~= nil then
      halfRowCount = halfRowCount + footerRowCount
    end
  end
  halfRowCount = math.ceil(halfRowCount) -- Round to next halfRow

  log(4, 'Total halfRowCount, max', halfRowCount, maxHalfRowCount)
  -- compute which range could be displayed
  if halfRowCount > maxHalfRowCount then  --if prunning is needed
    local firstHalfRowIdToDisplay = msgData.firstHalfRowToDisplay
    if firstHalfRowIdToDisplay == nil then
      log(2, 'No firstHalfRowIdToDisplay passed to build table ui object')
      firstHalfRowIdToDisplay = 0
    end
    firstHalfRowIdToDisplay = math.ceil(firstHalfRowIdToDisplay)
    local lastHalfIdRowToDisplay = firstHalfRowIdToDisplay + maxHalfRowCount - 1 -- Possibly higher than halfRowCount
    if lastHalfIdRowToDisplay >= halfRowCount then
      -- Rewind to display as much as possible finishing with the last
      lastHalfIdRowToDisplay = math.ceil(halfRowCount)
      firstHalfRowIdToDisplay = lastHalfIdRowToDisplay - maxHalfRowCount + 1
    end
    log(3, 'display range', firstHalfRowIdToDisplay, lastHalfIdRowToDisplay, halfRowCount)
    --drop the rows that aren't in range
    dropTableArray = {} -- array of table[] = true/false
    dropTableRow2dArray = {table = {}} -- 2d array of table[].row[] = true/false (naive, but will do for now)
    local halfRowId = 0
    local displayedTable = 0
    for i,tableObject in ipairs(msgData.tableArray or {}) do
      local previousRowId = halfRowId -- for log purpose
      dropTableArray[i] = false -- init table drop
      dropTableRow2dArray.table[i] = {row = {}, footer = false} -- create row dropArray for this table
      if displayedTable > 0 then -- not the first displayed table, count the gap between tables and the next headers
        -- Gap isn't dropable, ignore it
        previousRowId = halfRowId
        halfRowId = halfRowId + interTableGapRowCount
        log(4,'                  (tableId, field, top, bottom)', i, 'gap to prev', math.floor(previousRowId,2), math.floor(halfRowId,2))
        if tableObject.title1 ~= nil then
          if halfRowId < firstHalfRowIdToDisplay then
            log(4,'Dropping title1 because too high (tableId)', i)
            dropTableArray[i] = true  -- drop the whole table (title1 and/or title2 doesn't fit)
          end
          previousRowId = halfRowId
          halfRowId = halfRowId + title1RowCount
          log(4,'                  (tableId, field, top, bottom)', i, 'title1', math.floor(previousRowId,2), math.floor(halfRowId,2))
          if halfRowId > (lastHalfIdRowToDisplay + 1)  then
            log(4,'Dropping title1 because too low (tableId)', i)
            dropTableArray[i] = true  -- drop the whole table (title1 and/or title2 doesn't fit)
          end
        end
        if tableObject.title2 ~= nil then
          if halfRowId < firstHalfRowIdToDisplay then
            log(4,'Dropping title2 because too high (tableId)', i)
            dropTableArray[i] = true  -- drop the whole table (title1 and/or title2 doesn't fit)
          end
          previousRowId = halfRowId
          halfRowId = halfRowId + title2RowCount
          log(4,'                  (tableId, field, top, bottom)', i, 'title2', math.floor(previousRowId,2), math.floor(halfRowId,2))
          if halfRowId > (lastHalfIdRowToDisplay + 1) then
            log(4,'Dropping title2 because too low (tableId)', i)
            dropTableArray[i] = true  -- drop the whole table (title1 and/or title2 doesn't fit)
          end
        end
      end
      for j,row in ipairs(tableObject.rowArray) do
        dropTableRow2dArray.table[i].row[j] = false -- init row drop for this table
        if halfRowId < firstHalfRowIdToDisplay then
          log(4,'Dropping row because too high (tableId, rowId)', i, j)
          dropTableRow2dArray.table[i].row[j] = true  -- drop the row
        end
        previousRowId = halfRowId
        local rowPersonCount = #(((row or {}).personArray) or {}) -- get the number of person in that row
        halfRowId = halfRowId + personRowHeightCoef(rowPersonCount) -- Row height depends on number of person in it
        log(4,'                  (tableId, rowId, top, bottom)', i, j, math.floor(previousRowId,2), math.floor(halfRowId,2))
        if halfRowId > (lastHalfIdRowToDisplay + 1) then
          log(4,'Dropping row because too low (tableId, rowId)', i, j)
          dropTableRow2dArray.table[i].row[j] = true  -- drop the row
        end
      end
      if tableObject.footerText ~= nil then
        if halfRowId < firstHalfRowIdToDisplay then
          log(4,'Dropping footer because too high (tableId)', i)
          dropTableRow2dArray.table[i].footer = true  -- drop the footer
        end
        previousRowId = halfRowId
        halfRowId = halfRowId + footerRowCount
        log(4,'                  (tableId, field, top, bottom)', i, 'footer', math.floor(previousRowId,2), math.floor(halfRowId,2))
        if halfRowId > (lastHalfIdRowToDisplay + 1) then
          log(4,'Dropping footer because too low (tableId)', i)
          dropTableRow2dArray.table[i].footer = true  -- drop the footer
        end
      end

      -- drop table that have no row left, drop footer if the last row is dropped
      if dropTableArray[i] == false then -- No need to check if the table has already been dropped
        local allRowDropped = true
        local lastRowDropped = false
        for _,rowDropped in ipairs(dropTableRow2dArray.table[i].row) do
          if rowDropped == false then
            allRowDropped = false
          end
          lastRowDropped = rowDropped
        end
        if allRowDropped == true then
          log(3,'Dropping whole table because all row were (tableId)', i)
          dropTableArray[i] = true  -- drop the whole table since it has no rows
        elseif lastRowDropped == true then
          log(3,'Dropping table footer because the last row was (tableId)', i)
          dropTableRow2dArray.table[i].footer = true -- drop the footer since the bottom of the table is truncated
        end
      end

      -- if the table is at least partially displayed, update displayTableCounter
      if dropTableArray[i] == false then
        displayedTable = displayedTable + 1
      end
    end
  end

  local uiObjectArray = {}
  --Build a table ui object for each table in the array
  local nextTableYDivCount = nil
  for i,tableObject in ipairs(msgData.tableArray or {}) do

     -- Bypass checks if the dropTableArrays have not been built
    if dropTableArray == nil or dropTableArray[i] == false then
      -- Select the rows
      local prunedRows = {}
      for j,row in ipairs(tableObject.rowArray) do
        if dropTableArray == nil or dropTableRow2dArray.table[i].row[j] == false then
          table.insert(prunedRows, row)
        end
      end

      -- Possibly clear the footer (truncated table or just not enough space for the footer)
      if dropTableArray ~= nil and dropTableRow2dArray.table[i].footer == true then
        tableObject.footerText = nil
      end

      -- Build ui object
      local tableUiObject = ui_table_create({
        yDivCount = nextTableYDivCount,
        title1Text=tableObject.title1,
        title2Text=tableObject.title2,
        rowEntries=prunedRows,
        footerText=tableObject.footerText,
        title1BgColor=tableTitle1BgColor,
        title2BgColor=tableTitle2BgColor,
        rowBgColor=tableRowBgColor,
        footerBgColor=tableFooterBgColor,
        textColor=tableTxtColor,
        screenLayout=tableScreenLayout,
      })
      -- Add it if created
      if tableUiObject == nil then
        log(2, 'Unable to create table ui table (index)', i)
      else
        table.insert(uiObjectArray, tableUiObject)
      end
      -- Make the next one start one line below
      nextTableYDivCount = tableUiObject:getNextYDivCount() + layoutTableIntertableMarginYDivCount
    end
  end

  return uiObjectArray
end

function threads_drToUi_medalPresentationMedals_buildMessageData(updateObject)
  if type(updateObject) ~= 'table' then
    return nil
  end

  -- Parse each entry and store all 3, 2 and 1 in an array
  local someAdded = false
  local medalEntry2dArray = { [1] = {}, [2] = {}, [3] = {}}
  for _,entry in ipairs(updateObject.entryArray or {}) do
    -- Handle guest diver
    local medalistEntry = {rank=entry.rank, name=entry.name, club=entry.club, country=entry.country}
    if type(medalistEntry.rank) == 'string' and medalistEntry.rank:sub(1,1) == '-' then
      medalistEntry.rank = medalistEntry.rank:sub(2)
      medalistEntry.guest = true
    end
    local medalNumber = tonumber(medalistEntry.rank)
    if type(medalNumber) == 'number' then
      if medalNumber >= 1 and medalNumber <= 3 then
        if type(medalEntry2dArray[medalNumber]) == 'table' then
          table.insert(medalEntry2dArray[medalNumber], medalistEntry)
          log(4, 'Adding an entry to medal array(medal, entry name)', medalNumber, medalistEntry.name)
          someAdded = true
        end
        -- TODO add logs in all these else
      end
    end
  end

  if someAdded == false then
    return nil  -- No need to send anything, there are no medals
  end

  local msgType = 'UI_SET_MEDALS'
  local msgData = {}
  msgData.medalEntry2dArray = medalEntry2dArray

  return msgType, msgData
end

function threads_drToUi_setMedals_buildUiObjects(msgDataSerialized)
  --deserialize the data or return an error
  local medalsObject = threads_message_deserializeTableFromDataString(msgDataSerialized)
  if medalsObject == nil then
    log(2, "No medal object passed with SET_MEDALS message")
    return
  end

  if type(medalsObject.medalEntry2dArray) ~= 'table' then
    log(2, 'medalsObject does not contain the medal 2d table (type)', type(medalsObject.medalEntry2dArray))
    return
  end

-- TODO, extract language constants
    local medalName = {
      [1] = "Médaille d'or",
      [2] = "Médaille d'argent",
      [3] = "Médaille de bronze",
    }

  -- TODO, handle eventAorB
  local medalScreenLayout=ui_screenLayout_getLayout()
  local personsBgColor=ui_colorSwatch_get('medal','bgPerson')
  local medalBgColor=ui_colorSwatch_get('medal','bgMedal')
  local textColor=ui_colorSwatch_get('medal','txt')

  --Build ui object
  local uiObjectArray = {}

  -- Layout params for each medal (xDivPos and yDivPos are set inside the loop)
  local layoutParamTable = {wDivisor=layoutDiv, hDivisor=layoutDiv}
  layoutParamTable.yDivCount = layoutMedalBannerYDivPos -- medal name goes under
  layoutParamTable.wDivCount = layoutMedalBannerWRatio
  layoutParamTable.hDivCount = layoutMedalBannerHRatio
  for medal=1,3 do
    local pGeo
    layoutParamTable.xDivCount = layoutMedalBannerXDivPos[medal]
    layoutParamTable.yDivCount = layoutMedalBannerYDivPos -- medal name goes under
    local medalNameGeo = medalScreenLayout:getScreenFraction(layoutParamTable)

    -- if there's at least one entry for this medal, create the medal name banner and the entry banners
    if type(medalsObject.medalEntry2dArray[medal]) == 'table' and #medalsObject.medalEntry2dArray[medal] > 0 then
      local medalNameBanner = ui_banner_create({
      text=medalName[medal],
      fontName='medium',
      bgColor=medalBgColor,
      textColor=textColor,
      textAlign='center',
      w=medalNameGeo.w, h=medalNameGeo.h, x=medalNameGeo.x, y=medalNameGeo.y})
      -- Add it if created
      if medalNameBanner == nil then
        log(2, 'Unable to create medal name banner (medal)', medal)
      else
        table.insert(uiObjectArray, medalNameBanner)
      end

      for entryIndex,entry in pairs(medalsObject.medalEntry2dArray[medal]) do
        local reversedIndex = #medalsObject.medalEntry2dArray[medal] - entryIndex + 1
        layoutParamTable.yDivCount = layoutMedalBannerYDivPos - layoutParamTable.hDivCount * (reversedIndex)  -- persons stacks above
        local personGeo = medalScreenLayout:getScreenFraction(layoutParamTable)

        local personBannerPlus = ui_bannerPlus_create({
        text=entry.name,
        prefix=entry.club,
        flagKey=entry.country,
        fontName='medium',
        bgColor=personsBgColor,
        textColor=textColor,
        textAlign='center',
        w=personGeo.w, h=personGeo.h, x=personGeo.x, y=personGeo.y})
        table.insert(uiObjectArray, personBannerPlus)
      end
    end
  end

  return uiObjectArray
end
