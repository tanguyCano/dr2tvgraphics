--- Manage thread starting and stopping
-- keeps a reference on thread objects

--- Requires

require 'utils.misc'
require 'files'
require 'threads.utils'
require 'net.tcp'
require 'dr.updateFile'
require 'save.updateReplay'
local config = require 'config'
local SDL = require 'SDL'
local SDLNet = require 'SDL.net'

--- Constants

currentThreadId = 'tcp'
threadName = threads_utils_getThreadName(currentThreadId)
oldPrint = print; print = function(...) oldPrint(threadName,...) end

--- Private variables

local incomingChannel

--- Private functions

local function initThread()
  incomingChannel = SDL.getChannel(threads_utils_getThreadChannelName(currentThreadId))
  if not incomingChannel then FATAL("Unable to open incoming channel") end

  -- Init and start tcp module
  net_tcp_init(SDL,SDLNet,config.Dr.TcpPort)

  -- Init logging if activated
  if config.save.saveUpdatesForReplay == true then
    save_updateReplay_start()
  end
end

local function sendUpdateObjectToScheduler(updateObject)
  -- Check parameters
  if type(updateObject) ~= 'table' then
    log(2, "Invalid first parameter to sendUpdateObjectToScheduler (not a table)")
    return false
  end

  local threadMessageDataString = threads_message_serializeTableToDataString(updateObject)
  if type(threadMessageDataString) ~= 'string' then
    log(2, "Unable to serialize updateObject table")
  end

  -- Build and send the message
  local messageObject = threads_message_build('NEW_UPDATE_OBJECT',threadMessageDataString)
  if messageObject == nil then
    log(3, "sendUpdateObjectToScheduler: Unable to build message")
    return false
  end

  if threads_utils_sendMessage('sch', messageObject) ~= true then
    log(3, "sendUpdateObjectToScheduler: Unable to send message")
    return false
  end

  return true
end

local function tcpThreadLoop()
  while true do
    --wait and parse next channel message if any
    local channelMsgString = incomingChannel:wait()
    if channelMsgString ~= nil then -- Not needed anymore
      incomingChannel:pop()
      threads_message_logMessage(4, "Incoming message", channelMsgString)
      local msgSrc, msgType, msgData = threads_message_parse(channelMsgString)

      --handle message
      if msgType == "TERMINATE" then
        return -- terminate thread
      elseif msgType == "LOAD_TCP_FILE" then
        local fileDetails = threads_message_deserializeTableFromDataString(msgData)
        log(4, "Load tcp file (ip, folder, file)", fileDetails.hostIp, fileDetails.fileFolder, fileDetails.fileName)
        local loadSuccess, fileContent_utf16 = dr_updateFile_loadTcpFile(fileDetails)
        if loadSuccess == true then
          if config.save.saveUpdatesForReplay == true then
            save_updateReplay_save(fileContent_utf16, fileDetails.arrivalTick)
          end
          local fileContent_utf8 = files_utf16ToUtf8(fileContent_utf16)
          if fileContent_utf8 == nil then
            log(2, "Unable to convert update file from utf16 to utf8")
          else
            local updateObject = dr_updateFile_parseFile(fileContent_utf8)
            if updateObject == nil then
              log(2, "Unable to parse update file")
            else
              -- Forward to scheduler task
              updateObject.arrivalTick = fileDetails.arrivalTick
              if sendUpdateObjectToScheduler(updateObject) ~= true then
                log(2,"Unable to send update to Scheduler")
              end

            end
          end
        else
          log(2, "Load fail")
        end
      end
    end

  end
end

local function deinitThread()
end

--- Thread execution

log(4,'Thread startup')
initThread()
tcpThreadLoop()
deinitThread()
log(4,'Thread shutdown')
return 0
