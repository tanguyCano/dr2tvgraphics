--- Helper for parsing DiveRecorder (udp) update messages

--- Requires

require 'utils.misc'

--- Constants

--- Private variables

--- Private functions

local function splitPsvString(psvString)
  if type(psvString) ~= 'string' then return nil end

  return split(psvString,'|')
end

--- Public functions

function dr_updateMsg_parseUpdate(msgString)
  log(4, "Parsing update", msgString)
  local splitedMsg = splitPsvString(msgString)

  -- If the split failed, this isn't an update message
  if type(splitedMsg) ~= 'table' or #splitedMsg < 2 then
    return false
  end

  -- TODO, check for terminal ^

  -- Check the message type
  local msgType = splitedMsg[1]
  if msgType ~= 'UPDATE' then
    log(2, 'Message to parse wasn\'t an update')
    return nil
  end

  -- Build output table
  local updateObject = {}
  local fieldToSplitIndex =
  {
    ['messageType'] = 1,
    ['event'] = 2,
    ['senderHostName'] = 3,
    ['eventMode'] = 4,
    ['senderIp'] = 5,
    ['updateFileFullPath'] = 6,
  }
  for fieldName,splitIndex in pairs(fieldToSplitIndex) do
    if splitIndex > #splitedMsg then
      log(2, 'Update object field '.. (fieldName or 'nil') .. ' can\'t come from split entry index '.. (splitIndex or 'nil') .. ' of '..#splitedMsg)
    end
    updateObject[fieldName] = splitedMsg[splitIndex]
  end

  return updateObject
end

function dr_updateMsg_isUpdate(msgString)
  log(4, "Checking if update", msgString)
  local splitedMsg = splitPsvString(msgString)

  -- If the split failed, this isn't an update message
  if type(splitedMsg) ~= 'table' or #splitedMsg < 2 then
    return false
  end

  -- TODO, check for terminal ^

  -- Check the message type
  local msgType = splitedMsg[1]
  return (msgType == 'UPDATE')

end
