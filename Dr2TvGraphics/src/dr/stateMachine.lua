--- DR event state machine object
-- Keep track of event content and stage
-- Lua Class

--- Requires

require 'utils.table'
require 'threads.drToUi'
require 'command.command'
require 'threads.cmdToUi'
require 'save.diveTimestamp'
local config = require 'config'

--- Constants

--- Private functions

local function matchEntry(entryA, entryB)
  if type(entryA) == 'table' and type(entryB) == 'table' then
    local entryADiverField = entryA.diver
    local entryBDiverField = entryB.diver
    return entryADiverField == entryBDiverField and entryADiverField ~= nil
  end
  return false
end

local function rankFromPoints(sortedEntryArray, rankNumberField, rankStringField)
  if type(sortedEntryArray) ~= 'table' then return end
  if type(rankNumberField) ~= 'string' then return end
  if type(rankStringField) ~= 'string' then return end

  local currentRank = nil
  local currentRankPoints = nil
  local tiedForCurrentRank = nil
  local guestCount = 0
  local tiedGuestForCurrentRank = 0
  for i,entry in ipairs(sortedEntryArray) do -- Move down a sorted array
    local rankNumber, rankStr
    if type(entry.points) ~= 'number' then -- Handle no points field
      rankStr = '-'

    else -- Build rank from current entry points and last iteration
      if currentRankPoints == nil then
        -- Initialize loop variables (start tied with no-one)
        currentRank = 1
        currentRankPoints = entry.points
        tiedForCurrentRank = 0
      end

      -- Check for tie
      if currentRankPoints == entry.points then
        tiedForCurrentRank = tiedForCurrentRank + 1
      elseif currentRankPoints > entry.points then
        currentRankPoints = entry.points
        currentRank = currentRank + tiedForCurrentRank - tiedGuestForCurrentRank
        tiedForCurrentRank = 1
        tiedGuestForCurrentRank = 0

      --TODO catch non sorted (<)
      end

      -- Count guests
      if entry.guest then
        guestCount = guestCount + 1
        tiedGuestForCurrentRank = tiedGuestForCurrentRank + 1
      end

      -- Use all variables to build the string

      if entry.guest then
        rankNumber = currentRank + guestCount - tiedGuestForCurrentRank
        rankStr = '('..rankNumber..')'
      else
        rankNumber = currentRank
        rankStr = ''..rankNumber
      end

    end -- If/else point field is a number
    entry[rankNumberField] = rankNumber
    entry[rankStringField] = rankStr
  end
end

local function namesToTrigram(firstname, surname, firstnameLetterCount)
  if type(firstnameLetterCount) ~= 'number' then
    firstnameLetterCount = 0
  end
  local letterLeft = 3
  local trigram

  -- firstname prefix
  trigram = firstname:upper():gsub(' ', ''):sub(1,firstnameLetterCount)
  letterLeft = letterLeft - firstnameLetterCount

  -- surname
  trigram = trigram .. surname:upper():gsub(' ', ''):sub(1,letterLeft)
  return trigram:sub(1,3) -- ensure at most 3 letters
end

local function rebuildTrigrams(entryArray)
  if type(entryArray) ~= 'table' then return end

  -- assocative array of [trigram] = array of { entryId, diverId, [firstnameLetterCount] }
  local trigramToDivers = {}

  -- Build trigrams with zero first name letters
  for entryId,entry in ipairs(entryArray) do
    for diverId,diver in ipairs(entry.diverArray or {}) do
      if type(diver.surname) ~= 'string' then
        diver.trigram = ''
      else
        -- Take the first three real letters from the surname
        local firstnameLetterCount = 0
        diver.trigram = namesToTrigram(diver.firstname, diver.surname, firstnameLetterCount)
        trigramToDivers[diver.trigram] = trigramToDivers[diver.trigram] or {}
        table.insert(trigramToDivers[diver.trigram], {entryId=entryId,  diverId=diverId, firstnameLetterCount=firstnameLetterCount})
      end
    end
  end

  -- Handle trigram collisions (one level deep)
  -- TODO have a loop for deeper collisions (keep adding firstname letters as needed)
  for trigram,trigramDiverArray in pairs(trigramToDivers) do
    if #trigramDiverArray > 1 then
      -- If more than one diver per trigram, rebuild all these divers with one extra firstname letter
      for _,diverTrigramObject in ipairs(trigramDiverArray) do
        local diver = entryArray[diverTrigramObject.entryId].diverArray[diverTrigramObject.diverId]
        diver.trigram = namesToTrigram(diver.firstname, diver.surname, diverTrigramObject.firstnameLetterCount+1)
        trigramToDivers[diver.trigram] = trigramToDivers[diver.trigram] or {}
        table.insert(trigramToDivers[diver.trigram], {entryId=diverTrigramObject.entryId,  diverId=diverTrigramObject.diverId, firstnameLetterCount=diverTrigramObject.firstnameLetterCount+1})
      end
    end
  end
end

local function scrollActionToFirstHalfRowToDisplay(scrollAction, previousFirstHalfRowToDisplay)
  local newFirstHalfRowToDisplay = previousFirstHalfRowToDisplay or 0

  local scrollActionEnum = {} -- Helps with readability
  scrollActionEnum.HOME       = '100'
  scrollActionEnum.PAGE_UP    = '101'
  scrollActionEnum.PAGE_DOWN  = '102'
  scrollActionEnum.END        = '103'

  if scrollAction == scrollActionEnum.HOME then
    newFirstHalfRowToDisplay = 0
  elseif scrollAction == scrollActionEnum.END then
    newFirstHalfRowToDisplay = 99999 -- Big enough that we'll always 'rewind' to the bottom of what's to display
  elseif scrollAction == scrollActionEnum.PAGE_UP then
    newFirstHalfRowToDisplay = math.max(0,newFirstHalfRowToDisplay - config.Ui.tableScrollRowCount)
  elseif scrollAction == scrollActionEnum.PAGE_DOWN then
    newFirstHalfRowToDisplay = math.max(0,newFirstHalfRowToDisplay + config.Ui.tableScrollRowCount)
  else
    log(2, 'Invalid scrollAction, scroll position left unchanged (scrollAction)', scrollAction)
  end
  log(4, 'scroll position recalculated (scrollAction, previous position, new position)',
    scrollAction, previousFirstHalfRowToDisplay, newFirstHalfRowToDisplay)

  return newFirstHalfRowToDisplay
end

--- Class constructor

--- @param #table paramTable {event}
function dr_stateMachine_create(paramTable)

--- private members

--- private methods

local function buildAwardUsedArray(entry)
  if type(entry) ~= 'table' then
    return false
  end

  -- Skip when nothing to do
  if entry.awarded == false then
    return true -- Not an error, just no award to work on
  end

  local whatToDrop = {} -- array of {firstIndex, lastIndex, minAndMaxDropCount}
  -- Check we have what we need to work and set whatToDrop (FINA D7.5, D7.6 and D7.7)
  if entry.synchro == true then
    if type(entry.awardArray) ~= 'table' or type(entry.executionAwardCount) ~= 'number' or type(entry.synchroAwardCount) ~= 'number' then
      log(2, 'buildAwardUsedArray: Invalid field types (awardArray, executionAwardCount, synchroAwardCount)', type(entry.awardArray), type(entry.executionAwardCount), type(entry.synchroAwardCount))
      return false
    else
      if entry.executionAwardCount == 4 and entry.synchroAwardCount == 5 then
        whatToDrop = {{firstIndex = 1, lastIndex = 4, minAndMaxToDropCount = 1},  -- FINA D7.7
                      {firstIndex = 5, lastIndex = 9, minAndMaxToDropCount = 1}}
      elseif entry.executionAwardCount == 6 and entry.synchroAwardCount == 5 then -- FINA D7.6
        whatToDrop = {{firstIndex = 1, lastIndex = 3, minAndMaxToDropCount = 1},
                      {firstIndex = 4, lastIndex = 6, minAndMaxToDropCount = 1},
                      {firstIndex = 7, lastIndex =11, minAndMaxToDropCount = 1}}
      else
        log(2, 'buildAwardUsedArray: Unhandled synchro judge count (exec, sync)', entry.executionAwardCount, entry.synchroAwardCount)
        return false
      end
    end
  else
    if type(entry.awardArray) ~= 'table' then
      log(2, 'buildAwardUsedArray: Invalid awardArray type', type(entry.awardArray))
      return false
    else
      local awardCount = #entry.awardArray
      if awardCount == 5 then
        whatToDrop = {{firstIndex = 1, lastIndex = 5, minAndMaxToDropCount = 1}} -- FINA D7.5
      elseif awardCount == 7 then
        whatToDrop = {{firstIndex = 1, lastIndex = 7, minAndMaxToDropCount = 2}} -- FINA D7.5
      else
        log(2, 'buildAwardUsedArray: Unhandled solo judge count ', awardCount)
        return false
      end
    end
  end

  -- Initialise all awards as used
  entry.awardUsedArray = {}
  for i=1,#entry.awardArray do
    entry.awardUsedArray[i] = true
  end

  -- Drop awards based on rules
  for _,dropRule in pairs(whatToDrop) do
    if dropRule.firstIndex > 0 and dropRule.lastIndex <= #entry.awardArray then
      -- Build an array with the award in the rule, sort, and mark the n top and bottom ones as dropped
      local awardSet = {}
      for i=dropRule.firstIndex,dropRule.lastIndex do
        table.insert(awardSet, {awardId=i, awardValue=entry.awardArray[i]})
      end
      table.sort(awardSet, table_sorterBuilder_numberField('awardValue'))
      for extremumNth=1,dropRule.minAndMaxToDropCount do
        local minNthAwardIndex = awardSet[extremumNth].awardId
        local maxNthAwardIndex = awardSet[#awardSet - (extremumNth - 1)].awardId
        entry.awardUsedArray[minNthAwardIndex] = false
        entry.awardUsedArray[maxNthAwardIndex] = false
      end
    end
  end

  return true
end

local function addOrRefreshDiveEvent(self, updateObject)
  if type(self) ~= 'table' or type(updateObject) ~= 'table' then
    log(2, 'Invalid self or updateObject passed to addOrRefreshDiveEvent')
    return
  end

  if type(self.ongoingEventsArray) ~= 'table' then
    log(2, 'Missing field ongoingEvents in self passed to addOrRefreshDiveEvent')
    return
  end

  if type(updateObject.eventName) ~= 'string' then
    log(2, 'Missing field eventName in udpateObject passed to addOrRefreshDiveEvent')
    return
  end

  self.meetName = updateObject.meetName
  local eventName = updateObject.eventName
  -- Retrieve or create the current event entry in the ongoingEventsArray
  local currentEventObject = nil
  local _,entryIndexInOngoingEventsArray = table_find(eventName, self.ongoingEventsArray, table_finderBuilder_keyValue('eventName'))
  if entryIndexInOngoingEventsArray == nil then
    log(3, 'New event added (AorB, eventName)', self.eventAorB, eventName)
    currentEventObject = {eventName=eventName, entryArray = {}}
    table.insert(self.ongoingEventsArray, currentEventObject)
  else
    currentEventObject = self.ongoingEventsArray[entryIndexInOngoingEventsArray]
  end
  -- Protect against invalid creation/retrieval
  if currentEventObject == nil then
    log(2, 'Invalid currentEventObject create/retrieved in addOrRefreshDiveEvent')
    return
  end

  -- Update event latest informations
  -- TODO track changes (backup previous content of self.ongoingEvents[x] for this eventName)
  local fieldsToCopy = {'round', 'diver', 'roundCount', 'diverCount', 'awarded', 'synchro'}
  table_copyFields(updateObject,currentEventObject,fieldsToCopy)
  self.activeEvent = eventName

  -- Update overall info
  table_copyFields(updateObject, self.overallInfo, fieldsToCopy)

  -- Add diver with latest informations to array of events
  -- TODO, protect against nil currentEventObject.entryArray
  local _,entryIndexInCurrentEventEntryArray = table_find(updateObject, currentEventObject.entryArray, matchEntry)
  if entryIndexInCurrentEventEntryArray == nil then
    log(3, 'New diver added (AorB, eventName)', self.eventAorB, eventName) -- TODO, log diver name?
    table.insert(currentEventObject.entryArray, {})
    entryIndexInCurrentEventEntryArray = #currentEventObject.entryArray
  end
  local fieldsToCopy = {'round', 'diver', 'roundCount', 'diverCount', 'awarded', 'awardArray', 'diverArray',
    'diveNumber', 'divePosition', 'diveName', 'height', 'dd', 'diveTotal', 'points', 'rank', 'penalty',
    'synchro', 'executionAwardCount', 'synchroAwardCount', 'guest'}
  table_copyFields(updateObject,currentEventObject.entryArray[entryIndexInCurrentEventEntryArray],fieldsToCopy)
  -- Create the list of the awards that are dropped (FINA D7.5, D7.6 and D7.7)
  if buildAwardUsedArray(currentEventObject.entryArray[entryIndexInCurrentEventEntryArray]) ~= true then
    log(2, 'Unable to build awardUsedArray')
  end

  -- Sort entries by points and recompute rank for this event
  table.sort(currentEventObject.entryArray, table_sorterBuilder_numberField('points')) -- TODO check return?
  rankFromPoints(currentEventObject.entryArray, 'eventAwareRankNumber', 'eventAwareRankStr')
  -- Rebuild trigram for this event
  rebuildTrigrams(currentEventObject.entryArray)

  -- Check if all entries are at the same round or if some have more completed dives than others
  local allOnSameAwardedRound = true
  local referenceLastAwardedRound = nil
  for _,entry in ipairs(currentEventObject.entryArray) do
    entry.lastAwardedRound = entry.round
    if entry.lastAwardedRound ~= nil then
      if entry.awarded == false then
        entry.lastAwardedRound = entry.lastAwardedRound - 1
      end
      referenceLastAwardedRound = referenceLastAwardedRound or entry.lastAwardedRound -- Make sure there's a value to compare 2,n to
      if referenceLastAwardedRound ~= entry.lastAwardedRound then
        allOnSameAwardedRound = false -- At least this entry isn't on the same round as the first one
      end
      currentEventObject.eventAwareLastAwardedRound = math.max(currentEventObject.eventAwareLastAwardedRound or 0, entry.lastAwardedRound)
    end
  end
  if allOnSameAwardedRound then
    -- Effectively the end of a round
    currentEventObject.eventAwareCompletedRound = referenceLastAwardedRound
    log(4, 'Round has been completed and the next one is yet to be awarded (event, completed round)', eventName, currentEventObject.eventAwareCompletedRound)
  else
    -- Middle of current round
    currentEventObject.eventAwareCompletedRound = nil
    log(4, 'Event is in the middle of round (event, round)', eventName, currentEventObject.round)
  end

  -- Fix index references
  _,entryIndexInCurrentEventEntryArray = table_find(updateObject, currentEventObject.entryArray, matchEntry)
  if entryIndexInCurrentEventEntryArray == nil then
    -- How come the entry is missing now, we just sorted the array?!
    log(2, "Unable to find entry post sorting, this shouldn't happen")
  end
  self.activeEntryIndex = entryIndexInCurrentEventEntryArray

  -- Log info for event
  local awardedStr = ''
  if currentEventObject.awarded == true then awardedStr = 'Yes' else awardedStr = 'No ' end
  log(4, 'event ' .. (self.eventAorB or 'nil') ..
    ' is on diver ' .. (currentEventObject.diver or 'nil') .. '/' ..
    (currentEventObject.diverCount or 'nil') ..
    ' round ' .. (currentEventObject.round or 'nil') .. '/' ..
    (currentEventObject.roundCount or 'nil') ..
    ' awarded: ' .. (awardedStr or 'no ') ..
    ' name: ' .. (eventName or 'nil')
    )
end

local function saveDiveStamp(updateObject)

  local diveCode = (updateObject.diveNumber or '') .. (updateObject.divePosition or '')
  local timestamp = updateObject.arrivalTick
  local diverAName, diverBName
  -- Build dive names
  for i,entry in ipairs(updateObject.diverArray or {}) do
    if i == 1 then
      diverAName = entry.fullName
    elseif i == 2 then
      diverBName = entry.fullName
    end
  end

  save_diveTimestamp_save(updateObject.eventName,
    updateObject.diver, updateObject.round, diveCode,
    updateObject.arrivalTick, diverAName, diverBName)

end

--- class instance

local self = {}

--- public members

self.eventAorB = paramTable.eventAorB or 'a'
self.meetName = nil
self.state = nil -- NO_EVENT, IN_EVENT, IN_EVENT_RANKING
self.activeEvent = nil
self.activeEntryIndex = nil
self.ongoingEventsArray = nil -- array of = {eventName, round, diver, roundCount, diverCount, awarded, entryArray}, as of the last dive of this event
self.overallInfo = nil -- table {round, diver, roundCount, diverCount, awarded}
self.tableScrollFirstHalfRowToDisplay = nil -- TODO, rename to scrollPosition ?
self.displayRankingAsMedalCeremony = nil
self.showTime = false

--- public methods

function self:reset()
  -- Clear all fields tracking the state of the event
  self.meetName = nil
  self.state = 'NO_EVENT'
  self.activeEvent = nil
  self.activeEntryIndex = nil
  self.ongoingEventsArray = {}
  self.overallInfo = {}
  self.tableScrollFirstHalfRowToDisplay = nil
  self.displayRankingAsMedalCeremony = false
  self.showTime = false
end

--- @param #table updateObject (see dr.updateFile)
function self:processUpdate(updateObject)
  local outcomeFunctionArray = {} -- Array of {func, data}
  if type(updateObject) ~= 'table' or type(updateObject.typeStr) ~= 'string' then
    log(2, 'Invalid updateObject reached sateMachine:processUpdate (type(object), typeStr)',
      type(updateObject), type(updateObject.typeStr))
    return
  end

  -- Filter out updates not applying to this event state machine
  if type(updateObject.eventAorB) ~= 'string' then
    log(2, 'Invalid updateObject eventAorB field (type)', type(updateObject.eventAorB))
    return
  end
  if updateObject.eventAorB ~= self.eventAorB then
    log(4, 'Update message ignored because it applies to other event (stateMachine, updateObject)',
      updateObject.eventAorB, updateObject.eventAorB)
    return
  end

  if updateObject.typeStr == 'UPDATE_TYPE_DIVE' then
    if self.state == 'IN_EVENT_RANKING' then
      log(4, 'Post-ranking event resume detected (AorB)', self.eventAorB)
      self.state = 'IN_EVENT'
      self.tableScrollFirstHalfRowToDisplay = nil
      table.insert(outcomeFunctionArray, {func=threads_drToUi_clearAll_buildMessageData, data=self})
    elseif self.state ~= 'IN_EVENT' then
      log(3, 'Start of event detected (AorB)', self.eventAorB)
      self.state = 'IN_EVENT'
      table.insert(outcomeFunctionArray, {func=threads_drToUi_clearAll_buildMessageData, data=self})
    end
    addOrRefreshDiveEvent(self, updateObject)
    table.insert(outcomeFunctionArray, {func=threads_drToUi_inEventInfo_buildMessageData, data=self})
    table.insert(outcomeFunctionArray, {func=threads_drToUi_inEventDive_buildMessageData, data=self})
    -- Log divestamp
    if config.save.saveDivesTimestamp == true and updateObject.awarded ~= true then
      saveDiveStamp(updateObject) -- Record the start of this dive
    end

  elseif updateObject.typeStr == 'UPDATE_TYPE_RANKING' then

    -- Update the table scroll position
    self.tableScrollFirstHalfRowToDisplay = scrollActionToFirstHalfRowToDisplay(updateObject.scrollAction, self.tableScrollFirstHalfRowToDisplay)
    if self.state == 'IN_EVENT' then
      -- TODO, check our internal model is consistent with the ranking provided by the message
      -- (in fact, our internal should be better since it track multi event running together)
      table.insert(outcomeFunctionArray, {func=threads_drToUi_clearAll_buildMessageData, data=self})
      table.insert(outcomeFunctionArray, {func=threads_drToUi_inEventRanking_buildMessageData, data=self})
      -- Store the fact that we are displaying ranking
      self.state = 'IN_EVENT_RANKING'
    elseif self.state == 'NO_EVENT' then
      if self.displayRankingAsMedalCeremony == true then
        -- Use dedicated medal ceremony message builder (event banners and medal/person banners)
        table.insert(outcomeFunctionArray, {func=threads_drToUi_clearAll_buildMessageData, data=self})
        table.insert(outcomeFunctionArray, {func=threads_drToUi_medalPresentationEvent_buildMessageData, data=updateObject})
        table.insert(outcomeFunctionArray, {func=threads_drToUi_medalPresentationMedals_buildMessageData, data=updateObject})
      else
        -- For now all ranking messages are handled as a first ranking
        -- Limitation: We can't handle next page events with animation since we don't know when we start showing a different non in-event ranking
        updateObject.tableScrollFirstHalfRowToDisplay = self.tableScrollFirstHalfRowToDisplay -- duplicate the field since we're not passing the stateMachine
        table.insert(outcomeFunctionArray, {func=threads_drToUi_clearAll_buildMessageData, data=self})
        table.insert(outcomeFunctionArray, {func=threads_drToUi_noEventRanking_buildMessageData, data=updateObject})
      end
    elseif self.state == 'IN_EVENT_RANKING' then
      self.firstHalfRowToDisplay = firstHalfRowToDisplay
      table.insert(outcomeFunctionArray, {func=threads_drToUi_clearAll_buildMessageData, data=self})
      table.insert(outcomeFunctionArray, {func=threads_drToUi_inEventRanking_buildMessageData, data=self})
    else
      log(2, 'WARNING: ranking updates are not supported in state ', self.state)
    end

  elseif updateObject.typeStr == 'UPDATE_TYPE_BLANK' then
    -- Clear everything (internaly and UI)
    self:reset()
    table.insert(outcomeFunctionArray, {func=threads_drToUi_clearAll_buildMessageData, data=self})

  elseif updateObject.typeStr == 'UPDATE_TYPE_STARTLIST' then
    -- Update the table scroll position
    self.tableScrollFirstHalfRowToDisplay = scrollActionToFirstHalfRowToDisplay(updateObject.scrollAction, self.tableScrollFirstHalfRowToDisplay)
    updateObject.tableScrollFirstHalfRowToDisplay = self.tableScrollFirstHalfRowToDisplay -- duplicate the field since we're not passing the stateMachine
    if self.state ~= 'NO_EVENT' then
      -- TODO, we don't really expect this except outside of events, though we could?
      log(2, 'Warning, we are possibly going to lose the event context')
    end
    -- The start list isn't related to a running event, call the builder on the update message object
    table.insert(outcomeFunctionArray, {func=threads_drToUi_clearAll_buildMessageData, data=self})
    table.insert(outcomeFunctionArray, {func=threads_drToUi_startList_buildMessageData, data=updateObject})

  elseif updateObject.typeStr == 'UPDATE_TYPE_JUDGELIST' then
    -- Update the table scroll position
    self.tableScrollFirstHalfRowToDisplay = scrollActionToFirstHalfRowToDisplay(updateObject.scrollAction, self.tableScrollFirstHalfRowToDisplay)
    updateObject.tableScrollFirstHalfRowToDisplay = self.tableScrollFirstHalfRowToDisplay -- duplicate the field since we're not passing the stateMachine
    if self.state ~= 'NO_EVENT' then
      -- TODO, we don't really expect this except outside of events
      log(2, 'Warning, we are possibly going to lose the event context')
    end
    -- The judge list isn't related to a running event, call the builder on the update message object
    table.insert(outcomeFunctionArray, {func=threads_drToUi_clearAll_buildMessageData, data=self})
    table.insert(outcomeFunctionArray, {func=threads_drToUi_judgeList_buildMessageData, data=updateObject})

  end -- update type if/else
  return outcomeFunctionArray
end

--- @param #table commandObject (see command.lua)
function self:processCommand(commandObject)
  local outcomeFunctionArray = {} -- Array of {func, data}
  if type(commandObject) ~= 'table' or type(commandObject.typeStr) ~= 'string' then
    log(2, 'Invalid commandObject reached sateMachine:processcommand (type(object), typeStr)',
      type(commandObject), type(commandObject.typeStr))
    return
  end

  -- Filter out command not applying to this event state machine
  -- TODO Not obvious, for instance SHOW_NEXT_EVENT applies to both

  if commandObject.typeStr == commandType.CLEAR then
    -- Clear everything (internaly and UI)
    self:reset()
    table.insert(outcomeFunctionArray, {func=threads_cmdToUi_clearAll_buildMessageData, data=self})

  elseif commandObject.typeStr == commandType.SET_MEDAL_RANKING then
    self.displayRankingAsMedalCeremony = (commandObject.newValue == true)

  elseif commandObject.typeStr == commandType.SHOW_TIME then
    -- Copy the eventAorB of this stateMachine (to only display the proper events, on the proper layout)
    commandObject.eventAorB = self.eventAorB

    -- Set the new value
    local previousValue = (self.showTime == true)
    self.showTime = (commandObject.newValue == true)

    -- Exit if nothing changed
    local valueChanged = previousValue ~= self.showTime
    if valueChanged ~= true then
      return
    end

    if self.showTime == false then
      -- Hide time
      table.insert(outcomeFunctionArray, {func=threads_cmdToUi_hideTime_buildMessageData, data=commandObject})
    else
      -- Simulate tick to not duplicate show time code
      return self:processTick()
    end

  elseif commandObject.typeStr == commandType.SHOW_NEXT_EVENT then
    -- Copy the eventAorB of this stateMachine (to only display the proper events, on the proper layout)
    commandObject.eventAorB = self.eventAorB
    -- Ignore if we're in-event
    if self.state == 'NO_EVENT' then
      table.insert(outcomeFunctionArray, {func=threads_drToUi_clearAll_buildMessageData, data=self})
      table.insert(outcomeFunctionArray, {func=threads_cmdToUi_showNextEvent_buildMessageData, data=commandObject})
    end

  end -- command type if/else
  return outcomeFunctionArray
end

--- @param #table tickObject (not used for now)
function self:processTick(tickObject)
  local outcomeFunctionArray = {} -- Array of {func, data}

  -- Current implementation of tick is only used for showTime string change (next minute)
  if self.showTime == true then
    -- Get current time (hh, mm and ss)
    local timeObject = os.date("*t")
    if type(timeObject) ~= 'table' or type(timeObject.sec) ~= 'number' then
      log(2, 'Unable to retrieve current time (type(object), sec)', type(timeObject), timeObject.sec)
      return
    end

    -- start scheduler timer mechanism
    scheduleTick(60-timeObject.sec)

    -- Display new time
    local dataObject = {eventAorB=self.eventAorB, timeObject=timeObject}
    table.insert(outcomeFunctionArray, {func=threads_cmdToUi_showTime_buildMessageData, data=dataObject})
  end

  return outcomeFunctionArray
end

self:reset()
return self
end
