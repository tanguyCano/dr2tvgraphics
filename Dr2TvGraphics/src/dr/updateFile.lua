--- Helper for parsing DiveRecorder (tcp) update file

--- Requires

require 'net.tcp'
local config = require 'config'

--- Constants

local lenHeaderByteCount = 4

--- Private variables

--- Private functions

local function toboolean(booleanStr)
  if type(booleanStr) ~= 'string' or booleanStr == '' or booleanStr == ' ' then
    return nil
  end

  -- Compare lowercase to true and false strings
  booleanStr = booleanStr:lower()
  if booleanStr == 'true' then
    return true
  elseif booleanStr == 'false' then
    return false
  end
  return nil -- No match
end

local function awardStrToNumber(awardStr)
  if type(awardStr) ~= 'string' or awardStr == '' or awardStr == ' ' then
    return nil
  end

  -- Replace half point char to have a number parsable string
  local halfStr = '½'
  local sanitizedAwardStr = awardStr:gsub(halfStr, '.5')

  -- Parse the number
  local awardNumber = tonumber(sanitizedAwardStr)
  if awardNumber == nil then
    log(2, 'Unable to parse award \'' .. (awardStr or 'nil') .. '\'')
  end

  return awardNumber
end

local function splitFullNameAndTeamCode(fullNameAndTeamCodeStr)
  if type(fullNameAndTeamCodeStr) ~= 'string' or fullNameAndTeamCodeStr == '' or fullNameAndTeamCodeStr == ' ' then
    return nil, nil
  end

  -- Cut string at the separator between fullName and Team Code
  local separator = ' %-%- ' -- Escaped ' -- '
  local splitted = split(fullNameAndTeamCodeStr, separator, false)
  if splitted == nil or #splitted ~= 2 then
    --TODO, this separator won't work if no club is set, handle it here (check end of fullNameAndTeamCodeStr is ' --', trim it)
    log(2, "splitFullNameAndTeamCode did not split into two parts '".. (fullNameAndTeamCodeStr or 'nil') .. "'")
    return nil, nil
  end

  -- Sanitize a possibly missing club
  local fullName, club = splitted[1], splitted[2]
  if club == '' or club == ' ' then
    club = nil
  end

  return fullName, club
end

local function splitJudgeNameAndCode(fullNameAndCodeStr)
  if type(fullNameAndCodeStr) ~= 'string' or fullNameAndCodeStr == '' or fullNameAndCodeStr == ' ' then
    return fullNameAndCodeStr, nil
  end

  -- Cut string at the separator between judge name and code
  local separator = ' %[' -- Escaped ' ['
  local splitted = split(fullNameAndCodeStr, separator, false)
  if splitted == nil or #splitted < 1 or #splitted > 2 then
    log(2, "splitJudgeNameAndCode did not split into one or two parts '".. (fullNameAndCodeStr or 'nil') .. "'")
    return fullNameAndCodeStr, nil
  end

  local judgeName = splitted[1]
  local judgeCode
  -- properly extract the code from the '([)XXX]' string
  if type(splitted[2]) == 'string' then
    for code in string.gmatch('[' .. splitted[2], '%[(.-)%]') do
      judgeCode = code
      break
    end
  end
  -- Sanitize a possibly missing club
  if judgeCode == '' or judgeCode == ' ' then
    judgeCode = nil
  end

  return judgeName, judgeCode
end

local function parseUpdateAwards(updateArray, parsedMessage, firstAwardUpdateArrayFieldIndex,  judgeCountUpdateArrayFieldIndex)
  -- Check params
  if type(updateArray) ~= 'table' or type(parsedMessage) ~= 'table' then
    log(2, 'parseUpdateAwards: Invalid updateArray or parsedMessage types (type, type)', type(updateArray), type(parsedMessage))
    return
  end
  if type(firstAwardUpdateArrayFieldIndex) ~= 'number' then
    log(2, 'parseUpdateAwards: Invalid firstAwardUpdateArrayFieldIndex type (type)', type(firstAwardUpdateArrayFieldIndex))
    return
  end

  -- awards to a dedicated table
  local judgeCount = tonumber(updateArray[judgeCountUpdateArrayFieldIndex] or '')
  if judgeCount == nil then
    log(2, 'Unable to retrieve or parse to number object field \'number of judge\'')
    return
  end
  local awardParsingState = 0 -- 0=none yet, 1=execution, 2=gapBetween, 3=Syncho, 4=gapAfter, 5=invalid
  for i=firstAwardUpdateArrayFieldIndex,firstAwardUpdateArrayFieldIndex+10 do
    local awardIndex = i - firstAwardUpdateArrayFieldIndex + 1
    local currentAward = awardStrToNumber(updateArray[i])

    -- Update the parsing state based of whether there's an award at this index
    if type(currentAward) == 'number' then
      if awardParsingState == 0 or awardParsingState == 2 or awardParsingState == 4 then
        awardParsingState = awardParsingState + 1
      end
    elseif currentAward == nil then
      if awardParsingState == 1 or awardParsingState == 3 then
        awardParsingState = awardParsingState + 1
      end
    end
    -- Change state if synchro, still in exec and 6th awards (there will be no gap between)
    if parsedMessage.synchro == true and awardParsingState == 1 and awardIndex == 7 then
      awardParsingState = 3
    end
    -- Handle invalid states (throw some warning, but continue)
    -- be unhappy if solo and in state 3
    if awardParsingState >= 3 and parsedMessage.synchro == false then
      log(2, 'parseUpdateAwards: solo event but more awards after the gap (state, index)', awardParsingState, awardIndex)
    end
    -- If state 5 (invalid), revert to 4 and print an error message
    if awardParsingState >= 5 then
      log(2, 'parseUpdateAwards: Invalid state reached when parsing awards (state, index)', awardParsingState, awardIndex)
      awardParsingState = 4 -- Prevent moving past state 5
    end


    -- Store the award and its information
    if awardParsingState == 1 or awardParsingState == 3 then
      -- Add the award in a contiguous array
      parsedMessage.awardArray = parsedMessage.awardArray or {}
      table.insert(parsedMessage.awardArray, currentAward)
      -- Store metadata on awards
      if awardParsingState == 1 then
        parsedMessage.executionAwardCount = (parsedMessage.executionAwardCount or 0) + 1
      elseif awardParsingState == 3 then
        parsedMessage.synchroAwardCount = (parsedMessage.synchroAwardCount or 0) + 1
      end
    end
  end

  -- Check judgeCount matches the parsed number of awards (if any)
  if type(parsedMessage.awardArray) == 'table' then
    if #parsedMessage.awardArray ~= judgeCount then
      log(2,"parseUpdateAwards: Number of judge isn't consistent with number of awards (awards, judges)",
        #parsedMessage.awardArray, judgeCount)
      end
    end
end

--- Public functions

--TODO, pure tcp file functions should eventually move to a dedicated module
function dr_updateFile_buildFileDetails(hostIp, fileName, arrivalTick)
  -- TODO check parameters

  local fileDetails = {}
  fileDetails.hostIp = hostIp
  fileDetails.fileName = fileName
  fileDetails.arrivalTick = arrivalTick
  return fileDetails
end

---@function [parent=#dr.updateFile] dr_updateFile_loadTcpFile
--@param #table fileDetails Describe the file to load (hostIp and FileName strings)
--@return #bool Whether the file was loaded
--@return #string The file content
function dr_updateFile_loadTcpFile(fileDetails)
  -- TODO check parameters

  -- Open socket to host
  if net_tcp_openSocket(fileDetails.hostIp, config.Dr.TcpPort) ~= true then
    log(2, 'Unable to open tcp socket to load file on host, port', fileDetails.hostIp, config.Dr.TcpPort)
    return false
  end

  -- Send file request
  local tcpCommand = 'XFER|' .. fileDetails.fileName .. '\n'
  if net_tcp_send(tcpCommand) ~= true then
    log(2, 'Unable to send tcp command', tcpCommand)
    net_tcp_closeSocket()
    return false
  end

  -- Load file length
  local receiveSuccess, fileLengthString = net_tcp_receive(lenHeaderByteCount)
  if receiveSuccess ~= true then
    log(2, 'Unable to receive file length')
    net_tcp_closeSocket()
    return false
  end
  local fileContentLength = stringbytes_to_int(fileLengthString, 0, lenHeaderByteCount, true) -- true = MSBFirst
  if fileContentLength == nil or fileContentLength <= 0 then
    log(2, 'Unable to parse file content length', fileContentLength)
    net_tcp_closeSocket()
    return false
  end

  -- Load file content
  local receiveSuccess, fileContent = net_tcp_receive(fileContentLength)
  if receiveSuccess ~= true then
    log(2, 'Unable to receive file content')
    net_tcp_closeSocket()
    return false
  end

  -- Close socket and return content
  net_tcp_closeSocket()
  return true, fileContent
end


function dr_updateFile_parseDiveUpdate(updateArray)
  local parsedMessage = {}
  local fieldToSplitIndex =
  {
    ['updateType'] = 31,
    ['eventAorB'] = 2,
    ['round'] = 6,
    ['diver'] = 8,
    ['diveNumber'] = 13,
    ['divePosition'] = 14,
    ['diveName'] = 61,
    ['height'] = 16,
    ['dd'] = 15,
    ['diveTotal'] = 29,
    ['points'] = 30,
    ['rank'] = 32,
    ['penalty'] = 51,
    ['synchro'] = 47,
    ['eventName'] = 60,
    ['meetName'] = 62,
    ['roundCount'] = 63,
    ['diverCount'] = 64,
  }
  for fieldName,splitIndex in pairs(fieldToSplitIndex) do
    if splitIndex > #updateArray then
      log(2, 'Update object field '.. (fieldName or 'nil') .. ' can\'t come from split entry index '.. (splitIndex or 'nil') .. ' of '..#updateArray)
    end
    parsedMessage[fieldName] = updateArray[splitIndex]
  end

  -- Trim (possible) prefix from rank field before parsing it to a number
  if type(parsedMessage.rank) == 'string' then
    if parsedMessage.rank:sub(1,1) == 'P' then
      parsedMessage.rank = parsedMessage.rank:sub(2)
    end
  end

  -- Transform some of these strings to numbers
  local fieldToNumberize = {'round', 'diver', 'dd', 'diveTotal', 'points', 'rank', 'penalty', 'roundCount', 'diverCount'}
  for i,fieldName in pairs(fieldToNumberize) do
    if parsedMessage[fieldName] ~= nil then
      local numberizedFieldValue = tonumber(parsedMessage[fieldName])
      if numberizedFieldValue == nil then
        log(2, 'Parsed field '.. (fieldName or 'nil') .. ' (' .. (parsedMessage[fieldName] or 'nil') .. ') can\'t be converted to number')
        -- Field will be set to nil
      end
      parsedMessage[fieldName] = numberizedFieldValue
    end
  end

  -- Transform some of these strings to boolean
  local fieldToBooleanize = {'synchro'}
  for i,fieldName in pairs(fieldToBooleanize) do
    if parsedMessage[fieldName] ~= nil then
      local booleanizedFieldValue = toboolean(parsedMessage[fieldName])
      if booleanizedFieldValue == nil then
        log(2, 'Parsed field '.. (fieldName or 'nil') .. ' (' .. (parsedMessage[fieldName] or 'nil') .. ') can\'t be converted to boolean')
      else
        parsedMessage[fieldName] = booleanizedFieldValue
      end
    end
  end

  -- awards to a dedicated table
  -- adds fields: awardArray, executionAwardCount, synchroAwardCount
  parseUpdateAwards(updateArray,parsedMessage, 17, 50)

  -- set an awarded boolean flag
  if parsedMessage.awardArray == nil then
    parsedMessage.awarded = false
  else
    parsedMessage.awarded = true
  end

  -- parse name and club (trying to keep the DR name format) or all divers
  parsedMessage.diverArray = {}
  local guestDetected = false
  local diverFieldIndexes = {
  [1] = {fullName=9, club=56, country=68, surname=10, firstname=54},
  [2] = {fullName=11, club=59, country=70, surname=12, firstname=57},
  }
  for diverIndex,fieldIndex in ipairs(diverFieldIndexes) do
    local diverObject = {}
    diverObject.fullName, _= splitFullNameAndTeamCode(updateArray[fieldIndex.fullName])
    diverObject.club = updateArray[fieldIndex.club]
    diverObject.country = updateArray[fieldIndex.country]
    diverObject.surname = updateArray[fieldIndex.surname]
    diverObject.firstname = updateArray[fieldIndex.firstname]
    if diverObject.country ==  '' then
      diverObject.country = config.Dr.defaultDiverCountry or ''
    end
    -- Primitive guest detector
    if config.Dr.flagNonFraNorMonAsGuest == true then
      if diverObject.country ~= 'FRA' and diverObject.country ~= 'MON' then
        guestDetected = true
      end
    end

    if diverObject.fullName ~= nil then -- Only add the diver if it exists
      table.insert(parsedMessage.diverArray, diverObject)
    end
  end
  parsedMessage.guest = guestDetected

  return parsedMessage
end

function dr_updateFile_parseRankingUpdate(updateArray)
  local parsedMessage = {}
  local fieldToSplitIndex = -- Not much to parse, it's mostly dynamic fields
  {
    ['updateType'] = 31,
    ['eventAorB'] = 2,
    ['eventName'] = 60,
    ['meetName'] = 62,
  }
  for fieldName,splitIndex in pairs(fieldToSplitIndex) do
    if splitIndex > #updateArray then
      log(2, 'Update object field '.. (fieldName or 'nil') .. ' can\'t come from split entry index '.. (splitIndex or 'nil') .. ' of '..#updateArray)
    end
    parsedMessage[fieldName] = updateArray[splitIndex]
  end

  -- Parse each of the entries
  -- TODO factorize code with startlist
  -- TODO, very hard to handle synchro (unavailable for now)
  local firstEntryInfoFieldIndex = 74
  local entryInfoFieldCount = 6
  local entryInfoFieldToSplitIndexOffset =
  {
    ['rank'] = 0,
    ['points'] = 1,
    ['diverRound'] = 2,
    ['name'] = 3,
    ['club'] = 5,
  }
  local infoFieldToNumberize = {'points', 'diverRound'}
  parsedMessage.entryArray = {}
  local currentEntryInfoFieldIndex = firstEntryInfoFieldIndex
  while currentEntryInfoFieldIndex + entryInfoFieldCount <= #updateArray do
    local entryObject = {}
    for entryFieldName,entrySplitIndexOffset in pairs(entryInfoFieldToSplitIndexOffset) do
      local splitIndex = entrySplitIndexOffset + currentEntryInfoFieldIndex
      if entrySplitIndexOffset > #updateArray then
        log(2, 'Update object field '.. (entryFieldName or 'nil') .. ' can\'t come from split entry index '.. (splitIndex or 'nil') .. ' of '..#updateArray)
      end
      entryObject[entryFieldName] = updateArray[splitIndex]
      -- Numberize, if required
      if entryObject[entryFieldName] ~= nil and infoFieldToNumberize[entryFieldName] ~= nil then
        local numberizedEntryFieldValue = tonumber(entryObject[entryFieldName])
        if numberizedEntryFieldValue == nil then
          log(2, 'Parsed field '.. (entryFieldName or 'nil') .. ' (' .. (entryObject[entryFieldName] or 'nil') .. ') can\'t be converted to number')
          -- Field will be set to nil
        end
        entryObject[entryFieldName] = numberizedEntryFieldValue
      end
    end
    parsedMessage.scrollAction = entryObject.diverRound -- This field is hijacked for pageUp,pageDown purpose
    -- TODO, quick parse of club when multiple (club1/club2)
    local array = split(entryObject.club, '/')
    if type(array) == 'table' then
      entryObject.club, entryObject.country = array[1], (array[2] or config.Dr.defaultDiverCountry or '')
    end
    log(4, "Parsed entry (name, club, country, rank, point, diverRound)", entryObject.name, entryObject.club, entryObject.country, entryObject.rank, entryObject.points, entryObject.diverRound)
    table.insert(parsedMessage.entryArray, entryObject)
    currentEntryInfoFieldIndex = currentEntryInfoFieldIndex + entryInfoFieldCount -- Move to next entry fields
  end
  log(3, "Parsed entry count", #parsedMessage.entryArray)
  log(4, 'Parsed scrollAction', parsedMessage.scrollAction)

  return parsedMessage
end

function dr_updateFile_parseBlankUpdate(updateArray)
  local parsedMessage = {}
  local fieldToSplitIndex = -- Not much to parse since it's about requesting a blank display
  {
    ['updateType'] = 31,
    ['eventAorB'] = 2,
  }
  for fieldName,splitIndex in pairs(fieldToSplitIndex) do
    if splitIndex > #updateArray then
      log(2, 'Update object field '.. (fieldName or 'nil') .. ' can\'t come from split entry index '.. (splitIndex or 'nil') .. ' of '..#updateArray)
    end
    parsedMessage[fieldName] = updateArray[splitIndex]
  end

  return parsedMessage
end

function dr_updateFile_parseStartlistUpdate(updateArray)
  local parsedMessage = {}
  local fieldToSplitIndex = -- Not much to parse, it's motly dynamic fields
  {
    ['updateType'] = 31,
    ['eventAorB'] = 2,
    ['eventName'] = 60,
    ['meetName'] = 62,
  }
  for fieldName,splitIndex in pairs(fieldToSplitIndex) do
    if splitIndex > #updateArray then
      log(2, 'Update object field '.. (fieldName or 'nil') .. ' can\'t come from split entry index '.. (splitIndex or 'nil') .. ' of '..#updateArray)
    end
    parsedMessage[fieldName] = updateArray[splitIndex]
  end

  -- Parse each of the starters
  -- TODO, very hard to handle synchro (unavailable for now)
  local firstEntryInfoFieldIndex = 74
  local entryInfoFieldCount = 6
  local entryInfoFieldToSplitIndexOffset =
  {
    ['diverRound'] = 2,
    ['name'] = 3,
    ['startOrder'] = 4,
    ['club'] = 5,
  }
  parsedMessage.entryArray = {}
  local currentEntryInfoFieldIndex = firstEntryInfoFieldIndex
  while currentEntryInfoFieldIndex + entryInfoFieldCount <= #updateArray do
    local entryObject = {}
    for entryFieldName,entrySplitIndexOffset in pairs(entryInfoFieldToSplitIndexOffset) do
      local splitIndex = entrySplitIndexOffset + currentEntryInfoFieldIndex
      if entrySplitIndexOffset > #updateArray then
        log(2, 'Update object field '.. (entryFieldName or 'nil') .. ' can\'t come from split entry index '.. (splitIndex or 'nil') .. ' of '..#updateArray)
      end
      entryObject[entryFieldName] = updateArray[splitIndex]
    end
    parsedMessage.scrollAction = entryObject.diverRound -- This field is hijacked for pageUp,pageDown purpose
    -- TODO, quick parse of club when multiple (club1/club2)
    local array = split(entryObject.club, '/')
    if type(array) == 'table' then
      entryObject.club, entryObject.country = array[1], (array[2] or config.Dr.defaultDiverCountry or '')
    end
    log(4, "Parsed entry (name, club, country, startOrder)", entryObject.name, entryObject.club, entryObject.country, entryObject.startOrder)
    table.insert(parsedMessage.entryArray, entryObject)
    currentEntryInfoFieldIndex = currentEntryInfoFieldIndex + entryInfoFieldCount -- Move to next entry fields
  end
  log(3, "Parsed entry count", #parsedMessage.entryArray)
  log(4, 'Parsed scrollAction', parsedMessage.scrollAction)

  return parsedMessage
end

function dr_updateFile_parseJudgelistUpdate(updateArray)
  local parsedMessage = {}
  local fieldToSplitIndex = -- Not much to parse, it's motly dynamic fields
  {
    ['updateType'] = 31,
    ['eventAorB'] = 2,
    ['judgeCount'] = 50,
    ['eventName'] = 60,
    ['meetName'] = 62,
  }
  for fieldName,splitIndex in pairs(fieldToSplitIndex) do
    if splitIndex > #updateArray then
      log(2, 'Update object field '.. (fieldName or 'nil') .. ' can\'t come from split entry index '.. (splitIndex or 'nil') .. ' of '..#updateArray)
    end
    parsedMessage[fieldName] = updateArray[splitIndex]
  end

  -- Transform some of these strings to numbers
  local fieldToNumberize = {'judgeCount'}
  for i,fieldName in pairs(fieldToNumberize) do
    if parsedMessage[fieldName] ~= nil then
      local numberizedFieldValue = tonumber(parsedMessage[fieldName])
      if numberizedFieldValue == nil then
        log(2, 'Parsed field '.. (fieldName or 'nil') .. ' (' .. (parsedMessage[fieldName] or 'nil') .. ') can\'t be converted to number')
        -- Field will be set to nil
      end
      parsedMessage[fieldName] = numberizedFieldValue
    end
  end

  -- Parse each of the judges
  local firstJudgeInfoFieldIndex = 74
  local judgeInfoFieldCount = 6
  local judgeInfoFieldToSplitIndexOffset =
  {
    ['diverRound'] = 2,
    ['name'] = 3,
    ['role'] = 5,
  }
  parsedMessage.judgeArray = {}
  local currentJudgeInfoFieldIndex = firstJudgeInfoFieldIndex
  while currentJudgeInfoFieldIndex + judgeInfoFieldCount <= #updateArray do
    local judgeObject = {}
    for judgeFieldName,judgeSplitIndexOffset in pairs(judgeInfoFieldToSplitIndexOffset) do
      local splitIndex = judgeSplitIndexOffset + currentJudgeInfoFieldIndex
      if judgeSplitIndexOffset > #updateArray then
        log(2, 'Update object field '.. (judgeFieldName or 'nil') .. ' can\'t come from split entry index '.. (splitIndex or 'nil') .. ' of '..#updateArray)
      end
      judgeObject[judgeFieldName] = updateArray[splitIndex]
    end
    -- Name might contain the country code, split it
    judgeObject.name, judgeObject.country = splitJudgeNameAndCode(judgeObject.name)
    if judgeObject.country == nil then
      judgeObject.country = config.Dr.defaultJudgeCountry or ''
    end

    log(4, "Parsed judge (role, name, country)", judgeObject.role, judgeObject.name, judgeObject.country)
    table.insert(parsedMessage.judgeArray, judgeObject)
    currentJudgeInfoFieldIndex = currentJudgeInfoFieldIndex + judgeInfoFieldCount -- Move to next judge fields
    parsedMessage.scrollAction = judgeObject.diverRound -- This field is hijacked for pageUp,pageDown purpose
  end
  log(3, "Parsed judge count", #parsedMessage.judgeArray)
  log(4, 'Parsed scrollAction', parsedMessage.scrollAction)

  return parsedMessage
end

local updateType =
{
  -- TODO Build lookup tables instead of hardcoding it like that
  [1] = {typeInt = 1, typeKey = 'UPDATE_TYPE_DIVE',       typeParser = dr_updateFile_parseDiveUpdate},
  [2] = {typeInt = 2, typeKey = 'UPDATE_TYPE_RANKING',    typeParser = dr_updateFile_parseRankingUpdate},
  [4] = {typeInt = 4, typeKey = 'UPDATE_TYPE_BLANK',      typeParser = dr_updateFile_parseBlankUpdate},
  [6] = {typeInt = 6, typeKey = 'UPDATE_TYPE_STARTLIST',  typeParser = dr_updateFile_parseStartlistUpdate},
  [7] = {typeInt = 7, typeKey = 'UPDATE_TYPE_JUDGELIST',  typeParser = dr_updateFile_parseJudgelistUpdate},
}

function dr_updateFile_parseFile(fileContent)
  -- check parameters
  if type(fileContent) ~= 'string' then
    log(3, "Invalid parameters to dr_updateFile_parseFile, type is ", type(fileContent))
    return nil
  end

  -- Split to a table of | separated strings fields
  local splitArray = split(fileContent,'|', true)
  if type(splitArray) ~= 'table' or #splitArray < 1 then
    log(2, "Split failed, returned no array")
    return nil
  end

  -- Check 'framing'
  if splitArray[#splitArray] ~= '^\r\n' then
    log(2, "Invalid end of update file (not ^\\r\\n)")
    return nil
  end

  -- Retrieve update type
  local scoreBoardDisplay = splitArray[31] -- TODO, use a table for these values
  log(4, 'Update message type ' .. (scoreBoardDisplay or 'nil') .. ' received')

  -- Retrieve and invoke type parser
  local updateTypeEntry = updateType[tonumber(scoreBoardDisplay)]
  if updateTypeEntry == nil or type(updateTypeEntry.typeParser) ~= 'function' then
    log(2, "No parser for update type", scoreBoardDisplay)
    return nil
  end
  local updateObject = updateTypeEntry.typeParser(splitArray)
  updateObject.typeStr = updateTypeEntry.typeKey -- TODO, temporary abstraction, we might need better

  -- Check parsed update object validity
  if updateObject == nil then
    log(2, "Unable to build an update object")
    return nil
  end

  return updateObject
end
