#!/bin/bash

LOG_FILE=./Dr2TvGraphics.log
DR2TVGRAPHICS_REPLAY_DIR_BASE=./replays
DR2TVGRAPHICS_DIVESTAMPS_DIR=./divestamps
AUTOSTART_DELAY=5

cd `dirname $0`
echo "## Dr2TvGraphics autorun start" > /dev/tty1
echo "##   Log file is " `pwd`/$LOG_FILE > /dev/tty1
echo "##   IP list:" > /dev/tty1
for ip in `ifconfig | grep "inet " | awk '{print $2}'`
do
    echo "##    $ip" > /dev/tty1
done
echo "##   Waiting $AUTOSTART_DELAY seconds" > /dev/tty1
sleep $AUTOSTART_DELAY
echo "##   Loop starting" > /dev/tty1

echo "  " >> $LOG_FILE
echo "###################################" >> $LOG_FILE
echo "Starting autorun loop" >> $LOG_FILE
date >> $LOG_FILE
ifconfig >> $LOG_FILE

export LUA_PATH="$LUA_PATH;./src/?.lua"
echo "LUA_PATH set to $LUA_PATH" >> $LOG_FILE

while true
do
    echo "###################################" >> $LOG_FILE
    echo "Looking for previous replay folders"
    replayFolderCounter=0
    while true
    do
        replayFolderCountWithLeadingZeros=`printf '%04d\n' "$replayFolderCounter"`
        DR2TVGRAPHICS_REPLAY_DIR=$DR2TVGRAPHICS_REPLAY_DIR_BASE"/"$replayFolderCountWithLeadingZeros
        [ -d "$DR2TVGRAPHICS_REPLAY_DIR" ] || break
        #echo "Dir does exist, moving to next" " "$DR2TVGRAPHICS_REPLAY_DIR
        replayFolderCounter=$((replayFolderCounter + 1))
    done
    echo "Creating replay folders: $DR2TVGRAPHICS_REPLAY_DIR"
    mkdir -p $DR2TVGRAPHICS_REPLAY_DIR

    echo "Looking for previous divestamps files"
    divestampFileCounter=0
    while true
    do
        divestampFileCounterWithLeadingZeros=`printf '%04d\n' "$divestampFileCounter"`
        DR2TVGRAPHICS_DIVESTAMPS_FILE=$DR2TVGRAPHICS_DIVESTAMPS_DIR"/"$divestampFileCounterWithLeadingZeros".txt"
        [ -f "$DR2TVGRAPHICS_DIVESTAMPS_FILE" ] || break
        #echo "File does exist, moving to next" " "$DR2TVGRAPHICS_DIVESTAMPS_FILE
        divestampFileCounter=$((divestampFileCounter + 1))
    done
    echo "Creating divestamp folders: $DR2TVGRAPHICS_DIVESTAMPS_DIR"
    mkdir -p $DR2TVGRAPHICS_DIVESTAMPS_DIR

    echo "Starting main.lua" >> $LOG_FILE
    export DR2TVGRAPHICS_REPLAY_DIR="$DR2TVGRAPHICS_REPLAY_DIR"
    export DR2TVGRAPHICS_DIVESTAMPS_FILE="$DR2TVGRAPHICS_DIVESTAMPS_FILE"
    lua5.3 ./main.lua >> $LOG_FILE 2>&1
    echo "main.lua exited with value $?" >> $LOG_FILE
done
