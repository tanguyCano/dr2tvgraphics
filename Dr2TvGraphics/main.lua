require 'utils.misc'
require 'threads.utils'
local config = require 'config'

currentThreadId = 'Main'
oldPrint = print; print = function(...) oldPrint(currentThreadId,...) end

if threads_utils_startAllThreads() ~= true then
  FATAL("Unable to start threads")
end

if config.general.autostart == true then
  threads_utils_waitForAllThreads()
else
  threads_utils_stopAllThreads()
end

log(3, "App shutdown")
