#!/bin/sh

setup_failed () {
    echo "Failed during operation: $1"
    echo "Aborting setup"
    exit 3
}


# Last tested on Ubuntu 21.10
echo "Install Dr2TvGraphics dependencies on Ubuntu"
cd $HOME || setup_failed "Home dir entry"

echo " - Refresh package list"
echo "   Note: Sudo password prompt may appear"
sleep 2
sudo apt-get update || setup_failed "Package list update or first sudo prompt"

echo " - Install all required packages"
echo "   Note: Press [Y] when prompted"
sleep 2
sudo apt-get install lua5.3 liblua5.3-dev libsdl2-dev libsdl2-image-dev libsdl2-net-dev libsdl2-ttf-dev libsdl2-mixer-dev cmake git || setup_failed "Packages install"

echo " - Clone, build and install luasdl2"
sleep 2
rm -rf luasdl2
git clone https://github.com/Tangent128/luasdl2.git || setup_failed "Luasdl2 git checkout"
cd luasdl2/ || setup_failed "Luasdl2 git directory entry"
mkdir _build_ || setup_failed "Luasdl2 build dir creation"
cd _build_/ || setup_failed "Luasdl2 build dir entry"
cmake .. -DWITH_LUAVER=5.3 -DLua_INCLUDE_DIR=/usr/include/lua5.3 -DLua_LIBRARY=/usr/lib/x86_64-linux-gnu/ || setup_failed "Luasdl2 cmake"
make || setup_failed "Luasdl2 build"
sudo make install || setup_failed "Luasdl2 install"

echo "Setup completed, you can now launch run.sh in the ../Dr2TvGraphics folder"
