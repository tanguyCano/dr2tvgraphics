#!/bin/sh

if [ "$#" -ne 1 ]
then
    echo "Usage $0 release/path"
    exit 1
fi

runDir=`pwd`
srcDir=$runDir/../Dr2TvGraphics

# Prevent overwrite
releaseDir=$1
if [ -d $releaseDir ]
then
   echo "Error: Release folder already exists ($releaseDir)."
   exit 1
fi

#Create the release
mkdir $releaseDir
cp -vr $srcDir/resources $releaseDir/
cp -vr $srcDir/src $releaseDir/
cp -v $srcDir/main.lua $releaseDir/
cp -v $srcDir/*.sh $releaseDir/
git describe --all --long  --dirty > $releaseDir/gitDescribe.txt
