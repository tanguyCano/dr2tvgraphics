DR2TvGraphics
=====================

Companion software to [DiveRecorder][1] to generate live TV graphics.

## General plan
This is a work in progress.
The current plan is to use SDL2 and Lua on a linux platform (most likely a RaspberryPi) and to output in FullHD on the HDMI port for Chroma-Key blending (handled by an external video device).

[1]: https://diverecorder.co.uk/
